//
//  CustomerService.swift
//  CarWash
//
//  Created by iOS on 31/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import Foundation

class CustomerService: HTTPService
{
    override init() {
        super.init()
    }
    
    //MARK:  login
    
    func loginAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/login", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //MARK:  User Register
    
    func registerAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?,_ isImage : Bool?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ error : NSError? ) -> Void))
    {
        self.uploadFileRequestWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "auth/register", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error! as NSError)
        }
    }
    
    //MARK:  change mobile number
    
    func changeMobileNumberAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/changemobile", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //  Send Verification Code
    
    func sendVerificationAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/sendVerificationCode", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //  Verify OTP
    
    func verifyOtpAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/verifyOtp", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //MARK:  Forgot Password
    
    func forgotPwdAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/forgotPassword", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //  Verify Forgot OTP
    
    func verifyForgotOTPRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/verifyForgotOtp", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //  Reset Password
    
    func resetPwdAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "auth/resetPassword", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    
    //MARK: MORE SERVICES
    
    func profileRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "user/profile", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // Update profile pic
    
    func updatePicRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ error : NSError? ) -> Void))
    {
        self.uploadAvatarImageRequestWithParameters(paraDic, serviceName: "user/updateAvatar", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error! as NSError)
        }
        
        //self.uploadLoggedInFileRequestWithParameters(kHttpMethodType.kHttpMethodTypePost, paraDic, files: files, isImage: true, headers: nil, serviceName: "user/updateAvatar", success: { (response, data) in
        //    success(response, data)
        //}) { (error) in
        //    failure(error! as NSError)
        //}
    }
    
    func updateCoverPicRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ error : NSError? ) -> Void))
    {
        self.uploadCoverLoggedInFileRequestWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "user/updateCoverImage", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error! as NSError)
        }
    }
    
    //Update Profile
    
    func updateProfileRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/updateProfile", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //Update service provider Profile
    
    func updateServiceProviderProfileRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/updateProviderProfile", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //  Change Password
    
    func changePwdAPIRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/changePassword", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // Contact US
    
    func contactUsRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "public/contactus", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    
    // MARK: Home Screen APi
    
    // home api
    
    func homeApiRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "home", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // notification api
    
    func notificationApiRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "notification/notificationlist", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // notification On Off api
    func notificationOnOffApiRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/notificationsetting", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // Activity On Off api
    func activityOnOffApiRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/user_availability", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // Change Language api
    func changeLanguageApiRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/updatelanguage", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // Upadte operating hours Api
    func updateOperatingHoursRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/manageOperatingHour", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // ADD BOOKING
    
    func addBookingRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "booking/addbooking", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // UPDATE BOOKING STATUS
    
    func updateBookingStatusWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "booking/updatebookingstatus", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //MARK:  About US Data
    
    func getAboutUsDataRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "page/display?slug=\(files ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    //MARK:  check service provider
    
    func checkServiceProviderDataRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "home/checkProvider" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // Delete Document
    
    func deleteUploadedDocumentRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/deleteDocuments", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // get user booking history search data
    
    func getUserBookingSearchListServiceRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, _ files1 : NSString?, _ sort_string : NSString?, _ date_string : NSString?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethodWithAuthorization(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "booking/userbookinghistory?search=\(files ?? "")&page=\(files1 ?? "")&request_status=\(sort_string ?? "")&date=\(date_string ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // complaint booked service
    
    func getBookingHistoryDetailRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethodWithAuthorization(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "booking/bookingdetails?booking_id=\(files ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // get provider booking history search data
    
    func getProviderBookingSearchListServiceRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, _ files1 : NSString?, _ sort_string : NSString?, _ date_string : NSString?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethodWithAuthorization(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "booking/providerbookinghistory?search=\(files ?? "")&page=\(files1 ?? "")&request_status=\(sort_string ?? "")&date=\(date_string ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // logout user api
    
    func logoutUserRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "user/logout", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // complaint booked service
    
    func complaintBookedServiceRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "complain/bookingComplain", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // write review
    
    func writeReviewServiceRequestWithParameters(_ paraDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypePost, headers: nil, serviceName: "review/reviewrating", paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // get review list
    
    func getReviewListServiceRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, _ files1 : NSString?,  success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethodWithAuthorization(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "review/reviewlist?provider_id=\(files ?? "")&page=\(files1 ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
    
    // get user booking history search data
    
    func getConversationImageRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?,  success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethodWithAuthorization(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "user/getavatar?fcm_user_id=\(files ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
}
