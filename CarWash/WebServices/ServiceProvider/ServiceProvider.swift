//
//  ServiceProvider.swift
//  CarWash
//
//  Created by iOS on 08/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import Foundation

class ServiceProvider: HTTPService
{
    override init() {
        super.init()
    }
    
    // UPGRADE USER (Switch Account From User to service providebr)
    
    func switchUserRequestWithParameters(_ paraDic : NSMutableDictionary?,  _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.uploadCoverWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "user/upgradeUser", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error as? HTTPURLResponse, error as NSError?)
        }
    }
    
    // Upadte operating hours
    
    func updateOperatingHoursRequestWithParameters(_ paraDic : NSMutableDictionary?,  _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.uploadCoverWithParameters(paraDic, files: files, isImage: true, headers: nil, serviceName: "user/manageOperatingHour", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error as? HTTPURLResponse, error as NSError?)
        }
    }
    
    // Upload Documents
    
    func uploadDocsWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.uploadDocumentsRequestWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "user/uploadDocuments", success: { (response, data) in
             success(response, data)
            
        }) { (error) in
            failure(error as? HTTPURLResponse, error as NSError?)
        }
    }
    
    // add services
    
    func addServiceWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.addServiceRequestWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "service/addservice", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error as? HTTPURLResponse, error as NSError?)
        }
    }
    
    // delete service
    
    func deleteServiceWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.deleteServiceRequestWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "service/deleteservice", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error as? HTTPURLResponse, error as NSError?)
        }
    }
    
    // edit services
    
    func editServiceWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.editServiceRequestWithParameters(paraDic, files: files, isImage: false, headers: nil, serviceName: "service/editservice", success: { (response, data) in
            success(response, data)
        }) { (error) in
            failure(error as? HTTPURLResponse, error as NSError?)
        }
    }
    
    // Get Service List
    
    func getServiceListWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSMutableArray?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        self.startLoggedInUserRequestWithHttpMethod(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "service/servicelist" as NSString, paramDic: paraDic, success: { (response, data) in
             success(response, data)
        }, failure: { (response, error) in
            failure(response, error)
        })
    }
    
    // get booking search data
    
    func getBookingPullRefreshListServiceRequestWithParameters(_ paraDic : NSMutableDictionary?, _ files : NSString?, _ files1 : NSString?, _ sort_string : NSString?, _ date_string : NSString?,  success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary? ) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        self.startRequestWithHttpMethodWithAuthorization(kHttpMethodType.kHttpMethodTypeGet, headers: nil, serviceName: "booking/bookinglist?search=\(files ?? "")&page=\(files1 ?? "")&request_status=\(sort_string ?? "")&date=\(date_string ?? "")" as NSString, paramDic: paraDic, success: {(response, data) in
            success(response, data) }, failure: { (response, error) in
                failure(response, error)
        })
    }
}
