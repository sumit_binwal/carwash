//
//  NSBundle+Language.m
//  ios_language_manager
//
//  Created by Maxim Bilan on 1/10/15.
//  Copyright (c) 2015 Maxim Bilan. All rights reserved.
//

#import "NSBundle+Language.h"
#import "LanguageManager.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "CarWash-Swift.h"
#import <IQKeyboardManagerSwift/IQKeyboardManagerSwift-Swift.h>


//#ifdef USE_ON_FLY_LOCALIZATION

static const char kBundleKey = 0;

@interface BundleEx : NSBundle

@end

@implementation BundleEx

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName
{
    NSBundle *bundle = objc_getAssociatedObject(self, &kBundleKey);
    if (bundle) {
        return [bundle localizedStringForKey:key value:value table:tableName];
    }
    else {
        return [super localizedStringForKey:key value:value table:tableName];
    }
}

@end

@implementation NSBundle (Language)

+ (void)setLanguage:(NSString *)language
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object_setClass([NSBundle mainBundle], [BundleEx class]);
    });
    if ([LanguageManager isCurrentLanguageRTL]) {
        if ([[[UIView alloc] init] respondsToSelector:@selector(setSemanticContentAttribute:)]) {
            [[UIView appearance] setSemanticContentAttribute:
             UISemanticContentAttributeForceRightToLeft];
        }
    }else {
        if ([[[UIView alloc] init] respondsToSelector:@selector(setSemanticContentAttribute:)]) {
            [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        }
    }
   // BOOL appletDirection = [[NSUserDefaults standardUserDefaults]objectForKey:@"AppleTextDirection"];
   // BOOL force = [[NSUserDefaults standardUserDefaults]objectForKey:@"NSForceRightToLeftWritingDirection"];
    [[NSUserDefaults standardUserDefaults] setBool:[LanguageManager isCurrentLanguageRTL] forKey:@"AppleTextDirection"];
    [[NSUserDefaults standardUserDefaults] setBool:[LanguageManager isCurrentLanguageRTL] forKey:@"NSForceRightToLeftWritingDirection"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    id value = language ? [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]] :nil;
    
    if (value) {
        objc_setAssociatedObject([NSBundle mainBundle], &kBundleKey, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"USER_DATA"] != nil) {

            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"GUEST_USER"] == true) {
                [appDelegate addTabBar];

            }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTimeLaunch"] == false){
                ChangeLanguageVC *vc  = [[ChangeLanguageVC alloc]initWithNibName:@"ChangeLanguageVC" bundle:nil];
                UINavigationController *nv = [[UINavigationController alloc]init];
                nv.viewControllers = [NSArray arrayWithObject: vc];
                
                UIWindow *wind = [UIApplication sharedApplication].keyWindow;
                wind.rootViewController = nv;
            }
            else{
                WelComeVC *vc  = [[WelComeVC alloc]initWithNibName:@"WelComeVC" bundle:nil];
                UINavigationController *nv = [[UINavigationController alloc]init];
                nv.viewControllers = [NSArray arrayWithObject: vc];

                UIWindow *wind = [UIApplication sharedApplication].keyWindow;
                wind.rootViewController = nv;
            }
        }else{
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_DATA"];
            NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            int str = [[dict objectForKey:@"user_role"] intValue];
            if(str == 27)
            {
                [appDelegate addTabBarForServiceProvider];
            }
            else
            {
                [appDelegate addTabBar];
            }
        }
        
        
//        WelComeVC *vc  = [[WelComeVC alloc]initWithNibName:@"WelComeVC" bundle:nil];
//            UINavigationController *nv = [[UINavigationController alloc]init];
//        nv.viewControllers = [NSArray arrayWithObject: vc];
//
//        UIWindow *wind = [UIApplication sharedApplication].keyWindow;
//        wind.rootViewController = nv;
    
         NSLog(@"<=====Storyboard ki value =====>%@",value);
        
        [IQKeyboardManager sharedManager].toolbarDoneBarButtonItemText = NSLocalizedString(@"DONE", comment: "");
        
    }
    
    else {
        value = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Base" ofType:@"lproj"]];
        objc_setAssociatedObject([NSBundle mainBundle], &kBundleKey, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        [IQKeyboardManager sharedManager].toolbarDoneBarButtonItemText = NSLocalizedString(@"DONE", comment: "");
    }
}

@end

//#endif
