//
//  TotalCostCell.swift
//  CarWash
//
//  Created by Suman Payal on 11/12/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class TotalCostCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var totalCostLael: UILabel!
    @IBOutlet weak var navigationBtn: UIButton!
    @IBOutlet weak var lblTotalCost: UILabel!
    
    @IBOutlet weak var addressTrailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
