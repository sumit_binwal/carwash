//
//  ConversationCell.swift
//  CarWash
//
//  Created by iOS on 10/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//
//9852367456
import UIKit

class ConversationCell: UITableViewCell {

    @IBOutlet weak var unReadLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.profilePic.layer.cornerRadius = (self.profilePic.frame.size.width/320*SCREEN_WIDTH)/2
        self.profilePic.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        self.profilePic.layer.borderWidth = 1.0
        self.profilePic.layer.masksToBounds = true
        
        self.unReadLabel.layer.cornerRadius = (self.unReadLabel.frame.size.width/320*SCREEN_WIDTH)/2
        self.unReadLabel.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        self.unReadLabel.layer.borderWidth = 1.0
        self.unReadLabel.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
