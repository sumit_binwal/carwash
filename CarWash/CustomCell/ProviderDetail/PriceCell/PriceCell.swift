//
//  PriceCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 25/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell
{
    @IBOutlet var priceView : UIView?
    @IBOutlet var qualityView : UIView?
    @IBOutlet var timeView : UIView?
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuality: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var priceRatingView: FloatRatingView!
    @IBOutlet weak var qualityRatingView: FloatRatingView!
    @IBOutlet weak var timeRatingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        priceRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        priceRatingView.type = .halfRatings
        priceRatingView.rating = 0.0
        priceRatingView.isUserInteractionEnabled = false
        
        qualityRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        qualityRatingView.type = .halfRatings
        qualityRatingView.rating = 0.0
        qualityRatingView.isUserInteractionEnabled = false
        
        timeRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        timeRatingView.type = .halfRatings
        timeRatingView.rating = 0.0
        timeRatingView.isUserInteractionEnabled = false
        
        if language == "en"
        {
            
        }
        else
        {
            priceRatingView.transform = priceRatingView.transform.rotated(by: CGFloat(Double.pi))
            
            qualityRatingView.transform = qualityRatingView.transform.rotated(by: CGFloat(Double.pi))
            
            timeRatingView.transform = timeRatingView.transform.rotated(by: CGFloat(Double.pi))
        }
        
        priceRatingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        priceRatingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
        
        qualityRatingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        qualityRatingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
        
        timeRatingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        timeRatingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
