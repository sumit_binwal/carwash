//
//  TypeCell.swift
//  CarWash
//
//  Created by iOS on 24/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class TypeCell: UITableViewCell
{
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblDesc  : UILabel?
    
    @IBOutlet var imgCar   : UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
