//
//  HomeCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 12/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import IQDropDownTextField

class HomeCell: UITableViewCell
{
    @IBOutlet var btnIcon       : UIButton?
    @IBOutlet var lblTitle      : UILabel?
    @IBOutlet var lblValue      : UILabel?
    @IBOutlet var lblAddress    : UILabel?
    @IBOutlet var txtDate       : UITextField?
    @IBOutlet var imgArrow      : UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
