//
//  ReceiverTableViewCell.swift
//  CarWash
//
//  Created by iOS on 10/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class ReceiverTableViewCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var messageBackground: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        //self.messageBackground.layer.cornerRadius = 0
        //self.messageBackground.clipsToBounds = true
        
        //let imageR = UIImage(named: "")
        //self.messageBackground.image = imageR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
