//
//  SenderTableViewCell.swift
//  CarWash
//
//  Created by iOS on 10/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class SenderTableViewCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var messageBackground: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        //self.messageBackground.layer.cornerRadius = 0
        //self.messageBackground.clipsToBounds = true
        
        //let imageS = UIImage(named: "")
        //self.messageBackground.image = imageS
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
