//
//  TypeCell.swift
//  CarWash
//
//  Created by iOS on 24/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell
{
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
