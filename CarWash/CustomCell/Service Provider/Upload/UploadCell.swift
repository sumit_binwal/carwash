//
//  UploadCell.swift
//  CarWash
//
//  Created by iOS on 10/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class UploadCell: UITableViewHeaderFooterView
{
    @IBOutlet var lblText1 : UILabel?
    @IBOutlet var lblText2 : UILabel?
    @IBOutlet var btnAgree : UIButton?
    @IBOutlet var btnTerms : UIButton?

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
}
