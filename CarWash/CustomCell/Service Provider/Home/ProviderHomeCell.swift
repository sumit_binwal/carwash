//
//  ProviderHomeCell.swift
//  CarWash
//
//  Created by iOS on 06/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class ProviderHomeCell: UITableViewCell
{
    @IBOutlet var imgUser       : UIImageView?
    @IBOutlet var lblName       : UILabel?
    @IBOutlet var lblDate       : UILabel?
    @IBOutlet var btnCall       : UIButton?
    @IBOutlet var btnMail       : UIButton?
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    
    @IBOutlet weak var unreadCountView: UIView!
    @IBOutlet weak var unreadCountLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
