//
//  AccCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 18/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class AccCell: UITableViewCell
{
    @IBOutlet var lblTitle    : UILabel?
    @IBOutlet var txtValue    : UITextField?
    
    @IBOutlet var imgCheck    : UIImageView?
    @IBOutlet var lblValue    : UILabel?
    @IBOutlet var imgAdditional      : UIImageView?
    
    @IBOutlet var imgChange    : UIImageView?
    @IBOutlet var notificationSwitch    : UISwitch?
    
    @IBOutlet weak var trailing: NSLayoutConstraint!
    @IBOutlet weak var leading: NSLayoutConstraint!
    
    @IBOutlet weak var widthConstarint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
