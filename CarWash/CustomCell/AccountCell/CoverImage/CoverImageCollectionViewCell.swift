//
//  CoverImageCollectionViewCell.swift
//  CarWash
//
//  Created by Ratina on 4/20/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class CoverImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
