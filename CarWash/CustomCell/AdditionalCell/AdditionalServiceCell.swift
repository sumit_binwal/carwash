//
//  AdditionalServiceCell.swift
//  CarWash
//
//  Created by Suman Payal on 15/12/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class AdditionalServiceCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var plusImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
