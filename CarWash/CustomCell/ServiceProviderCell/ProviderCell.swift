//
//  ProviderCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 20/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class ProviderCell: UITableViewCell
{
    @IBOutlet var imgService    : UIImageView?
    @IBOutlet var imgLogo       : UIImageView?
    
    @IBOutlet weak var newProviderLabel: UILabel!
    
    @IBOutlet var lblName       : UILabel?
    @IBOutlet var lblContact    : UILabel?
    @IBOutlet var lblKm         : UILabel?
    @IBOutlet var lblReview     : UILabel?
    
    @IBOutlet var btnCall       : UIButton?
    @IBOutlet var btnMail       : UIButton?
    
    @IBOutlet weak var unreadCountView: UIView!
    @IBOutlet weak var unreadCountLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        ratingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        ratingView.type = .halfRatings
        ratingView.rating = 0.0
        ratingView.isUserInteractionEnabled = false
        
        if language == "en"
        {
            
        }
        else
        {
            ratingView.transform = ratingView.transform.rotated(by: CGFloat(Double.pi))
        }
        
        ratingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        ratingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
