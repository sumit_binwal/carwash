//
//  SignUpVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 04/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RSKImageCropper
import QuartzCore
import MBProgressHUD
import FirebaseAuth
import AWSS3

class SignUpVC: UIViewController
{
    @IBOutlet weak var  btnProfile      : UIButton?
    @IBOutlet weak var  txtName         : UITextField?
    @IBOutlet weak var  txtMail         : UITextField?
    @IBOutlet weak var  txtPwd          : UITextField?
    @IBOutlet weak var  txtConfirm      : UITextField?
    @IBOutlet weak var  txtMobileNo     : UITextField?
    
    @IBOutlet weak var  imgConfirm      : UIImageView?
    @IBOutlet weak var  imgTick         : UIImageView?
    
    @IBOutlet weak var  txtCode             : UITextField?
    @IBOutlet weak var  codePicker          : UIPickerView?
    
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var emailAddressTitle: UILabel!
    @IBOutlet weak var passwordTitle: UILabel!
    @IBOutlet weak var confirmPasswordTitle: UILabel!
    @IBOutlet weak var mobileNoTitle: UILabel!
    
    @IBOutlet  var  termsView       : UIView!
    @IBOutlet  var  aboutView       : UIView!
    
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var termsWebView: UIWebView!
    
    @IBOutlet fileprivate var  scrollView       : UIScrollView!
    
    let imagePicker = UIImagePickerController()
    var imageCropVC : RSKImageCropViewController!
    var isImageSelected : Bool = false
    
    var fcmUserID : String?
    
    var codeAry : NSArray?
    var index   : Int = 0
    
    var userImageUrl = ""
    
    var isFromConfirmBooking = false
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var agreeCheckButton: UIButton!
    @IBOutlet weak var agreeCheckLabel: UILabel!
    @IBOutlet weak var termsConditionsButton: UIButton!
    
    @IBOutlet weak var alreadyLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    
    var isTermsChecked = false
    
    @IBOutlet weak var innerView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        alreadyLabel.text = NSLocalizedString("Already have an account?", comment: "")
        agreeCheckLabel.text = NSLocalizedString("I agree with the", comment: "")
        
        signInButton.setTitle(NSLocalizedString("SIGN_IN", comment: ""), for: .normal)
        termsConditionsButton.setTitle(NSLocalizedString("TERMS_&_CONDITIONS", comment: ""), for: .normal)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        aboutView.layer.cornerRadius = 10.0
        aboutView.layer.borderColor = UIColor.clear.cgColor
        aboutView.layer.borderWidth = 1.0
        aboutView.layer.masksToBounds = true
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            txtName?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtMail?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtPwd?.contentHorizontalAlignment     = UIControlContentHorizontalAlignment.right
            txtConfirm?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            txtCode?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtMobileNo?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            
            txtName?.textAlignment = NSTextAlignment.left
            txtPwd?.textAlignment = NSTextAlignment.left
            txtMail?.textAlignment = NSTextAlignment.left
            txtConfirm?.textAlignment = NSTextAlignment.left
            txtCode?.textAlignment = NSTextAlignment.left
            txtMobileNo?.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtName?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtConfirm?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            txtMail?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            txtPwd?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            txtCode?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtMobileNo?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            
            txtCode?.textAlignment    = NSTextAlignment.right
            txtMobileNo?.textAlignment     = NSTextAlignment.right
            txtMail?.textAlignment    = NSTextAlignment.right
            txtPwd?.textAlignment     = NSTextAlignment.right
            txtName?.textAlignment    = NSTextAlignment.right
            txtConfirm?.textAlignment = NSTextAlignment.right
        }
        
        self.title = NSLocalizedString("SIGN_UP", comment: "")
        
        self.nameTitleLabel.text = NSLocalizedString("NAME_TEXT", comment: "")
        self.emailAddressTitle.text = NSLocalizedString("EMAIL_ADDRESS_TITLE", comment: "")
        self.passwordTitle.text = NSLocalizedString("PASSWORD_TITLE", comment: "")
        self.confirmPasswordTitle.text = NSLocalizedString("CONFIRM_PASSWORD", comment: "")
        self.mobileNoTitle.text = NSLocalizedString("MOBILE_NUMBER", comment: "")
        
        self.termsLabel.text = NSLocalizedString("TERMS_&_CONDITIONS", comment: "")
        
        self.txtName?.placeholder = NSLocalizedString("PLACEHOLDER_NAME", comment: "")
        self.txtMail?.placeholder = NSLocalizedString("PLACEHOLDER_EMAIL", comment: "")
        self.txtPwd?.placeholder = NSLocalizedString("PLACEHOLDER_PASSWORD", comment: "")
        self.txtConfirm?.placeholder = NSLocalizedString("PLACEHOLDER_CONFIRM", comment: "")
        self.txtMobileNo?.placeholder = NSLocalizedString("ENTER_YOUR_MOBILE_NUMBER", comment: "")
        
        imgTick?.isHidden = true
        isImageSelected = false
        
        codeAry = NSArray.init(contentsOfFile: Bundle.main.path(forResource: "Country", ofType: "plist")!)!
        
        scrollView.isScrollEnabled = true
        
        setUpGesture()
        
        imagePicker.delegate = self
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        termsView.viewWithTag(1)?.layer.cornerRadius = 4.0
        termsView.viewWithTag(1)?.layer.masksToBounds = true
        
        //for countryDict: [AnyHashable: Any] in codeAry
        var strCode : NSString
        var i: Int = 0
        
        strCode = CommonFunctions.getCallingCodeString() as NSString
        print("strCode = ", strCode)
        
        for countryDict in codeAry as! [NSDictionary]
        {
            var strCCode: String = countryDict["isd_code"] as? String ?? ""
            strCCode = strCCode.replacingOccurrences(of: " ", with: "")
            strCCode = (strCCode.replacingOccurrences(of: "+", with: "") as NSString) as String
            
            if (strCCode == strCode as String)
            {
                index = i
                
                let dict = codeAry?.object(at: index) as! NSDictionary
                let str  = dict["isd_code"] as? String
                
                txtCode?.text = "+" + str!
                
                codePicker?.selectRow(i, inComponent: 0, animated: false)
                
                break
            }
            
            i += 1
        }
        
        txtCode?.inputView = codePicker
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //self.title = "Sign Up"
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        if isFromConfirmBooking
        {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        }
        else
        {
            self.navigationItem.hidesBackButton = true
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:  UIImage(named: "right"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Instance Methods
    
    /// Used to set tap gesture on view to hide keyboard when tap on tableview
    func setUpGesture()
    {
        let tapGesture: UITapGestureRecognizer
        
        tapGesture =  UITapGestureRecognizer.init(target: self, action: #selector(handleTapGesture(_:)))
        tapGesture.numberOfTapsRequired = 1
        
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    func resetView()
    {
        scrollView.isScrollEnabled = false
    }
    
    @IBAction func signInButtonAction(_ sender: Any)
    {
        isGuestUser = false
        
        UserDefaults.standard.set(false, forKey: "GUEST_USER")
        UserDefaults.standard.synchronize()
        
        let signInVC = SignInVC()
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @IBAction func agreeCheckButtonAction(_ sender: Any)
    {
        if isTermsChecked
        {
            isTermsChecked = false
            agreeCheckButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
        else
        {
            isTermsChecked = true
            agreeCheckButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
    }
    
    @IBAction func termsConditionsButtonAction(_ sender: Any) {
        
        RESIGN_KEYBOARD
        
        termsView.removeFromSuperview()
        APPDELEGATE.window?.addSubview(termsView)
        termsView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        getAboutUsApi()
        
        let animation = CATransition()
        animation.delegate = self as? CAAnimationDelegate
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = 0.40
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
      
        termsView.layer.add(animation, forKey: kCATransition)
    }
    
    /// Used to hide keyboard
    ///
    /// - Parameter sender: instance of caller
    func handleTapGesture(_ sender: UITapGestureRecognizer)  {
        
        self.view.endEditing(true)
    }
    
    @IBAction func termsCond()
    {
        RESIGN_KEYBOARD
        
        termsView.removeFromSuperview()
        APPDELEGATE.window?.addSubview(termsView)
        termsView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        getAboutUsApi()
        
        let animation = CATransition()
        animation.delegate = self as? CAAnimationDelegate
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = 0.40
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        termsView.layer.add(animation, forKey: kCATransition)
    }
    
    func getAboutUsApi()
    {
        let service = CustomerService()
        
        //let dic : NSMutableDictionary = [:]
        
        let stringName = "terms-and-conditions"
        
        service.getAboutUsDataRequestWithParameters(nil, stringName as NSString, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("about us data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    var stringDescription : NSString? = nil
                    var titleNameStr : String? = nil
                    
                    if data?.object(forKey: "data") != nil {
                        
                        let sectionNames : NSDictionary = data?.object(forKey: "data") as! NSDictionary
                        
                        if language == "en"
                        {
                            stringDescription = sectionNames.object(forKey: "description") as? NSString
                            titleNameStr = sectionNames.object(forKey: "title") as? String
                        }
                        else
                        {
                            stringDescription = sectionNames.object(forKey: "description_ar") as? NSString
                            titleNameStr = sectionNames.object(forKey: "title_ar") as? String
                        }
                    }
                    
                    DispatchQueue.main.async
                        {
                            self.termsWebView.scrollView.isScrollEnabled = true
                            self.termsWebView.isUserInteractionEnabled = true
                            self.termsWebView.scrollView.showsVerticalScrollIndicator = false;
                            self.termsWebView.scrollView.showsHorizontalScrollIndicator = false
                            
                            if data?.object(forKey: "data") != nil {
                                self.termsLabel.text = titleNameStr
                                self.termsWebView.loadHTMLString(stringDescription! as String, baseURL: nil)
                            }
                    }
                }
            }
            else
            {
                CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    
                })
            }
            
        }) { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    @IBAction func closeTermsView()
    {
        let animation = CATransition()
        
        animation.delegate = self as? CAAnimationDelegate
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromBottom
        animation.duration = 0.40
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        termsView.layer.add(animation, forKey: kCATransition)
        
        termsView.frame = CGRect(x: 0, y: SCREEN_HEIGTH, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
    }
    
    @IBAction func showPic()
    {
        //RESIGN_KEYBOARD
        
        view.endEditing(true)
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false

                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtName?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_NAME", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtName?.becomeFirstResponder()
            })
        }
        else if(txtMail?.text?.isEmpty)!
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_EMAIL", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtMail?.becomeFirstResponder()
            })
        }
        else if !(Validation.validateEmail(email: txtMail?.text))
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("INVALID_EMAIL", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtMail?.becomeFirstResponder()
            })
        }
        else if(txtPwd?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_PASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtPwd?.becomeFirstResponder()
            })
        }
        else if((txtPwd?.text?.count)! < 6 || (txtPwd?.text?.count)! > 30)
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("PASSWORD_LENGTH", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtPwd?.becomeFirstResponder()
            })
        }
        else if(txtConfirm?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_CONFIRMPASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtConfirm?.becomeFirstResponder()
            })
        }
        else if(txtPwd?.text != txtConfirm?.text)
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("MATCH_CONFIRMPASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtConfirm?.becomeFirstResponder()
            })
        }
        else if(txtMobileNo?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_PHONE", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtMobileNo?.becomeFirstResponder()
            })
        }
        else if isTermsChecked == false
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("Please check Terms & Conditions", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
        
        return isError
    }
    
    //MARK: KeyboardManager done action
 
    func doneAction()
    {
        //resetView()
        
        if(!(txtConfirm?.text?.isEmpty)!)
        {
            let updatedTextString : NSString = txtConfirm!.text! as NSString
            
            if (updatedTextString .isEqual(to: (txtPwd?.text)!))
            {
                imgTick?.isHidden = false
            }
            else
            {
                imgTick?.isHidden = true
            }
        }
    }
    
    func addParallaxToView(vw: UIView)
    {
        let amount = 100
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        vw.addMotionEffect(group)
    }
    
    func doneButtonPressed()
    {
        RESIGN_KEYBOARD
        let _var = validateUserInput()
        if(_var)
        {
            if (CommonFunctions.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                registerOnFCM()
            }
        }
    }
    
    func registerOnFCM()
    {
        let device_token1 = USERDEFAULT.object(forKey: "FCM_Token") as! String
        
        User.registerUser(withName: (txtName?.text)!, email: (txtMail?.text)!, password: "123456", device_token : device_token1) { (isRegistered, errorCode, userInfo) in
            
            print("isRegistered = \(isRegistered)")
            print("errorCode = \(errorCode)")
            
            if isRegistered == true
            {
                self.fcmUserID = errorCode
                self.registerApi()
            }
            else
            {
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if errorCode == "emailAlreadyInUse"
                {
                    CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMAIL_ALREADY_EXIST", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
                else
                {
                    CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
            }
        }
    }
    
    //MARK: API Call
    
    func registerApi()
    {
        let service = CustomerService()
        
        if (USERDEFAULT.object(forKey: "FCM_Token") != nil)
        {
            device_token = USERDEFAULT.object(forKey: "FCM_Token") as! String as NSString
        }
        
        let number : NSString = (txtMobileNo?.text! as NSString?)!
        
        let dict = codeAry?.object(at: index) as! NSDictionary
        let str  = dict["country_code"] as? String
        
        let dic : NSMutableDictionary = [
        
            "name"         : txtName?.text ?? "",
            "email"        : txtMail?.text ?? "",
            "password"     : txtPwd?.text ?? "",
            "fcm_user_id"  : fcmUserID ?? "",
            "mobile"       : number,
            "country_code" : txtCode?.text ?? "",
            "iso_code"     : str ?? "",
            "device_token" : device_token,
            "avatar"       : self.userImageUrl
        ]
        
        print("dic =", dic)
        
        service.registerAPIRequestWithParameters(dic, nil, isImageSelected, success:
        { (response, data) in
        
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("register data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str1 : NSString = data?.object(forKey: "status") as! NSString
                
                if (str1.isEqual(to: "success"))
                {
                    let data1 = data?.object(forKey: "data") as! NSDictionary
                    
                    USERDEFAULT.set(data1.object(forKey: "temp_token") as! String, forKey: "temp_token")
                    
                    let dic1 : NSMutableDictionary = [
                        "mobile"          : number,
                        "country_code"    : self.txtCode?.text ?? "",
                        "temp_token"      : data1.object(forKey: "temp_token") as! String,
                        "resend"          : true,
                        "iso_code"        : str ?? ""
                    ]
                    
                    let dictChangeMobile : NSMutableDictionary = [
                        "mobile"          : number,
                        "country_code"    : self.txtCode?.text ?? "",
                        "id"      : data1.object(forKey: "id") as! String,
                        "iso_code"        : str ?? ""
                    ]
                    
                    let varVC = VerificationVC()
                    
                    varVC.isSignUp = true
                    varVC.codeDict = dic1
                    varVC.codeDictMobileChange = dictChangeMobile
                    varVC.isFromConfirmBooking = self.isFromConfirmBooking
                    
                    self.navigationController?.pushViewController(varVC, animated: true)
                }
            }
            else
            {
                CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    
                })
            }
        })
        { (error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension SignUpVC : UIPickerViewDataSource, UIPickerViewDelegate
{
    //MARK: UIPickerViewDataSource methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return codeAry!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: SCREEN_WIDTH - 20, height: 44))
        //label.textColor = UIColor.uicolorFromRGB(255, 0, 0)
        label.textAlignment = .center
        label.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        
        let dict = codeAry?.object(at: row) as! NSDictionary
        let str1 = dict["isd_code"] as! String
        let str2 = dict["country_name"] as! String
        
        label.text =  str1 + " " + str2
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        index = Int(row)
    }
}

extension SignUpVC : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        //scrollView.isScrollEnabled = true
        //scrollView.contentSize = CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH + 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField == txtCode)
        {
            let dict = codeAry?.object(at: index) as! NSDictionary
            let str  = dict["isd_code"] as? String
            
            txtCode?.text = "+" + str!
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)
        {
            return false
        }
        
        if(textField == txtConfirm)
        {
            var updatedTextString : NSString = textField.text! as NSString
            updatedTextString = updatedTextString.replacingCharacters(in: range, with: string) as NSString
            
            if (updatedTextString .isEqual(to: (txtPwd?.text)!))
            {
                imgTick?.isHidden = false
            }
            else
            {
                imgTick?.isHidden = true
            }
        }
        
        if textField  == txtMobileNo
        {
            if range.location == 11
            {
                return false
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            return checkEnglishPhoneNumberFormat(string: string, str: str, textField: textField)
        }
        
        return true
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?, textField: UITextField) -> Bool{
        
        if string == ""
        {
            //BackSpace
            return true
        }
        else if str!.count < 3
        {
            
        }
        else if str!.count == 4
        {
            textField.text = textField.text! + "-"
        }
        else if str!.count == 8
        {
            textField.text = textField.text! + "-"
        }
        else if str!.count > 14
        {
            return false
        }
        
        return true
    }
}

extension SignUpVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // UIImagePicker Delegates
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                self.imageCropVC = nil
                
                self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.circle)
                
                self.imageCropVC.delegate = self
                
                self.present(self.imageCropVC, animated: true, completion: nil)
            })
        }
    }
}

extension SignUpVC : RSKImageCropViewControllerDelegate
{
    //MARK: RSKImageCropViewController Delegates
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController)
    {
        isImageSelected = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect)
    {
        self.dismiss(animated: true, completion: nil)
        
        _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation(croppedImage, 0.6)!)
        
        if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
            self.uploadImage(usingImage: fileURL as NSURL)
        }
    }
    
    func uploadImage(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "avatar" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameAvatar + "image"
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    self.isImageSelected = false
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.userImageView?.layer.cornerRadius = ((97/375) * SCREEN_WIDTH)/2  //97/375
                    self.userImageView?.layer.masksToBounds = true
                    
                    self.userImageView?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
                    self.userImageView?.layer.borderWidth = 1.0
                    
                    self.isImageSelected = true
                    
                    self.userImageUrl = "https://s3-us-west-2.amazonaws.com/car-wash/" + "avatar/image/" + keyName
                    print(self.userImageUrl)
                    
                    self.userImageView?.sd_setImage(with: URL.init(string: self.userImageUrl), placeholderImage: #imageLiteral(resourceName: "group20"))
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    self.isImageSelected = false
                    return
                })
            }
            
            return nil
        }
    }
}
