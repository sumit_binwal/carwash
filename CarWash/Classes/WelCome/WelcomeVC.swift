//
//  ViewController.swift
//  SwiftSlideShow
//
//  Created by Malek T. on 8/29/16.
//  Copyright © 2016 Medigarage Studios LTD. All rights reserved.
//

import UIKit

class WelComeVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var guestBtn: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet var topConstraint: NSLayoutConstraint!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.scrollView.frame = CGRect(x:0, y:topConstraint.constant, width:self.view.frame.width, height:self.view.frame.height)
        let scrollViewWidth:CGFloat = SCREEN_WIDTH // self.scrollView.frame.width
        let scrollViewHeight:CGFloat = (232/667)*SCREEN_HEIGTH
        
        let top :CGFloat = (100/667)*SCREEN_HEIGTH
       
        if (LanguageManager.currentLanguageIndex() == 0) {
            let imgOne = UIImageView(frame: CGRect(x:0, y:top,width:scrollViewWidth, height:scrollViewHeight))
            imgOne.image = UIImage(named: "group1")
            let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:top,width:scrollViewWidth, height:scrollViewHeight))
            imgTwo.image = UIImage(named: "group2")
            let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:top,width:scrollViewWidth, height:scrollViewHeight))
            imgThree.image = UIImage(named: "group3")
            self.scrollView.addSubview(imgOne)
            self.scrollView.addSubview(imgTwo)
            self.scrollView.addSubview(imgThree)
            
        }else{
            let imgOne = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:top,width:scrollViewWidth, height:scrollViewHeight))
            imgOne.image = UIImage(named: "group1")
            let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:top,width:scrollViewWidth, height:scrollViewHeight))
            imgTwo.image = UIImage(named: "group2")
            let imgThree = UIImageView(frame: CGRect(x:0, y:top,width:scrollViewWidth, height:scrollViewHeight))
            imgThree.image = UIImage(named: "group3")
            self.scrollView.addSubview(imgOne)
            self.scrollView.addSubview(imgTwo)
            self.scrollView.addSubview(imgThree)
        }
    
        self.scrollView.contentSize = CGSize(width:scrollViewWidth * 3, height:scrollViewHeight)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
        guestBtn.setTitle(NSLocalizedString("CONTINUE_AS_GUEST", comment:""), for: UIControlState.normal)
        signInButton.setTitle(NSLocalizedString("SIGN_IN", comment:""), for: UIControlState.normal)
        signUpButton.setTitle(NSLocalizedString("SIGN_UP", comment:""), for: UIControlState.normal)
        lblTitle.text = NSLocalizedString("WELCOME_ONE_TITLE", comment:"")
        lblSubTitle.text = NSLocalizedString("WELCOME_ONE_SUBTITLE", comment:"")
        
        if (LanguageManager.currentLanguageIndex() == 0) {
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }else{
            self.scrollView.setContentOffset(CGPoint(x: scrollViewWidth * 2, y: 0), animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.navigationBar.isHidden = true
        
        if(LanguageManager.currentLanguageIndex() == 0) {
            language = "en"
        } else {
            language = "ar"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: UIScrollView Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        lblTitle.text = ""
        lblSubTitle.text = ""
        
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        if(currentPage <= 2)
        {
            if(LanguageManager.currentLanguageIndex() == 0)
            {
                self.pageControl.currentPage = Int(currentPage)
                if Int(currentPage) == 0
                {
                    lblTitle.text = NSLocalizedString("WELCOME_ONE_TITLE", comment:"")
                    lblSubTitle.text = NSLocalizedString("WELCOME_ONE_SUBTITLE", comment:"")
                }
                else if Int(currentPage) == 1
                {
                    lblTitle.text = NSLocalizedString("WELCOME_TWO_TITLE", comment:"")
                    lblSubTitle.text = NSLocalizedString("WELCOME_TWO_SUBTITLE", comment:"")
                }
                else if Int(currentPage) == 2
                {
                    lblTitle.text = NSLocalizedString("WELCOME_THREE_TITLE", comment:"")
                    lblSubTitle.text = NSLocalizedString("WELCOME_THREE_SUBTITLE", comment:"")
                }
            }
            else
            {
                if(currentPage == 0)
                {
                    self.pageControl.currentPage = 2
                    lblTitle.text = NSLocalizedString("WELCOME_THREE_TITLE", comment:"")
                    lblSubTitle.text = NSLocalizedString("WELCOME_THREE_SUBTITLE", comment:"")
                }
                else if(currentPage == 2)
                {
                    self.pageControl.currentPage = 0
                    lblTitle.text = NSLocalizedString("WELCOME_ONE_TITLE", comment:"")
                    lblSubTitle.text = NSLocalizedString("WELCOME_ONE_SUBTITLE", comment:"")
                }
                else
                {
                    self.pageControl.currentPage = Int(currentPage)
                    lblTitle.text = NSLocalizedString("WELCOME_TWO_TITLE", comment:"")
                    lblSubTitle.text = NSLocalizedString("WELCOME_TWO_SUBTITLE", comment:"")
                }
            }
        }
    }
    
    // IBAction Methods
    
    @IBAction func continueAsGuest()
    {
        //DEV
        UserDefaults.standard.set(true, forKey: "GUEST_USER")
        UserDefaults.standard.synchronize()
        
        isGuestUser = true
        APPDELEGATE.addTabBar()
    }
    
    @IBAction func signIn()
    {
        isGuestUser = false
        
        UserDefaults.standard.set(false, forKey: "GUEST_USER")
        UserDefaults.standard.synchronize()
        
        let signInVC = SignInVC()
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @IBAction func signUp()
    {
        isGuestUser = false
        
        UserDefaults.standard.set(false, forKey: "GUEST_USER")
        UserDefaults.standard.synchronize()
        
        let signInVC = SignUpVC()
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @IBAction func englishBtnTapped(_ sender: Any){
        
        LanguageManager.saveLanguage(by: 0)
        UserDefaults.standard.set(true, forKey: "Language_Class")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func arebicBtnTapped(_ sender: Any) {
    
        LanguageManager.saveLanguage(by: 1)
        UserDefaults.standard.set(true, forKey: "Language_Class")
        UserDefaults.standard.synchronize()
    }
}
