
//
//  ForgotPasswordVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 04/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import IQDropDownTextField
import MBProgressHUD

class ForgotPasswordVC: UIViewController
{
    @IBOutlet weak var  txtNumber           : UITextField?
    @IBOutlet weak var  txtCode             : UITextField?
    @IBOutlet weak var  codePicker          : UIPickerView?
    @IBOutlet weak var  btnVerify           : UIButton?
    @IBOutlet weak var  lblTitle            : UILabel?
    @IBOutlet weak var  lblTitle1           : UILabel?
    @IBOutlet weak var  lblMobileNumber     : UILabel?
    @IBOutlet weak var  lblPasswordChanged  : UILabel?
    @IBOutlet weak var  lblPasswordUpdate   : UILabel?
    @IBOutlet weak var  btnOk               : UIButton!
    
    var isSignUp: Bool?
    var isResend: Bool?
    var codeAry : NSArray?
    var index   : Int = 0

    var isFromConfirmBooking = false
    
    var codeDictMobileChange : NSMutableDictionary = [:]
    var codeDict : NSMutableDictionary = [:]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        //if (!isSignUp!)
        //{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        //}
        //else
        //{
        //   self.navigationItem.setHidesBackButton(true, animated: false)
        //}
        
        codeAry = NSArray.init(contentsOfFile: Bundle.main.path(forResource: "Country", ofType: "plist")!)!
        
        if(isSignUp)!
        {
            self.title = NSLocalizedString("Change Mobile Number", comment: "")
            lblTitle?.text = NSLocalizedString("TO_GET_STARTED", comment: "")
        }
        else
        {
            self.title = NSLocalizedString("FORGOT_PASSWORD_TITLE", comment: "")
            lblTitle?.text = NSLocalizedString("TO_GET_PASSWORD_RESET_CODE", comment: "")
        }
        
        lblTitle1?.text = NSLocalizedString("ENTER_YOUR_MOBILE_NUMBER", comment: "")
        lblMobileNumber?.text = NSLocalizedString("MOBILE_NUMBER", comment: "")
        lblPasswordChanged?.text = NSLocalizedString("PASSWORD_CHANGED", comment: "")
        lblPasswordUpdate?.text = NSLocalizedString("PASSWORD_UPDATE_MESSAGE", comment: "")
        btnVerify?.setTitle(NSLocalizedString("SEND_VERIFICATION_CODE", comment: ""), for: UIControlState.normal)
        
        btnOk?.setTitle(NSLocalizedString("OK_TEXT", comment: ""), for: UIControlState.normal)
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            txtCode?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtNumber?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            
            txtCode?.textAlignment = NSTextAlignment.left
            txtNumber?.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtCode?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtNumber?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        
            txtCode?.textAlignment    = NSTextAlignment.right
            txtNumber?.textAlignment     = NSTextAlignment.right
        }
        
        //for countryDict: [AnyHashable: Any] in codeAry
        var strCode : NSString
        var i: Int = 0
        
        if isSignUp!
        {
            strCode = codeDictMobileChange.object(forKey: "country_code") as! NSString
            strCode = strCode.replacingOccurrences(of: "+", with: "") as NSString
        }
        else
        {
            strCode = CommonFunctions.getCallingCodeString() as NSString
        }
        
        print("strCode = ", strCode)
        
        for countryDict in codeAry as! [NSDictionary]
        {
            var strCCode: String = countryDict["isd_code"] as? String ?? ""
            strCCode = strCCode.replacingOccurrences(of: " ", with: "")
            strCCode = (strCCode.replacingOccurrences(of: "+", with: "") as NSString) as String
            
            if (strCCode == strCode as String)
            {
                index = i
                
                let dict = codeAry?.object(at: index) as! NSDictionary
                let str  = dict["isd_code"] as? String
                
                txtCode?.text = "+" + str!

                codePicker?.selectRow(i, inComponent: 0, animated: false)
                
                break
            }
            
            i += 1
        }

        txtCode?.inputView = codePicker
        
        btnVerify?.layer.cornerRadius = 5.0
        btnVerify?.layer.masksToBounds = true
        
        if isSignUp!
        {
            self.txtNumber?.text = codeDictMobileChange.object(forKey: "mobile") as? String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendCode()
    {
        RESIGN_KEYBOARD
        self.view.endEditing(true)
        
        //self.txtCode?.resignFirstResponder()
        self.txtNumber?.resignFirstResponder()
        
        if(validateUserInput())
        {
            if(CommonFunctions.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                isSignUp! ? changeMobileNoApi() : forgotPwdApi()
            }
        }
    }

    // MARK: Validation
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtNumber?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_PHONE", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtNumber?.becomeFirstResponder()
            })
        }
        
        return isError
    }
    
    func forgotPwdApi()
    {
        let service = CustomerService()
        let number : NSString = (txtNumber?.text! as NSString?)!
        
        let dic : NSMutableDictionary = [
            
            "mobile"          : number,
            "country_code"    : txtCode?.text ?? ""
        ]
        
        service.forgotPwdAPIRequestWithParameters(dic, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                     let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        USERDEFAULT.set(data?.object(forKey: "temp_token"), forKey: "temp_token")
                        
                        let varVC = VerificationVC()
                        
                        varVC.isSignUp = self.isSignUp!
                        varVC.codeDict = dic
                        varVC.isFromConfirmBooking = self.isFromConfirmBooking
                        
                        self.navigationController?.pushViewController(varVC, animated: true)
                    }
                    else
                    {
                        CommonFunctions.showAlertWithTitle(title: "", message: (data?.object(forKey: "msg") as! String), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                            
                        })
                    }
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    CommonFunctions.showAlertWithTitle(title: "", message: (data?.object(forKey: "msg") as! String), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
        })
        { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func changeMobileNoApi()
    {
        let service = CustomerService()
        
        let number = txtNumber?.text!
        
        let dict1 = codeAry?.object(at: index) as! NSDictionary
        let str  = dict1["country_code"] as? String
        
        let dict : NSMutableDictionary = [
            
            "mobile" : number ?? "",
            "country_code"    : self.txtCode?.text ?? "",
            "id"      : codeDictMobileChange.object(forKey: "id") as! String,
            "iso_code"        : str ?? ""
        ]
        
        codeDictMobileChange = dict
        
        service.changeMobileNumberAPIRequestWithParameters(dict, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        let data1 = data?.object(forKey: "data") as! [String : Any]
                        
                        USERDEFAULT.set(data1["temp_token"] as! NSString, forKey: "temp_token")
                        
                        CommonFunctions.showAlertWithTitle(title: "", message: (data?.object(forKey: "msg") as! String), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                            
                            let varVC = VerificationVC()
                            
                            varVC.isSignUp = self.isSignUp!
                            varVC.codeDictMobileChange = self.codeDictMobileChange
                            varVC.codeDict = self.codeDict
                            varVC.isFromConfirmBooking = self.isFromConfirmBooking
                            
                            self.navigationController?.pushViewController(varVC, animated: true)
                        })
                    }
                    else
                    {
                        CommonFunctions.showAlertWithTitle(title: "", message: (data?.object(forKey: "msg") as! String), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                            
                        })
                    }
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    CommonFunctions.showAlertWithTitle(title: "", message: (data?.object(forKey: "msg") as! String), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
        })
        { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension ForgotPasswordVC : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField == txtCode)
        {
            let dict = codeAry?.object(at: index) as! NSDictionary
            let str  = dict["isd_code"] as? String
            
            txtCode?.text = "+" + str!
        }
        else
        {
            textField.resignFirstResponder()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        if textField  == txtNumber
        {
            if range.location == 11 { return false }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            return checkEnglishPhoneNumberFormat(string: string, str: str, textField: textField)
        }
        
        return true
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?, textField: UITextField) -> Bool
    {
        if string == ""
        {
            //BackSpace
            return true
        }
        else if str!.count < 3
        {
            
        }
        else if str!.count == 4
        {
            textField.text = textField.text! + "-"
        }
        else if str!.count == 8
        {
            textField.text = textField.text! + "-"
        }
        else if str!.count > 14
        {
            return false
        }
        
        return true
    }
}

extension ForgotPasswordVC : UIPickerViewDelegate, UIPickerViewDataSource
{
    //MARK: UIPickerViewDataSource methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return codeAry!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: SCREEN_WIDTH - 20, height: 44))
        label.textAlignment = .center
        label.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        
        let dict = codeAry?.object(at: row) as! NSDictionary
        let str1 = dict["isd_code"] as! String
        let str2 = dict["country_name"] as! String
        
        label.text =  str1 + " " + str2
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        index = Int(row)
    }
}
