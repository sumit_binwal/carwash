//
//  SignInVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 04/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class SignInVC: UIViewController
{
    @IBOutlet fileprivate var  txtMail          : UITextField!
    @IBOutlet fileprivate var  txtPwd           : UITextField!
    @IBOutlet fileprivate var  scrollView       : UIScrollView!
    @IBOutlet weak        var  emailAddressTitle: UILabel!
    @IBOutlet weak        var  passwordTitle    : UILabel!
    @IBOutlet weak        var  forgotPasswordButton: UIButton!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var accountLabel: UILabel!
    
    @IBOutlet weak var guestButton: UIButton!
    
    var isFromConfirmBooking = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        scrollView.isScrollEnabled = false
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            txtMail.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            txtPwd.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            
            txtMail.textAlignment = NSTextAlignment.left
            txtPwd.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtMail.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            txtPwd.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            
            txtMail.textAlignment = NSTextAlignment.right
            txtPwd.textAlignment = NSTextAlignment.right
        }
        
        self.txtMail?.placeholder = NSLocalizedString("PLACEHOLDER_EMAIL", comment: "")
        self.txtPwd?.placeholder = NSLocalizedString("PLACEHOLDER_PASSWORD", comment: "")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.title = NSLocalizedString("SIGN_IN", comment: "")
        
        self.emailAddressTitle.text = NSLocalizedString("EMAIL_ADDRESS_TITLE", comment: "")
        self.passwordTitle.text = NSLocalizedString("PASSWORD_TITLE", comment: "")
        self.forgotPasswordButton.setTitle(NSLocalizedString("FORGOT_PASSWORD", comment: ""), for: UIControlState.normal)
        
        self.guestButton.setTitle(NSLocalizedString("CONTINUE_AS_GUEST", comment: ""), for: UIControlState.normal)
        
        self.signUpButton.setTitle(NSLocalizedString("SIGN_UP", comment: ""), for: UIControlState.normal)
        
        accountLabel.text = NSLocalizedString("Don’t have an account yet?", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        if isFromConfirmBooking
        {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        }
        else
        {
            self.navigationItem.hidesBackButton = true
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:  UIImage(named: "right"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonPressed))
    }

    func backButtonPressed()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Instance Methods
    
    func setUpGesture()
    {
        let tapGesture: UITapGestureRecognizer
        tapGesture =  UITapGestureRecognizer.init(target: self, action: #selector(handleTapGesture(_:)))
        tapGesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    //MARK: Used to hide keyboard
    
    func handleTapGesture(_ sender: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func guestButtonAction(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "GUEST_USER")
        UserDefaults.standard.synchronize()
        
        isGuestUser = true
        APPDELEGATE.addTabBar()
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        
        isGuestUser = false
        
        UserDefaults.standard.set(false, forKey: "GUEST_USER")
        UserDefaults.standard.synchronize()
        
        let signInVC = SignUpVC()
        signInVC.isFromConfirmBooking = isFromConfirmBooking
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    //MARK: @IBAction Methods
    
    @IBAction func forgotPwd()
    {
        self.view.endEditing(true)
        
        let signInVC = ForgotPasswordVC()
        signInVC.isSignUp = false
        signInVC.isFromConfirmBooking = isFromConfirmBooking
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtMail?.text?.isEmpty)!
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_EMAIL", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
            
            })
        }
        else if !(Validation.validateEmail(email: txtMail?.text))
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("INVALID_EMAIL", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
        else if(txtPwd?.text?.isEmpty)!
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_PASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
        return isError
    }
    
    func doneButtonPressed()
    {
        RESIGN_KEYBOARD
        if (validateUserInput())
        {
            if(CommonFunctions.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                loginFirebase()
            }
        }
    }
    
    func loginFirebase()
    {
        User.loginUser(withEmail: txtMail.text!, password: "123456") { (isLogedIn, errorCode) in
            
            print("isLogedIn = \(isLogedIn)")
            print("errorCode = \(errorCode)")
            
            if isLogedIn == true
            {
                self.loginApi()
            }
            else
            {
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if errorCode == "userNotFound"
                {
                    CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("USER_NOT_FOUND", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
                else
                {
                    CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
            }
        }
    }
    
    //MARK: API Call
    func loginApi()
    {
        let service1 = CustomerService()
        
        if (USERDEFAULT.object(forKey: "FCM_Token") != nil)
        {
            device_token = USERDEFAULT.object(forKey: "FCM_Token") as! String as NSString
        }
        
        let dic : NSMutableDictionary = [
            
            "identifier"      : txtMail?.text ?? "",
            "password"        : txtPwd?.text ?? "",
            "device_token"    : device_token
        ]
        
        service1.loginAPIRequestWithParameters(dic, success:
        { (response, data) in
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    let dict = data?.object(forKey: "data") as! NSDictionary
                    print("dict = \(dict)")
                    
                    let name = dict.object(forKey: "name") as! String
                    let fcm_user_id = dict.object(forKey: "fcm_user_id") as! String
                    
                    User.updateUserInformation(withName: name, withEmail: self.txtMail.text!, withId: fcm_user_id, device_token: device_token as String, completion: { (success) in
                        
                        if (success)
                        {
                            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                            
                            let str = dict.object(forKey: "user_role") as! NSNumber
                            USERDEFAULT.set(NSKeyedArchiver.archivedData(withRootObject: data?.object(forKey: "data") as! NSDictionary), forKey: "USER_DATA")
                            USERDEFAULT.synchronize()
                            
                            if(str == 27)
                            {
                                APPDELEGATE.addTabBarForServiceProvider()
                            }
                            else
                            {
                                if self.isFromConfirmBooking
                                {
                                    self.navigationController?.dismiss(animated: true, completion: nil)
                                }
                                else
                                {
                                    APPDELEGATE.addTabBar()
                                }
                            }
                        }
                        else
                        {
                            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                                
                            })
                        }
                    })
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        if(data!["temp_token"] != nil)
                        {
                            let str : NSString = data?.object(forKey: "temp_token") as! NSString
                            
                            if(str.length > 0)
                            {
                                USERDEFAULT.set(data?.object(forKey: "temp_token"), forKey: "temp_token")
                                
                                let forgotVC = ForgotPasswordVC()
                                
                                forgotVC.isSignUp = false
                                forgotVC.isResend = true
                                forgotVC.isFromConfirmBooking = self.isFromConfirmBooking
                                
                                self.navigationController?.pushViewController(forgotVC, animated: true)
                            }
                        }
                    })
                }
            }
            else
            {
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    
                })
            }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension SignInVC : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    @IBAction func textFieldTextChange(_ textField: UITextField)
    {
        //   txtPwd.text = setCharacterSpacig(string: txtPwd.text!) as String
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        //if !string.canBeConverted(to: String.Encoding.ascii){
        //    return false
        //}
        
        return true
    }
    
    func setCharacterSpacig(string:String) -> NSString
    {
        let attributedStr = NSMutableAttributedString(string: string)
        attributedStr.addAttribute(NSKernAttributeName, value: 3, range: NSMakeRange(0, attributedStr.length))
        print("attributedStr = ", attributedStr)
        return attributedStr.string as NSString
    }
}
