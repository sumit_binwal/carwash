//
//  MyAccountVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 12/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class MyAccountVC: UIViewController
{
    @IBOutlet var tblService    : UITableView?
    @IBOutlet weak var btnSwitchAccountHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var btnSwitchAccount: UIButton!
    @IBOutlet var langPicker         : UIPickerView?
    
    var coverImagesArr : [String] = []
    
    var isOn  : Bool = false
    var accAry   = [[String]]()
    var userDict : NSDictionary = [:]
    var userRoleNo = NSNumber()
    var langIndex   : Int = 0
    var langAry     : NSMutableArray = ["English", "Arabic"];
    
    var profileCoverCollectionView : UICollectionView?
    var profileCoverPageControl : UIPageControl?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
         userRoleNo = dict.object(forKey: "user_role") as! NSNumber
        
        if (userRoleNo == 27)
        {
            btnSwitchAccount.isHidden = true
            btnSwitchAccountHeightConstaint.constant = 0
        }
        else
        {
            btnSwitchAccount.isHidden = false
            btnSwitchAccountHeightConstaint.constant = 43
        }
        
        btnSwitchAccount.setTitle(NSLocalizedString("SWITCH_ACCOUNT_BUTTON", comment: ""), for: UIControlState.normal)
        
        let groupA = [NSLocalizedString("NAME_TEXT", comment: ""), NSLocalizedString("EMAIL_ADDRESS_TITLE", comment: ""), NSLocalizedString("MOBILE_NUMBER", comment: "")]
        //let groupB = [NSLocalizedString("LOYALTY_POINTS", comment: ""), NSLocalizedString("LANGUAGE_TEXT", comment: ""), NSLocalizedString("NOTIFICATIONS_TEXT", comment: "")]
        let groupB = [NSLocalizedString("LANGUAGE_TEXT", comment: ""), NSLocalizedString("NOTIFICATIONS_TEXT", comment: "")]
        let groupC = [NSLocalizedString("CHANGE_PASSWORD", comment: "")]
        
        // then add them all to the "groups" array
        accAry.append(groupA)
        accAry.append(groupB)
        accAry.append(groupC)

        // for layout issue in ios 11
        if #available(iOS 11.0, *) {
            tblService?.contentInsetAdjustmentBehavior = .never
        }
        
        tblService?.register(UINib(nibName: "AccCell", bundle: nil), forCellReuseIdentifier: "AccCell")
        tblService?.register(UINib(nibName: "ProfileCell", bundle : nil), forHeaderFooterViewReuseIdentifier: "ProfileCell")
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        setUpNavigationBar()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        userProfile()
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("MY_ACCOUNT_TITLE", comment: "")

        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:  UIImage(named: "edit"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(editButtonPressed))
    }

    func editButtonPressed()
    {
        let editVC = EditMyAccount()
        editVC.profileDict = userDict
        self.navigationController?.pushViewController(editVC, animated: true)
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchAccount()
    {
        let obj = CreateProfileVC()
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func doneAction()
    {
        self.view.endEditing(true)
        self.ChangeLanguageApi()
    }
    
     func changeNotificationValue()
     {
        if(isOn)
        {
            //notififcationSwitch?.setOn(false, animated: true)
            isOn = false
            self.notificationOnOffApi()
        }
        else
        {
            //notififcationSwitch?.setOn(true, animated: true)
            isOn = true
            self.notificationOnOffApi()
        }
    }
    
    //MARK:  API Call
    
    func userProfile()
    {
        let service = CustomerService()
    
        service.profileRequestWithParameters(nil, success:
        { (response, data) in
        
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    self.userDict = (data?.object(forKey: "data") as? NSDictionary)!
                    self.showData()
                }
            }
        })
        { (response, error) in
            
        }
    }

    func showData()
    {
        tblService?.reloadData()
        
        let headerView = UIView()
        let headerCell: ProfileCell = tblService!.dequeueReusableHeaderFooterView(withIdentifier: "ProfileCell") as! ProfileCell
        
        if userRoleNo == 27
        {
            headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 200 * scaleFactorX)
            headerCell.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 200 * scaleFactorX)
        }
        else
        {
            headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 125 * scaleFactorX)
            headerCell.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 125 * scaleFactorX)
        }
        
        // avatar_url
        
        let imageStr = userDict.object(forKey: "avatar") as! String
        
        var imageUrlStr = ""
        if imageStr.contains("http") {
            imageUrlStr = imageStr
        } else {
            imageUrlStr = String(format:"%@/user_images/%@", BASE_URL, imageStr)
        }
    
        let url = URL(string: imageUrlStr)
        
        print("url = ", url ?? "")
        
        headerCell.imgProfile?.setIndicatorStyle(.gray)
        headerCell.imgProfile?.setShowActivityIndicator(true)
        
        headerCell.imgProfile?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "group20"))
        
        headerCell.imgProfile?.layer.cornerRadius = ((headerCell.imgProfile?.frame.size.width)!/2)

        headerCell.imgProfile?.layer.masksToBounds = true
        
        headerCell.imgProfile?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        headerCell.imgProfile?.layer.borderWidth = 1.0
        
        headerCell.profilePageControl.isHidden = true
        headerCell.profileCollectionView.isHidden = true
        headerCell.coverImgProfile?.isHidden = true
        
        if (userRoleNo == 27)
        {
            headerCell.profilePageControl.isHidden = false
            headerCell.profileCollectionView.isHidden = false
            
            coverImagesArr = userDict.object(forKey: "cover_image") as! [String]
            
            profileCoverCollectionView = headerCell.profileCollectionView
            
            profileCoverCollectionView?.register(UINib.init(nibName: "CoverImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CoverImageCollectionViewCellIdentifiler")
            
            profileCoverCollectionView?.delegate = self
            profileCoverCollectionView?.dataSource = self
            
            profileCoverPageControl = headerCell.profilePageControl
            profileCoverPageControl?.numberOfPages = coverImagesArr.count
            
            profileCoverCollectionView?.reloadData()
        }

        headerView.addSubview(headerCell)
        
        tblService?.tableHeaderView = headerView
        
        if userRoleNo == 27
        {
            tblService?.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 200 * scaleFactorX)
        }
        else
        {
            tblService?.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 125 * scaleFactorX)
        }
    }
    
    // Notification ON OFF api
    
    func notificationOnOffApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary =
            [
                "notification_setting" : isOn
            ]
     
        service.notificationOnOffApiRequestWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                        {
                            self.userProfile()
                    }
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                    })
                }
            }
            else
            {
                let msg = data?.object(forKey: "msg") as! NSString
                
                AlertViewController.showAlertWith(title: "", message: msg as String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    // Language change api
    
    func ChangeLanguageApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary =
            [
                "language" : langIndex == 0 ? "en" : "ar"
        ]
        
        service.changeLanguageApiRequestWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    self.userProfile()
                    
                    if(self.langIndex == 0)
                    {
                        language = "en"
                        LanguageManager.saveLanguage(by: 0)
                        UserDefaults.standard.set(true, forKey: "Language_Class")
                        UserDefaults.standard.synchronize()
                    }
                    else
                    {
                        language = "ar"
                        LanguageManager.saveLanguage(by: 1)
                        UserDefaults.standard.set(true, forKey: "Language_Class")
                        UserDefaults.standard.synchronize()
                    }
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    })
                }
            }
            else
            {
                let msg = data?.object(forKey: "msg") as! NSString
                
                AlertViewController.showAlertWith(title: "", message: msg as String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension MyAccountVC : UITextFieldDelegate
{
    // MARK: UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        langPicker?.selectRow(langIndex, inComponent: 0, animated: true)
        langPicker?.reloadAllComponents()
    }
}

extension MyAccountVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return accAry.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  accAry[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccCell") as! AccCell?
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            cell?.txtValue?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            
            cell?.txtValue?.textAlignment = NSTextAlignment.right
            cell?.lblValue?.textAlignment = NSTextAlignment.right
        }
        else
        {
            cell?.txtValue?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            
            cell?.txtValue?.textAlignment = NSTextAlignment.left
            cell?.lblValue?.textAlignment = NSTextAlignment.left
        }
        
        cell?.imgAdditional?.isHidden = true
        cell?.txtValue?.isHidden = false
        cell?.notificationSwitch?.isHidden = false
        cell?.imgCheck?.isHidden = false
        cell?.lblValue?.isHidden = false
        cell?.imgChange?.isHidden = true
        cell?.txtValue?.isUserInteractionEnabled = false
        
        if(indexPath.section == 0)
        {
            cell?.leading.constant = 15;
            cell?.trailing.constant = 15;
            
            cell?.notificationSwitch?.isHidden = true
            cell?.imgCheck?.isHidden = true
            cell?.lblValue?.isHidden = false
            
            if(indexPath.row == 0)
            {
                if (userRoleNo == 27)
                {
                    let dict =  userDict.object(forKey: "provider_name") as? NSDictionary
                    if(language == "en")
                    {
                        cell?.lblValue?.text = dict?.object(forKey: "en") as? String
                    }
                    else
                    {
                        cell?.lblValue?.text = dict?.object(forKey: "ar") as? String
                    }
                }
                else
                {
                    cell?.lblValue?.text = userDict.object(forKey: "name") as? String
                }
            }
            else if(indexPath.row == 1)
            {
                cell?.lblValue?.text = userDict.object(forKey: "email") as? String
            }
            else if((userDict.object(forKey: "mobile")) != nil)
            {
                let strCode =  userDict.object(forKey: "country_code") as? String
                let strMobile =  userDict.object(forKey: "mobile") as? String
                
                cell?.lblValue?.text =  strCode! +  " " + strMobile!
            }
        }
        else if(indexPath.section == 1)
        {
            cell?.leading.constant = 0;
            cell?.trailing.constant = 0;
            cell?.txtValue?.isHidden = true
            
            if(indexPath.row == 0)
            {
                cell?.notificationSwitch?.isHidden = true
                cell?.imgCheck?.isHidden = true
                cell?.txtValue?.isHidden = false
                cell?.txtValue?.isUserInteractionEnabled = true
                cell?.lblValue?.isHidden = true
                cell?.txtValue?.inputView = langPicker
                cell?.txtValue?.delegate = self
                
                if(userDict.allKeys.count > 0)
                {
                    let language1 =  userDict.object(forKey: "language") as? String
                    
                    if language1 == "en"
                    {
                        cell?.txtValue?.text = "English"
                        langIndex = 0
                    }
                    else
                    {
                        cell?.txtValue?.text = "Arabic"
                        langIndex = 1
                    }
                }
                
                //// Loyalty Points
                //cell?.notificationSwitch?.isHidden = true
            }
            else if(indexPath.row == 1)
            {
                cell?.txtValue?.isHidden = true
                cell?.txtValue?.isUserInteractionEnabled = false
                cell?.lblValue?.isHidden = true
                cell?.imgCheck?.isHidden = true
                
                if(userDict.allKeys.count > 0)
                {
                    cell?.notificationSwitch?.tintColor = UIColor.clear
                    cell?.notificationSwitch?.backgroundColor = UIColor.uicolorFromRGB(168, 168, 168)
                    cell?.notificationSwitch?.layer.cornerRadius = 16
                    
                    cell?.notificationSwitch?.addTarget(self, action: #selector(changeNotificationValue), for: UIControlEvents.touchUpInside)
                    
                    isOn =  userDict.object(forKey: "notification_setting") as! Bool
                    cell?.notificationSwitch?.isOn = isOn
                    
                    cell?.notificationSwitch?.isUserInteractionEnabled = true
                }
                
                //cell?.notificationSwitch?.isHidden = true
                //cell?.imgCheck?.isHidden = true
                //cell?.txtValue?.isHidden = false
                //cell?.txtValue?.isUserInteractionEnabled = true
                //cell?.lblValue?.isHidden = true
                //cell?.txtValue?.inputView = langPicker
                //cell?.txtValue?.delegate = self
                //
                //if(userDict.allKeys.count > 0)
                //{
                //    let language1 =  userDict.object(forKey: "language") as? String
                //
                //    if language1 == "en"
                //    {
                //        cell?.txtValue?.text = "English"
                //        langIndex = 0
                //    }
                //    else
                //    {
                //        cell?.txtValue?.text = "Arabic"
                //        langIndex = 1
                //    }
                //}
            }
            //else
            //{
                //cell?.txtValue?.isHidden = true
                //cell?.txtValue?.isUserInteractionEnabled = false
                //cell?.lblValue?.isHidden = true
                //cell?.imgCheck?.isHidden = true
                //
                //if(userDict.allKeys.count > 0)
                //{
                //    cell?.notificationSwitch?.tintColor = UIColor.clear
                //    cell?.notificationSwitch?.backgroundColor = UIColor.uicolorFromRGB(168, 168, 168)
                //    cell?.notificationSwitch?.layer.cornerRadius = 16
                //
                //    cell?.notificationSwitch?.addTarget(self, action: #selector(changeNotificationValue), for: UIControlEvents.touchUpInside)
                //
                //    isOn =  userDict.object(forKey: "notification_setting") as? Bool
                //    cell?.notificationSwitch?.isOn = isOn!
                //
                //    cell?.notificationSwitch?.isUserInteractionEnabled = true
                //}
            //}
        }
        else
        {
            cell?.notificationSwitch?.isHidden = true
            cell?.imgCheck?.isHidden = true
            cell?.lblValue?.isHidden = true
            cell?.txtValue?.isHidden = true
            cell?.imgChange?.isHidden = false
            cell?.lblTitle?.textColor = UIColor.uicolorFromRGB(53, 53, 53)
        }
        
        let ary = accAry[indexPath.section]
        cell?.lblTitle?.text = ary[indexPath.row]
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!;
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.section == 2)
        {
            let changeVC = ChangePasswordVC()
            self.navigationController?.pushViewController(changeVC, animated: true)
        }
        else if(indexPath.section == 1)
        {
            if(indexPath.row == 1)
            {
                let cell = tableView.cellForRow(at: indexPath) as! AccCell
                cell.txtValue?.becomeFirstResponder()
            }
        }
    }
}

extension MyAccountVC : UIPickerViewDelegate, UIPickerViewDataSource
{
    // MARK: -  PickerView Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return langAry.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: SCREEN_WIDTH - 20, height: 44))
        label.textAlignment = .center
        label.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        label.text = langAry[row] as? String
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        langIndex = row
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension MyAccountVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return coverImagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoverImageCollectionViewCellIdentifiler", for: indexPath) as! CoverImageCollectionViewCell
        
        print("coverImageArr[indexPath.row] = ", coverImagesArr[indexPath.row])
        
        cell.coverImageView.setIndicatorStyle(.gray)
        cell.coverImageView.setShowActivityIndicator(true)
        
        cell.coverImageView.sd_setImage(with: URL.init(string: coverImagesArr[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "group29"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // your code here
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let indexPath = profileCoverCollectionView?.indexPathsForVisibleItems.first
        profileCoverPageControl?.currentPage = (indexPath?.row)!
    }
}

