//
//  MessagesVC.swift
//  CarWash
//
//  Created by Suman Payal on 15/12/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD
import IQKeyboardManagerSwift

class MessagesVC: UIViewController {

    @IBOutlet weak var chatTableView: UITableView!
    
    @IBOutlet weak var inputTextView: UITextView!
    
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    var messagesArr = [Message]()
    let barHeight: CGFloat = 50
    
    let dateFormatter = DateFormatter()
    
    var selected_user_id : String = ""
    var selected_user_name : String = ""
    var device_token_string : String = ""
    
    @IBOutlet weak var constraintContainerBottom: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        chatTableView?.register(UINib(nibName: "ReceiverTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiverTableViewCell")
        chatTableView?.register(UINib(nibName: "SenderTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderTableViewCell")
        
        customization()
        
        // Keyobard frame change notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textInputModeChangedNotification), name: NSNotification.Name.UITextInputCurrentInputModeDidChange, object: nil)
    }
    
    //MARK: -
    func textInputModeChangedNotification()
    {
        print(UITextInputMode .activeInputModes)
        self.inputTextView.reloadInputViews()
    }
    
    //MARK:- Notification Keyboard Frame Change
    func keyboardWillChangeFrame(notification: Notification)
    {
        if let userInfo = notification.userInfo
        {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height
            {
                self.constraintContainerBottom.constant = 0.0
                self.tableBottomConstraint.constant = 60.0
            }
            else
            {
                self.constraintContainerBottom.constant = endFrame?.size.height ?? 0.0
                self.tableBottomConstraint.constant = ((endFrame?.size.height)! + 60.0)
                
                if messagesArr.count > 0
                {
                    //self.chatTableView?.reloadData()
                    
                    self.chatTableView.scrollToRow(at: IndexPath(row: self.messagesArr.count - 1, section: 0), at: .bottom, animated: true)
                }
            }
            
            weak var weakSelf = self
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations:
            {
                weakSelf?.view.layoutIfNeeded()
            },
            completion: nil)
        }
    }
    
    override var canBecomeFirstResponder: Bool
    {
        return true
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextInputCurrentInputModeDidChange, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        let customView = UIView()
        inputTextView.inputAccessoryView = customView
        inputTextView.keyboardDistanceFromTextField = 8
        
        IQKeyboardManager.sharedManager().enable = false
        
        setUpNavigationBar()
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            inputTextView.textAlignment = NSTextAlignment.left
        }
        else
        {
            inputTextView.textAlignment = NSTextAlignment.right
        }
        
        chatTableView?.delegate = self
        chatTableView?.dataSource = self
        
        User.setUserStatusOnline(completion: { (_) in
        })
        
        UserDefaults.standard.set("1", forKey: "isChatScreen")
        UserDefaults.standard.synchronize()
        
        self.messagesArr.removeAll()
        
        fetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
        
        IQKeyboardManager.sharedManager().enable = true
        
        User.setUserStatusOffline (completion:{ (_) in
        })
        
        //IQKeyboardManager.sharedManager().enable = true
        
        UserDefaults.standard.set("0", forKey: "isChatScreen")
        UserDefaults.standard.synchronize()
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        self.title = "\(selected_user_name)"
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: Methods
    func customization()
    {
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm a"
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationWillEnterBackGround(notification:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object:nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationWillBecomeActive(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object:nil)
    }
    
    func applicationWillEnterBackGround(notification: NSNotification)
    {
        inputTextView.resignFirstResponder()
    
        User.setUserStatusOffline (completion:{ (_) in
        })
    }
    
    func applicationWillBecomeActive(notification: NSNotification)
    {
        User.setUserStatusOnline(completion: { (_) in
        })
    }
    
    //Downloads messages   self.currentUser!.id
    func fetchData() {
        
        print("selected_user_id = \(selected_user_id)")
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        Message.markMessagesRead(forUserID: selected_user_id)
        
        Message.downloadAllMessages(forUserID:selected_user_id , completion: {[weak weakSelf = self] (message) in
            
            weakSelf?.messagesArr.append(message)
            //weakSelf?.messagesArr.sort{ $0.timestamp < $1.timestamp }
            
            DispatchQueue.main.async {
                
                if let state = weakSelf?.messagesArr.isEmpty, state == false {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.chatTableView?.reloadData()
                    
                    self.chatTableView.scrollToRow(at: IndexPath(row: self.messagesArr.count - 1, section: 0), at: .bottom, animated: true)
                }
            }
        })
    }
    
    func animateExtraButtons(toHide: Bool)
    {
        
    }
    
    @IBAction func showMessage(_ sender: Any)
    {
        self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func showOptions(_ sender: Any)
    {
        self.animateExtraButtons(toHide: false)
    }
    
    @IBAction func sendMessage(_ sender: Any)
    {
        if let text = self.inputTextView.text
        {
            if text.count > 0
            {
                CommonFunctions.composeMessage(type: .text, content: self.inputTextView.text, device_token: device_token_string, fcm_user_id: selected_user_id)
                self.inputTextView.text = ""
            }
        }
    }
}

extension MessagesVC : UITextViewDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
    
        return true
    }
}

extension MessagesVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.messagesArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60/375 * SCREEN_WIDTH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.messagesArr[indexPath.row].owner
        {
        
        case .receiver:
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTableViewCell") as! ReceiverTableViewCell?
            
            if cell == nil
            {
                cell = ReceiverTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ReceiverTableViewCell")
            }
            
            if (LanguageManager.currentLanguageIndex() == 0)
            {
                cell?.message.textAlignment = NSTextAlignment.right
                cell?.lblTime.textAlignment = NSTextAlignment.right
            }
            else
            {
                cell?.lblTime.textAlignment = NSTextAlignment.left
                cell?.message.textAlignment = NSTextAlignment.left
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            switch self.messagesArr[indexPath.row].type
            {
                
            case .text:
                
                cell?.message.text = self.messagesArr[indexPath.row].content as? String
                
                let time:Int? = self.messagesArr[indexPath.row].timestamp
                
                var timeStamp = String(time!)
                
                if (timeStamp.count) > 10
                {
                    let index2 = (timeStamp.index((timeStamp.startIndex), offsetBy: 10))
                    let indexStart = index2
                    
                    let indexEnd = (timeStamp.endIndex)
                    
                    timeStamp.removeSubrange(indexStart ..< indexEnd)
                }
                
                let date = Date(timeIntervalSince1970: Double(timeStamp)! )
                //print("date = \(date)")
                
                dateFormatter.locale = NSLocale.current
                
                let timeString = CommonFunctions.timeStringAgoSinceDate(date, currentDate: Date(), numericDates: false)
                //print("timeString = \(timeString)")
                
                if timeString == "Today"
                {
                    dateFormatter.dateFormat = "hh:mm a"
                    cell?.lblTime.text = dateFormatter.string(from: date)
                }
                else if timeString == "Yesterday"
                {
                    dateFormatter.dateFormat = "dd MMM yyyy"
                    cell?.lblTime.text = "Yesterday \(dateFormatter.string(from: date))"
                }
                else
                {
                    dateFormatter.dateFormat = "hh:mm a dd MMM yyyy"
                    cell?.lblTime.text = dateFormatter.string(from: date)
                }
                
            case .photo:
                
                if let image = self.messagesArr[indexPath.row].image
                {
                    cell?.messageBackground.image = image
                    cell?.message.isHidden = true
                }
                else
                {
                    self.messagesArr[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        
                        if state == true
                        {
                            DispatchQueue.main.async
                                {
                                self.chatTableView?.reloadData()
                            }
                        }
                    })
                }
                
            case .location:
                cell?.message.isHidden = true
            }
            
            return cell!
            
        case .sender:
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "SenderTableViewCell") as! SenderTableViewCell?
            
            if cell == nil
            {
                cell = SenderTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "SenderTableViewCell")
            }
            
            if (LanguageManager.currentLanguageIndex() == 0)
            {
                cell?.message.textAlignment = NSTextAlignment.left
                cell?.lblTime.textAlignment = NSTextAlignment.left
            }
            else
            {
                cell?.lblTime.textAlignment = NSTextAlignment.right
                cell?.message.textAlignment = NSTextAlignment.right
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            switch self.messagesArr[indexPath.row].type
            {
                
            case .text:
                
                cell?.message.text = self.messagesArr[indexPath.row].content as? String
                
                let time:Int? = self.messagesArr[indexPath.row].timestamp
                let epocTime = TimeInterval(time!)
                let date = Date(timeIntervalSince1970:epocTime)
                cell?.lblTime.text = dateFormatter.string(from: date)
                
            case .photo:
                
                if let image = self.messagesArr[indexPath.row].image
                {
                    cell?.messageBackground.image = image
                    cell?.message.isHidden = true
                }
                else
                {
                    self.messagesArr[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        
                        if state == true
                        {
                            DispatchQueue.main.async
                                {
                                self.chatTableView?.reloadData()
                            }
                        }
                    })
                }
                
            case .location:
                cell?.message.isHidden = true
            }
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.inputTextView.resignFirstResponder()
    }
}
