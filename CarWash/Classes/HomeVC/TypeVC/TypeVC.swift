//
//  TypeVC.swift
//  CarWash
//
//  Created by iOS on 05/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class TypeVC: UIViewController
{
    @IBOutlet weak var topConstarint: NSLayoutConstraint!
    @IBOutlet var tblList : UITableView?
    
    var okPressed: ((_ : NSString) -> Void)? = nil
    
    let ary : NSArray = [NSLocalizedString("SMALL_CARS", comment: ""), NSLocalizedString("MEDIUM_CARS", comment: ""),NSLocalizedString("BIG_CARS", comment: "")]
    
    let imgAry : NSArray = ["small", "medium", "big"]

    override func viewDidLoad()
    {
        super.viewDidLoad()
            
        setUpNavigationBar()
        
        tblList?.register(UINib(nibName: "TypeCell", bundle: nil), forCellReuseIdentifier: "TypeCell")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("SELECT_CAR_TYPE", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension TypeVC : UITableViewDataSource, UITableViewDelegate
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  ary.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TypeCell") as! TypeCell?
        
        cell?.lblTitle?.text = ary[indexPath.row] as? String
        cell?.imgCar?.image = UIImage(named: (imgAry[indexPath.row] as? String)!)
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell!
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let str =  ary[indexPath.row] as? String
        
        if (okPressed != nil)
        {
            okPressed!(str! as NSString)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}
