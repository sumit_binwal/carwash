//
//  ReviewsListVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 28/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ReviewsListVC: UIViewController
{
    @IBOutlet weak var noReviewsLabel: UILabel!
    @IBOutlet var tblReviews         : UITableView?
    
    var reviewProviderId = ""
    
    var reviewDataAry : NSMutableArray = []
    
    let refreshControl = UIRefreshControl()
    
    var pageNumber = 1
    
    var isPageRefresh = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        noReviewsLabel.isHidden = true
        
        tblReviews?.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tblReviews?.refreshControl = refreshControl
        } else {
            tblReviews?.addSubview(refreshControl)
        }
        
        noReviewsLabel.text = NSLocalizedString("NO_REVIEWS", comment: "")
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
    }

    func refreshHomeData(_ sender: Any) {
        // Fetch Weather Data
        
        pageNumber = 1
        isPageRefresh = true
        reviewDataAry.removeAllObjects()
        
        getReviewListApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        isPageRefresh = true
        pageNumber = 1
        reviewDataAry.removeAllObjects()
        getReviewListApi()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = false
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }

    func setUpNavBar()
    {
        self.title = NSLocalizedString("REVIEWS", comment: "")

        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }

    func getReviewListApi()
    {
        let service1 = CustomerService()
        
        service1.getReviewListServiceRequestWithParameters(nil, reviewProviderId as NSString, "\(pageNumber)" as NSString, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                self.refreshControl.endRefreshing()
                
                if(response?.statusCode == 200)
                {
                    if self.pageNumber == 1 {
                        self.reviewDataAry.removeAllObjects()
                    }
                    
                    if self.reviewDataAry.count == 0 {
                        self.reviewDataAry = (data?.object(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray
                    } else {
                        
                        let dataArr : NSMutableArray = (data?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                        
                        if (dataArr.count > 0) {
                            for i in 0 ..< dataArr.count {
                                let dict = dataArr.object(at: i) as! NSDictionary
                                self.reviewDataAry.add(dict)
                            }
                        }
                        else {
                            self.pageNumber = 1
                            self.isPageRefresh = false
                        }
                    }
                    
                    DispatchQueue.main.async
                        {
                            if(self.reviewDataAry.count == 0)
                            {
                                self.tblReviews?.isHidden = true
                                self.noReviewsLabel.isHidden = false
                            }
                            else
                            {
                                self.noReviewsLabel.isHidden = true
                                self.tblReviews?.isHidden = false
                                self.tblReviews?.reloadData()
                            }
                    }
                }
                else
                {
                    self.tblReviews?.isHidden = true
                    self.noReviewsLabel.isHidden = false
                    self.isPageRefresh = true
                }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            self.tblReviews?.isHidden = true
            self.noReviewsLabel.isHidden = false
            self.isPageRefresh = true
        }
    }
}

extension ReviewsListVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return reviewDataAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  SCREEN_WIDTH * 0.242
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as! ReviewCell?
        
        if reviewDataAry.count > 0
        {
            cell?.imgUser?.layer.cornerRadius = (0.133 * SCREEN_WIDTH)/2
            cell?.imgUser?.layer.masksToBounds = true
            
            cell?.imgUser?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            cell?.imgUser?.layer.borderWidth = 1.0
            
            let dict = reviewDataAry.object(at: indexPath.row) as! NSDictionary
            
            let rating = dict.object(forKey: "avg_rating") as! Double
            cell?.ratingView.rating = rating
            
            let review_des = dict.object(forKey: "review") as! String
            cell?.lblReview?.text = review_des
            
            let user_dict = dict.object(forKey: "user_id") as! NSDictionary
            
            let name = user_dict.object(forKey: "name") as! String
            cell?.lblName?.text = name
            
            let str = String(format:"%@", user_dict.object(forKey: "avatar_url") as! CVarArg) as NSString
            let url = NSURL(string: str as String)!
            
            cell?.imgUser?.setIndicatorStyle(.gray)
            cell?.imgUser?.setShowActivityIndicator(true)
            
            cell?.imgUser?.sd_setImage(with: url as URL, placeholderImage: #imageLiteral(resourceName: "group20"))
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            if (isPageRefresh && self.reviewDataAry.count-1 == indexPath.row)
            {
                pageNumber += 1
                getReviewListApi()
            }
        }
        
        return cell!
    }
}
