//
//  ProviderListModel.swift
//  CarWash
//
//  Created by Ratina on 6/12/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import Foundation

class ProviderListModel
{
    var provider_id = ""
    
    var provider_name_en = ""
    var provider_name_ar = ""
    
    var provider_email = ""
    var provider_address = ""
    var provider_image = ""
    
    var booking_compleated = 0
    
    var country_code = ""
    var iso_code = ""
    var mobile = ""
    
    var price_rating = 0.0
    var quality_rating = 0.0
    var time_rating = 0.0
    var ratings = 0.0
    
    var review_count = 0
    
    var device_token = ""
    var fcm_user_id = ""
    
    var cover_image : [String] = []
    
    var dist : [String : Any] = [:]
    //dist =
    //{
    //    calculated = "1415.423305390987";
    //    location =
    //    (
    //        "75.78528594970703",
    //        "26.91262054443359"
    //    );
    //};
    
    var services : [[String : Any]] = []
    //services =
    //(
    //    {
    //        "_id" = 5b1e70605dbeb2be56dbec63;
    //
    //        "additional_services" =
    //        (
    //            {
    //                "desc_ar" = "Gdgk;ldfh";
    //                "desc_en" = "Lsjflk skflsdg";
    //                price = 5;
    //                "title_ar" = "additional ar";
    //                "title_en" = additional;
    //            }
    //        );
    //
    //        "car_type" = medium;
    //
    //        createdAt = "2018-06-11T12:51:44.551Z";
    //
    //        description =
    //        {
    //            ar = Hfhgf;
    //            en = Fgjfj;
    //        };
    //
    //        duration = 15;
    //
    //        "inc_id" = 1003;
    //
    //        isDeleted = 0;
    //
    //        isactive = 1;
    //
    //        notes =
    //        (
    //            ""
    //        );
    //
    //        price = 15;
    //
    //        requirements =
    //        (
    //            Water,
    //            Electricity
    //        );
    //
    //        "service_name" =
    //        {
    //            ar = hkjhsf;
    //            en = "package name eng";
    //        };
    //
    //        "sort_description" =
    //        {
    //            ar = "Hksjhfkjds gdshgksj\thfkdhgs";
    //            en = Fdgdfh;
    //        };
    //
    //        updatedAt = "2018-06-12T05:09:10.312Z";
    //
    //        "user_id" = 5ad9d255757011592a5f4788;
    //    }
    //);
    
    init()
    {
        provider_id = ""
        
        provider_name_ar = ""
        provider_name_en = ""
        
        provider_address = ""
        provider_image = ""
        provider_email = ""
        
        booking_compleated = 0
        
        country_code = ""
        iso_code = ""
        mobile = ""
        
        price_rating = 0.0
        quality_rating = 0.0
        time_rating = 0.0
        ratings = 0.0
        
        review_count = 0
        
        device_token = ""
        fcm_user_id = ""
        
        cover_image = []
        
        dist = [:]
        
        services = []
    }
    
    deinit
    {
        print("ProviderList Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let provider_id1 = dictionary["_id"] as? String
        {
            provider_id = provider_id1
        }
        
        if let provider_name1 = dictionary["provider_name"] as? [String : Any]
        {
            provider_name_en = provider_name1["en"] as! String
        }
        
        if let provider_name1 = dictionary["provider_name"] as? [String : Any]
        {
            provider_name_ar = provider_name1["ar"] as! String
        }
        
        if let email1 = dictionary["email"] as? String
        {
            provider_email = email1
        }
        
        if let avatar = dictionary["avatar"] as? String
        {
            provider_image = avatar
        }
        
        if let address = dictionary["address"] as? String
        {
            provider_address = address
        }
        
        if let booking_compleated1 = dictionary["booking_compleated"] as? Int
        {
            booking_compleated = booking_compleated1
        }
        
        if let country_code1 = dictionary["country_code"] as? String
        {
            country_code = country_code1
        }
        
        if let iso_code1 = dictionary["iso_code"] as? String
        {
            iso_code = iso_code1
        }
        
        if let mobile1 = dictionary["mobile"] as? String
        {
            mobile = mobile1
        }
        
        if let cover_image1 = dictionary["cover_image"] as? [String]
        {
            cover_image = cover_image1
        }
        
        if let device_token1 = dictionary["device_token"] as? String
        {
            device_token = device_token1
        }
        
        if let fcm_user_id1 = dictionary["fcm_user_id"] as? String
        {
            fcm_user_id = fcm_user_id1
        }
        
        if let dist1 = dictionary["dist"] as? [String : Any]
        {
            dist = dist1
        }
        
        if let price_rating1 = dictionary["price_rating"] as? Double
        {
            price_rating = price_rating1
        }
        
        if let quality_rating1 = dictionary["quality_rating"] as? Double
        {
            quality_rating = quality_rating1
        }
        
        if let time_rating1 = dictionary["time_rating"] as? Double
        {
            time_rating = time_rating1
        }
        
        if let ratings1 = dictionary["ratings"] as? Double
        {
            ratings = ratings1
        }
        
        if let review_count1 = dictionary["review_count"] as? Int
        {
            review_count = review_count1
        }
        
        if let services1 = dictionary["services"] as? [[String : Any]]
        {
            services = services1
        }
    }
}

//{
//    "_id" = 5ad9d255757011592a5f4788;
//    address = "Ganpati Nagar, Jaipur, Rajasthan";
//    avatar = "https://s3-us-west-2.amazonaws.com/car-wash/avatar/5ad9d255757011592a5f4788/avatar1525093196.jpg";
//    "booking_cancel" = 0;
//    "booking_compleated" = 0;
//    "booking_count" = 3;
//    "country_code" = "+966";
//    "cover_image" =             (
//    "https://s3-us-west-2.amazonaws.com/car-wash/cover/5ad9d255757011592a5f4788/cover1524463362.jpg",
//    "https://s3-us-west-2.amazonaws.com/car-wash/cover/5ad9d255757011592a5f4788/cover1525093161.jpg"
//    );
//    createdAt = "2018-04-20T11:43:17.837Z";
//    "device_token" = "d-PG34W-LAs:APA91bHYWFKWb5HUXQTVAsL3xdKXHtKKtyLKMp-WQ3TnUjO0zRd_Q1vLXCaq8ERZnRHgKSlGopLJ18BLq9BhBmnmWWE6PVpXhzZ4tUTh8ztFBfYDnEvz5-ZoFOsmVaN-ugLSKVRTFIeP";
//    dist =             {
//        calculated = "1415.423305390987";
//        location =                 (
//            "75.78528594970703",
//            "26.91262054443359"
//        );
//    };
//    documents =             (
//        {
//            document = "https://s3-us-west-2.amazonaws.com/car-wash/documents/5ad9d255757011592a5f4788/document1524226698.jpg";
//            name = "jklnbc,v,nb";
//        }
//    );
//    email = "caruser5@yopmail.com";
//    "fcm_user_id" = oGhIe7QR3NT0OGIZnwVmHkhEwx23;
//    "formatted_number" = 1123456568;
//    "inc_id" = 100;
//    isDeleted = 0;
//    isactive = 1;
//    "iso_code" = US;
//    language = ar;
//    location =             (
//        "75.78528594970703",
//        "26.91262054443359"
//    );
//    "login_token" = "6UxkYkkHH9rNKcNSug77sw4lIsELJ/GXXXE7jziwka3zoQ==";
//    mobile = "123-456-568";
//    name = "Car User 5";
//    "notification_setting" = 1;
//    "number_of_car" = 8;
//    "operating_hours" =             (
//        {
//            day = Sunday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    },
//        {
//            day = Monday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    },
//        {
//            day = Tuesday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    },
//        {
//            day = Wednesday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    },
//        {
//            day = Thursday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    },
//        {
//            day = Friday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    },
//        {
//            day = Saturday;
//            "is_close" = 0;
//            "shift_1" =                     {
//                close = "23:50";
//                "close_minutes" = 1430;
//                open = "01:50";
//                "open_minutes" = 110;
//            };
//    }
//    );
//    os = ios;
//    otp = "";
//    password = "$2a$10$dr4TwobweDFboXFoiXp8GeHy325brHa.KTzSesEPKT9ZwrC84dvuO";
//    "price_rating" = "2.625";
//    "provider_name" =             {
//        ar = "Nx,n ,mbm";
//        en = "Car Provider 5";
//    };
//    "quality_rating" = "2.625";
//    ratings = "2.916666666666667";
//    "review_count" = 6;
//    services =             (
//        {
//            "_id" = 5b1e70605dbeb2be56dbec63;
//            "additional_services" =                     (
//            {
//            "desc_ar" = "Gdgk;ldfh";
//            "desc_en" = "Lsjflk skflsdg";
//            price = 5;
//            "title_ar" = "additional ar";
//            "title_en" = additional;
//            }
//            );
//            "car_type" = medium;
//            createdAt = "2018-06-11T12:51:44.551Z";
//            description =                     {
//                ar = Hfhgf;
//                en = Fgjfj;
//            };
//            duration = 15;
//            "inc_id" = 1003;
//            isDeleted = 0;
//            isactive = 1;
//            notes =                     (
//                ""
//            );
//            price = 15;
//            requirements =                     (
//                Water,
//                Electricity
//            );
//            "service_name" =                     {
//                ar = hkjhsf;
//                en = "package name eng";
//            };
//            "sort_description" =                     {
//                ar = "Hksjhfkjds gdshgksj\thfkdhgs";
//                en = Fdgdfh;
//            };
//            updatedAt = "2018-06-12T05:09:10.312Z";
//            "user_id" = 5ad9d255757011592a5f4788;
//        }
//    );
//    "temp_token" = "";
//    "time_rating" = "3.5";
//    updatedAt = "2018-06-12T06:37:00.750Z";
//    "user_role" = 27;
//}
