//
//  ServiceProviderVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 20/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ServiceProviderVC: UIViewController
{
    @IBOutlet var tblService    : UITableView?
    @IBOutlet var lblCarType    : UILabel?
    @IBOutlet var lblDay        : UILabel?
    @IBOutlet var lblNoResult   : UILabel?
    
    var serviceDict : NSDictionary?
    
    let formatter = DateFormatter()
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterInnerView: UIView!
    
    @IBOutlet weak var sort_label: UILabel!
    
    @IBOutlet weak var sort_price_label: UILabel!
    @IBOutlet weak var sort_rating_label: UILabel!
    @IBOutlet weak var sort_duration_label: UILabel!
    
    @IBOutlet weak var sort_price_image: UIImageView!
    @IBOutlet weak var sort_rating_image: UIImageView!
    @IBOutlet weak var sort_duration_image: UIImageView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var providerListDataArr = [ProviderListModel]()
    
    var sortString = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        print("dict = \(String(describing: serviceDict))")
        
        lblNoResult?.isHidden = true
        //uncomment this
        lblCarType?.text = serviceDict?.value(forKey: "type") as? String
        
        var str1 = serviceDict?.value(forKey: "date")as! String
        formatter.dateFormat = "dd MMM yyyy"
        let date = formatter.date(from: str1)
        formatter.dateFormat = "dd MMM"
        str1 = formatter.string(from: date! )
        
        let str2 = serviceDict?.value(forKey: "time")as! String
        lblDay?.text = str1 + ", " + str2
        
        tblService?.register(UINib(nibName: "ProviderCell", bundle: nil), forCellReuseIdentifier: "ProviderCell")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    
        self.filterView.isHidden = true
        
        setUpNavigationBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        tblService?.setContentOffset(CGPoint.zero, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerListDataArr.removeAll()
        serviceProvidersListingApi()
        
        setUpFilterView()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpFilterView()
    {
        filterInnerView.layer.borderColor = UIColor.clear.cgColor
        filterInnerView.layer.borderWidth = 1.0
        filterInnerView.layer.cornerRadius = 5.0
        filterInnerView.layer.masksToBounds = true
        
        filterInnerView.isHidden = false
        
        submitButton.setTitle(NSLocalizedString("SUBMIT_TEXT", comment: ""), for: .normal)
        
        sort_price_label.text = NSLocalizedString("Price, lowest to highest", comment: "")
        sort_rating_label.text = NSLocalizedString("Highest rating", comment: "")
        sort_duration_label.text = NSLocalizedString("Duration, lowest to highest", comment: "")
        
        sort_label.text = NSLocalizedString("Sort By", comment: "")
        
        if sortString == "price"
        {
            sort_price_image.isHidden = false
            sort_rating_image.isHidden = true
            sort_duration_image.isHidden = true
        }
        else if sortString == "duration"
        {
            sort_price_image.isHidden = true
            sort_rating_image.isHidden = true
            sort_duration_image.isHidden = false
        }
        else if sortString == "rating"
        {
            sort_price_image.isHidden = true
            sort_rating_image.isHidden = false
            sort_duration_image.isHidden = true
        }
        else
        {
            sort_price_image.isHidden = true
            sort_rating_image.isHidden = true
            sort_duration_image.isHidden = true
        }
        
        CommonFunctions.setUIButtonBorderColor(submitButton)
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("SELECT_SERVICE_PROVIDER", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        let location1 = UIBarButtonItem(image:  #imageLiteral(resourceName: "location"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(locationPressed))
        
        let search1 = UIBarButtonItem(image:  #imageLiteral(resourceName: "search"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(searchButtonPressed))
        
        self.navigationItem.rightBarButtonItems = [location1, search1]
    }
    
    func searchButtonPressed()
    {
        let providerSearchVC = ProviderSearchVC()
        
        providerSearchVC.serviceDict = serviceDict
        
        self.navigationController?.pushViewController(providerSearchVC, animated: true)
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func locationPressed()
    {
        let mapVC = MapVC()
        
        mapVC.serviceDict = serviceDict
        mapVC.providerListDataArr = self.providerListDataArr
        
        self.navigationController?.pushViewController(mapVC, animated: true)
    }
    
    @IBAction func filter()
    {
        filterView.isHidden = false
    }
    
    @IBAction func sortSubmitButtonAction(_ sender: Any)
    {
        filterView.isHidden = true
            
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerListDataArr.removeAll()
        serviceProvidersListingApi()
    }
    
    @IBAction func sortPriceButtonAction(_ sender: Any)
    {
        sortString = "price"
        
        sort_price_image.isHidden = false
        sort_rating_image.isHidden = true
        sort_duration_image.isHidden = true
    }
    
    @IBAction func sortRatingButtonAction(_ sender: Any)
    {
        sortString = "rating"
        
        sort_price_image.isHidden = true
        sort_rating_image.isHidden = false
        sort_duration_image.isHidden = true
    }
    
    @IBAction func sortDurationCloseButtonAction(_ sender: Any)
    {
        sortString = "duration"
        
        sort_price_image.isHidden = true
        sort_rating_image.isHidden = true
        sort_duration_image.isHidden = false
    }
    
    @IBAction func filterCloseButtonAction(_ sender: Any)
    {
        sortString = ""
        
        sort_price_image.isHidden = true
        sort_rating_image.isHidden = true
        sort_duration_image.isHidden = true
        
        filterView.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerListDataArr.removeAll()
        serviceProvidersListingApi()
    }
    
    // MARK: API Call
    func serviceProvidersListingApi()
    {
        var str1 = serviceDict?.value(forKey: "date")as! String
        formatter.dateFormat = "dd MMM yyyy"
        var date = formatter.date(from: str1)
        formatter.dateFormat = "YYYY-MM-dd"
        str1 = formatter.string(from: date!)
        
        var str2 = serviceDict?.value(forKey: "time")as! String
        // split string into array
        var fullNameArr = str2.components(separatedBy: "-")
        str2 = fullNameArr[0]
        
        formatter.dateFormat = "hh:mm a"
        date = formatter.date(from: str2)
        formatter.dateFormat = "HH:mm"
        str2 = formatter.string(from: date!)
        
        var type : NSString = ""
        let str = lblCarType?.text!
        
        if(str?.isEqual(NSLocalizedString("SMALL_CARS", comment: "")))!
        {
            type = "small"
        }
        else if(str?.isEqual(NSLocalizedString("MEDIUM_CARS", comment: "")))!
        {
            type = "medium"
        }
        else if(str?.isEqual(NSLocalizedString("BIG_CARS", comment: "")))!
        {
            type = "big"
        }
        
        let dic : [String : Any] =
            [
                "latitude"          : userLattitude,
                "longitude"         : userLongtitude,
                "car_type"          : type,
                "date"              : str1,
                "time"              : str2,
                "sort"              : sortString
        ]
        
        print("dict = \(dic)")
        
        let url = BASE_URL + "home"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .post, parameters: dic, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        
                })
                
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let servicesDtaArr1 = dictionary!["data"] as? [[String : Any]]
                    
                    guard servicesDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self?.updateProviderListModelArray(usingArray: servicesDtaArr1!)
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            
                    })
                }
            }
        }
    }
    
    //MARK:- Updating provider list Model Array
    func updateProviderListModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ProviderListModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            providerListDataArr.append(model) // Adding model to array
        }
        
        if(self.providerListDataArr.count > 0)
        {
            self.lblNoResult?.isHidden = true
            self.tblService?.isHidden = false
            self.tblService?.reloadData()
        }
        else
        {
            self.lblNoResult?.isHidden = false
            self.tblService?.isHidden = true
        }
    }
}

extension ServiceProviderVC : UITableViewDataSource, UITableViewDelegate
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return providerListDataArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 266 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderCell") as! ProviderCell?
        
        cell?.imgLogo?.layer.cornerRadius  = ((cell?.imgLogo?.frame.size.height)!/2) * scaleFactorX
        cell?.imgLogo?.layer.masksToBounds = true
        
        cell?.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        cell?.imgLogo?.layer.borderWidth = 1.0
        
        cell?.btnCall?.tag = indexPath.row
        cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
        
        cell?.unreadCountView.isHidden = true
        
        cell?.unreadCountView.layer.cornerRadius = (cell?.unreadCountView.frame.size.height)!/2
        cell?.unreadCountView.layer.borderColor = UIColor.clear.cgColor
        cell?.unreadCountView.layer.borderWidth = 1.0
        cell?.unreadCountView.layer.masksToBounds = true
        
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            cell?.btnMail?.isHidden = false
            
            Conversation.showProviderUnReadCount(from_fcm_user_id: providerListDataArr[indexPath.row].fcm_user_id, completion: { (unreadcount) in
                
                if unreadcount == "0"
                {
                    cell?.unreadCountLabel.text = ""
                    cell?.unreadCountView.isHidden = true
                }
                else
                {
                    cell?.unreadCountView.isHidden = false
                    cell?.unreadCountLabel.text = unreadcount
                }
            })
        }
        else
        {
            cell?.btnMail?.isHidden = true
        }
        
        cell?.btnMail?.tag = indexPath.row
        cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
        
        if language == "en"
        {
            cell?.lblName?.text = providerListDataArr[indexPath.row].provider_name_en
        }
        else
        {
            cell?.lblName?.text = providerListDataArr[indexPath.row].provider_name_ar
        }
        
        cell?.lblName?.text = cell?.lblName?.text?.uppercased()
        
        cell?.imgService?.setShowActivityIndicator(true)
        cell?.imgService?.setIndicatorStyle(.gray)
        
        cell?.imgLogo?.setShowActivityIndicator(true)
        cell?.imgLogo?.setIndicatorStyle(.gray)
        
        if providerListDataArr[indexPath.row].cover_image.count > 0
        {
            let str1 = providerListDataArr[indexPath.row].cover_image[0]
            cell?.imgService?.sd_setImage(with: URL.init(string: str1), placeholderImage: #imageLiteral(resourceName: "group29"))
        }
        else
        {
            cell?.imgService?.image = #imageLiteral(resourceName: "group29")
        }
        
        cell?.imgLogo?.sd_setImage(with: URL.init(string: providerListDataArr[indexPath.row].provider_image), placeholderImage: #imageLiteral(resourceName: "group20"))
        
        let contact = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
        let number : NSString = String(format : "%@ %@", providerListDataArr[indexPath.row].country_code, providerListDataArr[indexPath.row].mobile) as NSString
        
        let contactAttribute : String = String(format : "\(contact): %@",number)
        
        var attributedString = NSMutableAttributedString(string: contactAttribute)
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
            ], range: NSRange(location: 0, length:contact.length)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
            ], range: NSRange(location: attributedString.length - number.length, length: number.length)
        )
        cell?.lblContact?.attributedText = attributedString
        
        let distDict = providerListDataArr[indexPath.row].dist
        let distanceNum = distDict["calculated"] as! Double
        let distanceNumInMeter = (distanceNum/1000)
        let distanceStr : String = String (format : "%.2f", distanceNumInMeter)
        let distanceStr1 = "\(distanceStr)km"
        
        attributedString = NSMutableAttributedString(string: distanceStr1)
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
            ], range: NSRange(location: 0, length: attributedString.length-2)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
            ], range: NSRange(location: attributedString.length-2, length: 2)
        )
        cell?.lblKm?.attributedText = attributedString
        
        if language == "en"
        {
            cell?.lblKm?.textAlignment = .right
        }
        else
        {
            cell?.lblKm?.textAlignment = .left
        }
        
        let ratings = providerListDataArr[indexPath.row].ratings
        cell?.ratingView.rating = ratings
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        let reviews_count = providerListDataArr[indexPath.row].review_count
        cell?.lblReview?.text = "\(reviews_count) \(NSLocalizedString("REVIEWS", comment: ""))"
        
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            cell?.lblReview?.isUserInteractionEnabled = true
        }
        else
        {
            cell?.lblReview?.isUserInteractionEnabled = false
        }
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(reviewLabelClicked))
        cell?.lblReview?.tag = indexPath.row
        cell?.lblReview?.addGestureRecognizer(tap)
        
        cell?.newProviderLabel.text = CommonFunctions.getLocalizedString(localizedName: "New")
        
        let booking_compleated = providerListDataArr[indexPath.row].booking_compleated
        if booking_compleated > 5
        {
            cell?.ratingView.isHidden = false
            cell?.lblReview?.isHidden = false
        
            cell?.newProviderLabel.isHidden = true
        }
        else
        {
            cell?.ratingView.isHidden = true
            cell?.lblReview?.isHidden = true
            
            cell?.newProviderLabel.isHidden = false
        }
        
        return cell!
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let detailVC = ProviderDetailVC()
        
        detailVC.timeDict = serviceDict
        detailVC.providerData = providerListDataArr[indexPath.row]
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func reviewLabelClicked(sender : UITapGestureRecognizer)
    {
        let tag = sender.view?.tag
        
        let reviewlist = ReviewsListVC()
        
        reviewlist.reviewProviderId = providerListDataArr[tag!].provider_id
        
        self.navigationController?.pushViewController(reviewlist, animated: true)
    }
    
    // MARK: - Other Methods
    
    func callProvider(sender : UIButton)
    {
        let tag = sender.tag
        
        let contact : String = String(format : "%@%@", providerListDataArr[tag].country_code, providerListDataArr[tag].mobile)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailProvider(sender : UIButton)
    {
        let tag = sender.tag
        
        var provider_name = ""
        if language == "en"
        {
            provider_name = providerListDataArr[tag].provider_name_en
        }
        else
        {
            provider_name = providerListDataArr[tag].provider_name_ar
        }
        
        if providerListDataArr[tag].fcm_user_id != ""
        {
            let messages = MessagesVC()
            
            messages.selected_user_name = provider_name
            messages.selected_user_id = providerListDataArr[tag].fcm_user_id
            messages.device_token_string = providerListDataArr[tag].device_token
            
            self.navigationController?.pushViewController(messages, animated: true)
        }
    }
}
