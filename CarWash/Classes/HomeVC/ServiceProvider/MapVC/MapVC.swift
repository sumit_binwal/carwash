//
//  MapVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 28/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit
import MBProgressHUD

class MapVC: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate
{
    @IBOutlet var mapView            : MKMapView?
    @IBOutlet var lblCarType         : UILabel?
    @IBOutlet var lblDay             : UILabel?
    
    var serviceDict : NSDictionary?
    var callOutView : AnnotationView!
    
    var isRatingSet = false
    
    let formatter = DateFormatter()
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterInnerView: UIView!
    
    @IBOutlet weak var sort_label: UILabel!
    
    @IBOutlet weak var sort_price_label: UILabel!
    @IBOutlet weak var sort_rating_label: UILabel!
    @IBOutlet weak var sort_duration_label: UILabel!
    
    @IBOutlet weak var sort_price_image: UIImageView!
    @IBOutlet weak var sort_rating_image: UIImageView!
    @IBOutlet weak var sort_duration_image: UIImageView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var sortString = ""
    
    var providerListDataArr = [ProviderListModel]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.filterView.isHidden = true
        
        setUpNavigationBar()
        
        lblCarType?.text = serviceDict?.value(forKey: "type") as? String
        
        let str1 = serviceDict?.value(forKey: "date")as! String
        let str2 = serviceDict?.value(forKey: "time")as! String
        lblDay?.text = str1 + ", " + str2
        
        self.setUpMapView()
        
        setUpFilterView()
    }
    
    func setUpFilterView()
    {
        filterInnerView.layer.borderColor = UIColor.clear.cgColor
        filterInnerView.layer.borderWidth = 1.0
        filterInnerView.layer.cornerRadius = 5.0
        filterInnerView.layer.masksToBounds = true
        
        filterInnerView.isHidden = false
        
        submitButton.setTitle(NSLocalizedString("SUBMIT_TEXT", comment: ""), for: .normal)
        
        sort_price_label.text = NSLocalizedString("Price, lowest to highest", comment: "")
        sort_rating_label.text = NSLocalizedString("Highest rating", comment: "")
        sort_duration_label.text = NSLocalizedString("Duration, lowest to highest", comment: "")
        
        sort_label.text = NSLocalizedString("Sort By", comment: "")
        
        if sortString == "price"
        {
            sort_price_image.isHidden = false
            sort_rating_image.isHidden = true
            sort_duration_image.isHidden = true
        }
        else if sortString == "duration"
        {
            sort_price_image.isHidden = true
            sort_rating_image.isHidden = true
            sort_duration_image.isHidden = false
        }
        else if sortString == "rating"
        {
            sort_price_image.isHidden = true
            sort_rating_image.isHidden = false
            sort_duration_image.isHidden = true
        }
        else
        {
            sort_price_image.isHidden = true
            sort_rating_image.isHidden = true
            sort_duration_image.isHidden = true
        }
        
        CommonFunctions.setUIButtonBorderColor(submitButton)
    }
    
    func setUpMapView()
    {
        mapView?.showsUserLocation = true
        mapView?.delegate = self
        
        if let allAnnotations = self.mapView?.annotations
        {
            self.mapView?.removeAnnotations(allAnnotations)
        }
        
        let views = Bundle.main.loadNibNamed("AnnotationView", owner: nil, options: nil)
        callOutView = views?[0] as! AnnotationView
        
        let center = CLLocationCoordinate2D(latitude: CLLocationDegrees(userLattitude), longitude: CLLocationDegrees(userLongtitude))
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        mapView?.setRegion(region, animated: true)
        
        for (index, providerData) in providerListDataArr.enumerated()
        {
            let dataDict1 = providerData.dist
            
            let dataArray : [Double] = dataDict1["location"] as! [Double]
            
            let latitudeVal = dataArray.last
            let longitudeVal = dataArray.first
            
            let annotation = PinAnnotation()
            let coordinate1 = CLLocationCoordinate2D(latitude: latitudeVal!, longitude: longitudeVal!)
            annotation.coordinate = coordinate1
            annotation.tag1 = index
            
            let url = NSURL(string: providerData.provider_image)!
            
            let booking_compleated = providerData.booking_compleated
            
            annotation.booking_compleated = booking_compleated
            
            annotation.imgUrl = url
            
            if language == "en"
            {
                annotation.userName = providerData.provider_name_en
            }
            else
            {
                annotation.userName = providerData.provider_name_ar
            }
            
            let ratings = providerData.ratings
            annotation.rating = ratings
            
            let distanceNum = dataDict1["calculated"] as! Double
            
            let distanceNumInMeter = (distanceNum/1000)
            let distanceStr : String = String (format : "%.2f", distanceNumInMeter)
            let distanceStr1 = "\(distanceStr)km"
            
            annotation.distance = distanceStr1
            
            mapView?.addAnnotation(annotation)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("SELECTED_SERVICE_PROVIDER", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filter()
    {
        if callOutView.isHidden
        {
            filterView.isHidden = false
        }
        else
        {
            callOutView.isHidden = true
            filterView.isHidden = false
        }
    }
    
    @IBAction func sortSubmitButtonAction(_ sender: Any)
    {
        filterView.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerListDataArr.removeAll()
        serviceProvidersListingApi()
    }
    
    @IBAction func sortPriceButtonAction(_ sender: Any)
    {
        sortString = "price"
        
        sort_price_image.isHidden = false
        sort_rating_image.isHidden = true
        sort_duration_image.isHidden = true
    }
    
    @IBAction func sortRatingButtonAction(_ sender: Any)
    {
        sortString = "rating"
        
        sort_price_image.isHidden = true
        sort_rating_image.isHidden = false
        sort_duration_image.isHidden = true
    }
    
    @IBAction func sortDurationCloseButtonAction(_ sender: Any)
    {
        sortString = "duration"
        
        sort_price_image.isHidden = true
        sort_rating_image.isHidden = true
        sort_duration_image.isHidden = false
    }
    
    @IBAction func filterCloseButtonAction(_ sender: Any)
    {
        sortString = ""
        
        sort_price_image.isHidden = true
        sort_rating_image.isHidden = true
        sort_duration_image.isHidden = true
        
        filterView.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerListDataArr.removeAll()
        serviceProvidersListingApi()
    }
    
    // MARK: API Call
    func serviceProvidersListingApi()
    {
        var str1 = serviceDict?.value(forKey: "date")as! String
        formatter.dateFormat = "dd MMM yyyy"
        var date = formatter.date(from: str1)
        formatter.dateFormat = "YYYY-MM-dd"
        str1 = formatter.string(from: date!)
        
        var str2 = serviceDict?.value(forKey: "time")as! String
        // split string into array
        var fullNameArr = str2.components(separatedBy: "-")
        str2 = fullNameArr[0]
        
        formatter.dateFormat = "hh:mm a"
        date = formatter.date(from: str2)
        formatter.dateFormat = "HH:mm"
        str2 = formatter.string(from: date!)
        
        var type : NSString = ""
        let str = lblCarType?.text!
        
        if(str?.isEqual(NSLocalizedString("SMALL_CARS", comment: "")))!
        {
            type = "small"
        }
        else if(str?.isEqual(NSLocalizedString("MEDIUM_CARS", comment: "")))!
        {
            type = "medium"
        }
        else if(str?.isEqual(NSLocalizedString("BIG_CARS", comment: "")))!
        {
            type = "big"
        }
        
        let dic : [String : Any] =
            [
                "latitude"          : userLattitude,
                "longitude"         : userLongtitude,
                "car_type"          : type,
                "date"              : str1,
                "time"              : str2,
                "sort"              : sortString
        ]
        
        print("dict = \(dic)")
        
        let url = BASE_URL + "home"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .post, parameters: dic, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        
                })
                
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let servicesDtaArr1 = dictionary!["data"] as? [[String : Any]]
                    
                    guard servicesDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self?.updateProviderListModelArray(usingArray: servicesDtaArr1!)
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            
                    })
                }
            }
        }
    }
    
    //MARK:- Updating provider list Model Array
    func updateProviderListModelArray(usingArray array : [[String:Any]]) -> Void
    {
        providerListDataArr.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ProviderListModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            providerListDataArr.append(model) // Adding model to array
        }
        
        self.setUpMapView()
    }
    
    func addPin(withTitle title: String, andCoordinate strCoordinate: String, index: Int)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation.isKind(of: MKUserLocation.self.self)
        {
            return nil
        }
        
        var annotationIdentifier = ""
        var annotationView: MKAnnotationView?
        
        if(annotation.isKind(of: PinAnnotation.self))
        {
            annotationIdentifier = "Pin"
            
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            }
            else
            {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            }
            
            if let annotationView = annotationView
            {
                annotationView.canShowCallout = false
                annotationView.image = UIImage(named: "location")
            }
        }
        
        annotationView?.annotation = annotation
        
        return annotationView
    }
    
    func selectAnnotation(sender : UIButton)
    {
        let detailVC = ProviderDetailVC()
        
        detailVC.providerData = providerListDataArr[sender.tag]
        detailVC.timeDict = serviceDict
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        if view.annotation is MKUserLocation
        {
            print("this is did select 11")
            return
        }
        
        if(view.annotation?.isKind(of: PinAnnotation.self))!
        {
            let customAnnotation =  view.annotation as! PinAnnotation
            
            callOutView.backgroundColor = UIColor.white
            callOutView.btnDetail.addTarget(self, action: #selector(selectAnnotation(sender:)), for: UIControlEvents.touchUpInside)
            
            callOutView.lblName?.text = customAnnotation.userName
            callOutView.lblKm?.text = customAnnotation.distance
        
            callOutView.imgService.setIndicatorStyle(.gray)
            callOutView.imgService.setShowActivityIndicator(true)
            
            callOutView.imgService.sd_setImage(with: customAnnotation.imgUrl as URL!, placeholderImage: #imageLiteral(resourceName: "group20"))
            
            if customAnnotation.booking_compleated > 5
            {
                callOutView.lblNew.isHidden = true
                callOutView.ratingView.isHidden = false
                
                callOutView.ratingView.rating = customAnnotation.rating!
                callOutView.ratingView.backgroundColor = UIColor.clear
                
                if language == "en"
                {
                    
                }
                else
                {
                    if isRatingSet == false
                    {
                        isRatingSet = true
                        callOutView.ratingView.transform = callOutView.ratingView.transform.rotated(by: CGFloat(Double.pi))
                    }
                }
                
                callOutView.ratingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
                
                callOutView.ratingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
                
                /** Note: With the exception of contentMode, type and delegate,
                 all properties can be set directly in Interface Builder **/
                callOutView.ratingView.type = .halfRatings
                callOutView.ratingView.isUserInteractionEnabled = false
            }
            else
            {
                callOutView.lblNew.isHidden = false
                callOutView.ratingView.isHidden = true
                
                callOutView.lblNew.text = CommonFunctions.getLocalizedString(localizedName: "New")
            }
            
            callOutView.frame = CGRect(x: 0, y: 0, width: 200, height: 70)
            callOutView.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 115)
            callOutView.btnDetail.tag = customAnnotation.tag1
            
            if !callOutView .isDescendant(of: self.view)
            {
                self.view.addSubview(callOutView)
            }
            
            callOutView.isHidden = false
            
            mapView.setCenter((view.annotation?.coordinate)!, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView)
    {
        callOutView.isHidden = true
    }
}
