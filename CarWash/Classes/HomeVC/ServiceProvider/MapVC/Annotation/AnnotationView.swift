//
//  AnnotationView.swift
//  CarWash
//
//  Created by iOS on 26/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit

class AnnotationView: UIView
{
    @IBOutlet var imgService    : UIImageView!
    @IBOutlet var lblName       : UILabel!
    @IBOutlet var lblNew       : UILabel!
    @IBOutlet var lblKm         : UILabel!
    @IBOutlet var btnDetail     : UIButton!
    @IBOutlet var ratingView    : FloatRatingView!
}
