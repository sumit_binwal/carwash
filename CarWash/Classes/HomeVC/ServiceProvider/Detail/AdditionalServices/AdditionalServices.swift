//
//  AdditionalServices.swift
//  CarWash
//
//  Created by iOS on 30/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class AdditionalServices: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var tblServices     : UITableView?
    @IBOutlet var lblPrice        : UILabel?
    
    @IBOutlet weak var lblTotalCost: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    var selectedAdditionalServiceAry : [[String : Any]] = []
    
    var timeDict     : NSDictionary?
    
    var totalPrice   : Double = 0
    
    @IBOutlet weak var totalView: UIView!
    
    var providerData = ProviderListModel()
    var serviceDictAdditional : [String : Any]?
    
    var allAdditionalServicesArr : [[String : Any]]?
    
    var currencyCodeCA = ""
    
    var selectedAry : [IndexPath] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let countryCodeCA = providerData.iso_code
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        _ = localeCA.object(forKey: NSLocale.Key.currencySymbol)
        currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        let price : Int = serviceDictAdditional!["price"] as! Int
        
        totalPrice += Double(price)
        
        let tempPrice  = String(format : "%@%.2f", currencyCodeCA, totalPrice)
        lblPrice?.text = String(format : "%@", tempPrice)
        
        lblTotalCost.text = String (format : "\(NSLocalizedString("TOTAL_COST", comment: "")) :")
        
        btnNext.setTitle(NSLocalizedString("NEXT_TEXT", comment: ""), for: UIControlState.normal)
        
        if let additional_services = serviceDictAdditional!["additional_services"] as? [[String : Any]]
        {
            if additional_services.count > 0
            {
                allAdditionalServicesArr = additional_services
            }
        }
        
        tblServices?.register(UINib(nibName: "AdditionalServiceCell", bundle: nil), forCellReuseIdentifier: "AdditionalServiceCell")
        
        if language == "en"
        {
            lblPrice?.textAlignment = .right
        }
        else
        {
            lblPrice?.textAlignment = .left
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }

    func setUpNavBar()
    {
        self.title = NSLocalizedString("SELECT_ADDITIONAL_SERVICES", comment: "")

        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func checkout()
    {
        let obj = ConfirmBookingVC()
        
        obj.serviceDictAdditional = serviceDictAdditional
        obj.providerData = providerData
        obj.timeDict = timeDict
        obj.totalCost = lblPrice?.text
        
        obj.selectedAdditionalArr = selectedAdditionalServiceAry
            
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    // MARK: - UITableView Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (allAdditionalServicesArr?.count)!
     }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (69/375) * SCREEN_WIDTH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalServiceCell") as! AdditionalServiceCell?
        
        cell?.nameLabel?.font = UIFont(name: FONT_REGULAR, size: 16.5)
        cell?.nameLabel?.textColor = UIColor.uicolorFromRGB(86, 86, 86)
        //cell?.lblTitle?.text = "Additional Service 1"
        cell?.plusImage?.image = UIImage(named:"plus_gray")
        
        cell?.priceLabel?.font = UIFont(name: FONT_REGULAR, size: 16.5)
        cell?.priceLabel?.textColor = UIColor.uicolorFromRGB(86, 86, 86)
        
        if(selectedAry.contains(indexPath))
        {
            cell?.plusImage?.image = #imageLiteral(resourceName: "checked")
        }
        
        let dic = allAdditionalServicesArr![indexPath.row]
        
        if (language == "en")
        {
            cell?.nameLabel?.text = dic["title_en"] as? String
        }
        else
        {
            cell?.nameLabel?.text = dic["title_ar"] as? String
        }
        
        let price = Double(dic["price"] as! String)
        
        cell?.priceLabel.text = String (format : "\((NSLocalizedString("PRICE_TEXT", comment: "")).capitalized) : %@%.2f", currencyCodeCA, price!)
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell!
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell : AdditionalServiceCell = tableView.cellForRow(at: indexPath) as! AdditionalServiceCell
        cell.plusImage?.image = UIImage(named:"checked")
        
        let dic = allAdditionalServicesArr![indexPath.row]
        
        if selectedAry.count > 0
        {
            for (i, indexpath) in selectedAry.enumerated()
            {
                if indexpath == indexPath
                {
                    cell.plusImage?.image = #imageLiteral(resourceName: "plus_gray")
                    
                    totalPrice -= Double(dic["price"] as! String)!
                    
                    selectedAry.remove(at: i)
                    
                    selectedAdditionalServiceAry.remove(at: i)
                }
                else
                {
                    cell.plusImage?.image = #imageLiteral(resourceName: "checked")
                    
                    totalPrice += Double(dic["price"] as! String)!
                    
                    selectedAry.append(indexpath)
                    
                    selectedAdditionalServiceAry.append(dic)
                }
            }
        }
        else
        {
            cell.plusImage?.image = #imageLiteral(resourceName: "checked")
            
            totalPrice += Double(dic["price"] as! String)!
            
            selectedAry.append(indexPath)
            
            selectedAdditionalServiceAry.append(dic)
        }
        
        lblPrice?.text = String(format : "%@%.2f", currencyCodeCA, totalPrice)
    }
}
