//
//  ProviderCoverImageVC.swift
//  CarWash
//
//  Created by Ratina on 4/23/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class ProviderCoverImageVC: UIViewController {

    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var coverImagesArr : [String] = []
    var selectedIndex = 0
    
    @IBOutlet var pageControlCoverImage: UIPageControl!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        photosCollectionView.register(UINib.init(nibName: "CoveImageProviderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CoveImageProviderCollectionViewCell")
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        setUpNavigationBar()
        
        setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    //MARK: - Deinit
    deinit
    {
        print("ShowPhotosAndZoomViewController deinit")
    }
    
    func setUpView()
    {
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        
        pageControlCoverImage.numberOfPages = coverImagesArr.count
        
        pageControlCoverImage.currentPage = selectedIndex
        
        photosCollectionView.reloadData()
        
        photosCollectionView.layoutIfNeeded()
        
        photosCollectionView.scrollToItem(at: IndexPath.init(row: selectedIndex, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    //MARK:- setUpNavigationBar
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("Cover Images", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ProviderCoverImageVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return coverImagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoveImageProviderCollectionViewCell", for: indexPath) as! CoveImageProviderCollectionViewCell
        
        print("coverImageArr[indexPath.row] = ", coverImagesArr[indexPath.row])
        
        cell.coverImageView.setIndicatorStyle(.gray)
        cell.coverImageView.setShowActivityIndicator(true)
        
        cell.coverImageView.sd_setImage(with: URL.init(string: coverImagesArr[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "group29"))
        
        cell.coverImageViewScrollView.minimumZoomScale = 1.0
        cell.coverImageViewScrollView.maximumZoomScale = 5.0
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let indexPath = photosCollectionView.indexPathsForVisibleItems.first
        pageControlCoverImage.currentPage = (indexPath?.row)!
        photosCollectionView.reloadData()
    }
}

