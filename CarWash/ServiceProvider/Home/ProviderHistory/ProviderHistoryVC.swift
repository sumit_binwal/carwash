//
//  ProviderHistoryVC.swift
//  CarWash
//
//  Created by Ratina on 4/24/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProviderHistoryVC: UIViewController {

    @IBOutlet var tblHistory    : UITableView?
    @IBOutlet var searchView    : UIView?
    @IBOutlet var searchBar     : UISearchBar?
    
    var filter : UIBarButtonItem?
    
    @IBOutlet var lblNoResult   : UILabel?
    
    var providerServicesArr = [ProviderServicesModel]()
    
    @IBOutlet weak var tableHistoryTopConstraint: NSLayoutConstraint!
    
    let refreshControl = UIRefreshControl()
    
    var pageNumber = 1
    
    var isPageRefresh = false
    
    @IBOutlet var filterView: UIView!
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet var filterFooterView: UIView!
    
    var filterArr : [[String:Any]] = []
    
    var selectedFilterArr : [String] = []
    
    let formatter = DateFormatter()
    
    var date_string = ""
    
    var datePicker = UIDatePicker()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblNoResult?.isHidden = true
        searchView?.isHidden = true
        
        tblHistory?.register(UINib(nibName: "ProviderHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "ProviderHistoryTableViewCell")
        
        filterTableView?.register(UINib(nibName: "filterProviderTableViewCell", bundle: nil), forCellReuseIdentifier: "filterProviderTableViewCell")
        
        filterTableView?.register(UINib(nibName: "SelectDateTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectDateTableViewCell")
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *)
        {
            tblHistory?.refreshControl = refreshControl
        }
        else
        {
            tblHistory?.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
        
        // pending=1, accept=2, reject=3, inprocess=4, complete=5, reached=6, cancel by user=7
        
        submitButton.setTitle(NSLocalizedString("DONE", comment: ""), for: .normal)
        
        var dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Completed"),
            "name" : "5"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Rejected"),
            "name" : "3"
        ]
        
        filterArr.append(dict)
        
        datePicker.datePickerMode = .date
        formatter.dateFormat = "dd MMM yyyy"
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        
        formatter.dateFormat = "dd MMM yyyy"
        
        cellDate.dateTextField.text = formatter.string(from: (datePicker.date))
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        date_string = formatter.string(from: (datePicker.date))
    }
    
    func refreshHomeData(_ sender: Any)
    {
        // Fetch Weather Data
        
        pageNumber = 1
        self.isPageRefresh = true
        self.providerServicesArr.removeAll()
        
        if searchBar?.text == ""
        {
            getHistorySearchData(searchString: "")
        }
        else
        {
            getHistorySearchData(searchString: (searchBar?.text)!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        self.searchBar?.text = ""
        searchView?.isHidden = true
        
        setUpNavigationBar()
        
        searchView?.layer.shadowColor = UIColor.lightGray.cgColor
        searchView?.layer.shadowOffset = CGSize(width: 3, height: 3)
        searchView?.layer.shadowOpacity = 0.2
        searchView?.layer.shadowRadius = 3.0
        searchView?.layer.masksToBounds = false
        
        searchBar?.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        searchBar?.setValue(NSLocalizedString("CANCEL_TEXT", comment: ""), forKey:"_cancelButtonText")
        
        let textFieldInsideUISearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.left
        }
        else
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.right
        }
        
        lblNoResult?.text = NSLocalizedString("NO_HISTORY", comment: "")
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        isPageRefresh = true
        pageNumber = 1
        providerServicesArr.removeAll()
        
        getHistorySearchData(searchString: "")
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("HISTORY_TITLE", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        filter = UIBarButtonItem(image:  #imageLiteral(resourceName: "menu"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(filterButtonPressed))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = filter
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func filterButtonPressed()
    {
        self.searchBar?.endEditing(true)
        searchBar?.setShowsCancelButton(false, animated: true)
        
        showFilterPopUp()
    }
    
    func showFilterPopUp()
    {
        filterView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        
        filter?.isEnabled = false
        
        self.filterTableView?.tableFooterView = self.filterFooterView
        
        self.filterTableView.reloadData()
        
        APPDELEGATE.window?.addSubview(filterView!)
    }
    
    @IBAction func closeFilterView()
    {
        self.selectedFilterArr.removeAll()
        
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        cellDate.dateTextField.text = ""
        
        date_string = ""
        
        filter?.isEnabled = true
        filterView?.removeFromSuperview()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerServicesArr.removeAll()
        pageNumber = 1
        isPageRefresh = true
        
        self.getHistorySearchData(searchString: "")
    }
    
    @IBAction func submitButtonAction()
    {
        filter?.isEnabled = true
        filterView?.removeFromSuperview()
        
        if selectedFilterArr.count == 0 && date_string == ""
        {
            self.tblHistory?.reloadData()
            
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("Please select atleast one option.", comment: ""), dismissBloack: {
                
            })
        }
        else
        {
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            
            providerServicesArr.removeAll()
            pageNumber = 1
            isPageRefresh = true
            
            self.getHistorySearchData(searchString: "")
        }
    }
    func getHistorySearchData(searchString : String)
    {
        var searchStr = searchString as NSString
        searchStr = searchStr.replacingOccurrences(of: " ", with: "%20") as NSString
        
        var sort_string = ""
        if selectedFilterArr.count > 0
        {
            sort_string = CommonFunctions.getCommaSepratedStringFromArray (completeArray: self.selectedFilterArr)
        }
        else
        {
            sort_string = ""
        }
        
        let url = BASE_URL + "booking/providerbookinghistory?search=\(searchStr)&page=\(pageNumber)&request_status=\(sort_string)&date=\(date_string)"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .get, parameters: nil) { [weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            self?.refreshControl.endRefreshing()
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        self?.lblNoResult?.isHidden = false
                        self?.tblHistory?.isHidden = true
                        self?.searchView?.isHidden = true
                        
                        self?.isPageRefresh = true
                })
                
                return
            }
            
            if dictionary == nil
            {
                self?.lblNoResult?.isHidden = false
                self?.tblHistory?.isHidden = true
                self?.searchView?.isHidden = true
                
                self?.isPageRefresh = true
                
                return
            }
            
            if statusCode == 200
            {
                let str = dictionary!["status"] as? String
                
                if str == "success"
                {
                    let servicesDtaArr1 = dictionary!["data"] as? [[String : Any]]
                    
                    guard servicesDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self?.updateServicesModelArray(usingArray: servicesDtaArr1!)
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            self?.lblNoResult?.isHidden = false
                            self?.tblHistory?.isHidden = true
                            self?.searchView?.isHidden = true
                            
                            self?.isPageRefresh = true
                    })
                }
            }
        }
    }
    
    //MARK:- Updating review Model Array
    func updateServicesModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0
        {
            self.isPageRefresh = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ProviderServicesModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            providerServicesArr.append(model) // Adding model to array
        }
        
        self.tblHistory?.isHidden = false
        
        if searchBar?.text == ""
        {
            if (self.providerServicesArr.count > 0)
            {
                self.tblHistory?.tableHeaderView = self.searchView
                self.searchView?.isHidden = false
            }
            else
            {
                self.tblHistory?.tableHeaderView = nil
                self.searchView?.isHidden = true
            }
        }
        else
        {
            self.tblHistory?.tableHeaderView = self.searchView
            self.searchView?.isHidden = false
        }
        
        self.tblHistory?.reloadData()
        
        if self.providerServicesArr.count == 0
        {
            self.lblNoResult?.isHidden = false
        }
        else
        {
            self.lblNoResult?.isHidden = true
        }
        
        if self.providerServicesArr.count == 0
        {
            if self.selectedFilterArr.count > 0 || self.date_string != ""
            {
                self.filter?.isEnabled = true
            }
            else
            {
                self.filter?.isEnabled = false
            }
        }
        else
        {
            self.filter?.isEnabled = true
        }
    }
    
    func doneAction()
    {
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        
        formatter.dateFormat = "dd MMM yyyy"
        
        cellDate.dateTextField.text = formatter.string(from: (datePicker.date))
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        date_string = formatter.string(from: (datePicker.date))
    }
}

extension ProviderHistoryVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblHistory
        {
            return  providerServicesArr.count
        }
        else
        {
            return filterArr.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblHistory
        {
            return UITableViewAutomaticDimension
        }
        else
        {
            if indexPath.row == filterArr.count + 1
            {
                return SCREEN_WIDTH * (77/375)
            }
            else
            {
                return SCREEN_WIDTH * (44/375)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblHistory
        {
            return  SCREEN_WIDTH * 0.340
        }
        else
        {
            return SCREEN_WIDTH * (44/375)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblHistory
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "ProviderHistoryTableViewCell") as! ProviderHistoryTableViewCell?
            
            if cell == nil
            {
                cell = ProviderHistoryTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ProviderHistoryTableViewCell")
            }
            
            if self.providerServicesArr.count > 0
            {
                if language == "en"
                {
                    cell?.lblKm?.textAlignment = NSTextAlignment.right
                    cell?.lblSchedule?.textAlignment = NSTextAlignment.left
                }
                else
                {
                    cell?.lblKm?.textAlignment = NSTextAlignment.left
                    cell?.lblSchedule?.textAlignment = NSTextAlignment.right
                }
                
                cell?.btnCall?.tag = indexPath.row
                cell?.btnMail?.tag = indexPath.row
                
                cell?.imgUser?.layer.cornerRadius = (0.133 * SCREEN_WIDTH)/2
                cell?.imgUser?.layer.masksToBounds = true
                
                cell?.imgUser?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
                cell?.imgUser?.layer.borderWidth = 1.0
                
                cell?.unreadCountView.isHidden = true
                
                cell?.unreadCountView.layer.cornerRadius = (cell?.unreadCountView.frame.size.height)!/2
                cell?.unreadCountView.layer.borderColor = UIColor.clear.cgColor
                cell?.unreadCountView.layer.borderWidth = 1.0
                cell?.unreadCountView.layer.masksToBounds = true
                
                let userDict = providerServicesArr[indexPath.row].user_id
                
                cell?.lblName?.text = userDict["name"] as? String
                
                cell?.imgUser?.setIndicatorStyle(.gray)
                cell?.imgUser?.setShowActivityIndicator(true)
                
                if let avatar = userDict["avatar_url"] as? String
                {
                    if avatar.contains("http")
                    {
                        cell?.imgUser?.sd_setImage(with: URL.init(string: avatar), placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                    else
                    {
                        let avatar1 = String(format:"%@/user_images/%@", BASE_URL, avatar)
                        cell?.imgUser?.sd_setImage(with: URL.init(string: avatar1), placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                }
                
                if let fcm_user_id = userDict["fcm_user_id"] as? String
                {
                    Conversation.showProviderUnReadCount(from_fcm_user_id: fcm_user_id, completion: { (unreadcount) in
                        
                        if unreadcount == "0"
                        {
                            cell?.unreadCountLabel.text = ""
                            cell?.unreadCountView.isHidden = true
                        }
                        else
                        {
                            cell?.unreadCountView.isHidden = false
                            cell?.unreadCountLabel.text = unreadcount
                        }
                    })
                }
                
                let formatter = DateFormatter()
                
                var endTimeStr = providerServicesArr[indexPath.row].endtime
                
                if (endTimeStr.contains(" "))
                {
                    formatter.dateFormat = "HH:mm a"
                }
                else
                {
                    formatter.dateFormat = "HH:mm"
                }
                
                var date = formatter.date(from: endTimeStr)
                formatter.dateFormat = "hh:mma"
                
                if let date1 = date
                {
                    endTimeStr = formatter.string(from: date1)
                }
                
                var startTimeStr = providerServicesArr[indexPath.row].starttime
                
                if (startTimeStr.contains(" "))
                {
                    formatter.dateFormat = "HH:mm a"
                }
                else
                {
                    formatter.dateFormat = "HH:mm"
                }
                
                date = formatter.date(from: startTimeStr)
                formatter.dateFormat = "hh:mma"
                
                if let date1 = date
                {
                    startTimeStr = formatter.string(from: date1)
                }
                
                var str1 = providerServicesArr[indexPath.row].date
                formatter.dateFormat = "yyyy-MM-dd"
                date = formatter.date(from: str1)
                formatter.dateFormat = "dd MMM"
                str1 = formatter.string(from: date!)
                
                cell?.lblScheduleText.text = String (format : "\(NSLocalizedString("SCHEDULE_TEXT", comment: "")) :")
                cell?.lblSchedule?.text = String(format: "%@, %@ - %@", str1, startTimeStr, endTimeStr)
                
                let distanceNum = providerServicesArr[indexPath.row].distance
                let distanceStr : String = String (format : "%.2f", distanceNum)
                let distanceStr1 = "\(distanceStr)km"
                
                let attributedString = NSMutableAttributedString(string: distanceStr1)
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                        NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                    ], range: NSRange(location: 0, length: attributedString.length-2)
                )
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                        NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
                    ], range: NSRange(location: attributedString.length-2, length: 2)
                )
                cell?.lblKm?.attributedText = attributedString
                
                cell?.lblRequestNumber.text = NSLocalizedString("Request Number", comment: "") + " : \(providerServicesArr[indexPath.row].inc_id)"
                
                let status : Int = providerServicesArr[indexPath.row].request_status
                
                if (status == 1)
                {
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "pending")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "pending_ar")
                    }
                }
                else if (status == 2)
                {
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "accepted")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "accepted_ar")
                    }
                }
                else if (status == 3)
                {
                    //rejected
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "rejected")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "rejected_ar")
                    }
                }
                else if (status == 4)
                {
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "progress")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "progress_ar")
                    }
                }
                else if (status == 5)
                {
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "completed")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "completed_ar")
                    }
                }
                else if status == 6
                {
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "reached")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "reached_ar")
                    }
                }
                else if status == 7
                {
                    if language == "en"
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "canceled")
                    }
                    else
                    {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "canceled_ar")
                    }
                }
                
                let numberShow = providerServicesArr[indexPath.row].show_phone
                if numberShow == 1
                {
                    cell?.contactHeightConstraint.constant = 15
                    cell?.contactTopConstraint.constant = 8
                    
                    cell?.btnCall?.isHidden = false
                    cell?.lblContact?.isHidden = false
                    
                    cell?.btnCall?.tag = indexPath.row
                    cell?.btnCall?.addTarget(self, action: #selector(callUser), for: .touchUpInside)
                    
                    let strCode =  userDict["country_code"] as? String
                    let strMobile =  userDict["mobile"] as? String
                    
                    cell?.lblContact?.text =  NSLocalizedString("CONTACT_TEXT", comment: "") + " :  " + strCode! + " " + strMobile!
                }
                else
                {
                    cell?.contactHeightConstraint.constant = 0
                    cell?.contactTopConstraint.constant = 0
                    
                    cell?.btnCall?.isHidden = true
                    cell?.lblContact?.isHidden = true
                    
                    cell?.lblContact?.text = ""
                }
                
                cell?.btnMail?.addTarget(self, action: #selector(mailUser), for: .touchUpInside)
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                if (isPageRefresh && providerServicesArr.count - 1 == indexPath.row)
                {
                    pageNumber += 1
                    
                    if searchBar?.text == ""
                    {
                        getHistorySearchData(searchString: "")
                    }
                    else
                    {
                        getHistorySearchData(searchString: (searchBar?.text)!)
                    }
                }
            }
            
            return cell!;
        }
        else
        {
            if indexPath.row == filterArr.count + 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDateTableViewCell") as! SelectDateTableViewCell?
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell?.selectDateLabel.text = CommonFunctions.getLocalizedString(localizedName: "SELECT_DATE")
                
                cell?.dateTextField.inputView = datePicker
                cell?.dateTextField.delegate = self
                
                if(LanguageManager.currentLanguageIndex() == 0)
                {
                    cell?.dateTextField.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
                    cell?.dateTextField.textAlignment = NSTextAlignment.left
                }
                else
                {
                    cell?.dateTextField.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
                    cell?.dateTextField.textAlignment = NSTextAlignment.right
                }
                
                return cell!
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "filterProviderTableViewCell") as! filterProviderTableViewCell?
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell?.checkButton.isUserInteractionEnabled = false
                
                if indexPath.row == 0
                {
                    if selectedFilterArr.count > 0 || date_string != ""
                    {
                        if selectedFilterArr.count == filterArr.count
                        {
                            cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                        }
                        else
                        {
                            cell?.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                        }
                    }
                    else
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                        
                        for dict in filterArr
                        {
                            selectedFilterArr.append(dict["name"] as! String)
                        }
                    }
                    
                    cell?.sortLabel.text = CommonFunctions.getLocalizedString(localizedName: "Select All")
                }
                else
                {
                    let dict = filterArr[indexPath.row - 1]
                    
                    if selectedFilterArr.contains(dict["name"] as! String)
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    }
                    else
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                    }
                    
                    cell?.sortLabel.text = dict["image"] as? String
                }
                
                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblHistory
        {
            let historyDeyailVC = ProviderHistoryDetailVC()
            historyDeyailVC.providerServiceData = providerServicesArr[indexPath.row]
            self.navigationController?.pushViewController(historyDeyailVC, animated: true)
        }
        else
        {
            if indexPath.row == 0
            {
                if selectedFilterArr.count == 0
                {
                    for (i, dict)  in filterArr.enumerated()
                    {
                        let index = IndexPath(row: i + 1, section: 0)
                        let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                        selectedFilterArr.append(dict["name"] as! String)
                        cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                else
                {
                    for (i, dict)  in filterArr.enumerated()
                    {
                        for (j, str) in selectedFilterArr.enumerated()
                        {
                            if str == dict["name"] as! String
                            {
                                selectedFilterArr.remove(at: j)
                                
                                let index = IndexPath(row: i + 1, section: 0)
                                let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                                cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                                
                            }
                        }
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                }
            }
            else if indexPath.row == filterArr.count + 1
            {
                let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
                
                cellDate.dateTextField.becomeFirstResponder()
            }
            else
            {
                let cell = tableView.cellForRow(at: indexPath) as! filterProviderTableViewCell
                
                let dict = filterArr[indexPath.row - 1]
                
                if selectedFilterArr.contains(dict["name"] as! String)
                {
                    cell.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                    for (i, str) in selectedFilterArr.enumerated()
                    {
                        if str == dict["name"] as! String
                        {
                            selectedFilterArr.remove(at: i)
                        }
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                }
                else
                {
                    cell.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    selectedFilterArr.append(dict["name"] as! String)
                }
                
                if selectedFilterArr.count == filterArr.count
                {
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
            }
        }
    }
    
    // MARK: - Other Methods
    
    func callUser(sender : UIButton)
    {
        let tag = sender.tag
        
        let userDict = providerServicesArr[tag].user_id
        
        let contact = "\(userDict["country_code"] as! String)" + "\(userDict["mobile"] as! String)"
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailUser(sender : UIButton)
    {
        let tag = sender.tag
        
        let userDict = providerServicesArr[tag].user_id
        
        let user_name = userDict["name"] as! String
        
        if let device_token = userDict["device_token"] as? String
        {
            if let fcm_user_id = userDict["fcm_user_id"] as? String
            {
                if fcm_user_id != ""
                {
                    let messages = MessagesVC()
                
                    messages.selected_user_name = user_name
                    messages.selected_user_id = fcm_user_id
                    messages.device_token_string = device_token
                
                    self.navigationController?.pushViewController(messages, animated: true)
                }
            }
        }
    }
}

extension ProviderHistoryVC : UISearchBarDelegate
{
    //MARK: - UISearchController delegates
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerServicesArr.removeAll()
        pageNumber = 1
        searchBar.text = ""
        
        getHistorySearchData(searchString: searchBar.text!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        providerServicesArr.removeAll()
        pageNumber = 1
        
        getHistorySearchData(searchString: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
    }
}

extension ProviderHistoryVC : UITextFieldDelegate
{
    
}
