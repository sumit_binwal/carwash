//
//  ProviderHistoryDetailVC.swift
//  CarWash
//
//  Created by Ratina on 4/24/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProviderHistoryDetailVC: UIViewController
{
    @IBOutlet weak var completTableView: UITableView?
    
    var providerServiceData = ProviderServicesModel()
    
    var currency_code : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        //print("dict = \(dict)")
        
        let countryCodeCA = dict.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        completTableView?.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
        completTableView?.register(UINib(nibName: "ServiceHeader", bundle: nil), forCellReuseIdentifier: "ServiceHeader")
        completTableView?.register(UINib(nibName: "WashServiceCell", bundle: nil), forCellReuseIdentifier: "WashServiceCell")
        completTableView?.register(UINib(nibName: "TotalCostCell", bundle: nil), forCellReuseIdentifier: "TotalCostCell")
        completTableView?.register(UINib(nibName: "PackageCellTableViewCell", bundle: nil), forCellReuseIdentifier: "PackageCellTableViewCell")
        
        setUpNavigationBar()
        
        completTableView?.isHidden = true
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        completTableView?.reloadData()
        completTableView?.isHidden = false
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("HISTORY_DETAIL", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension ProviderHistoryDetailVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            let dict = providerServiceData.service_data
            let assitional_services_arr = dict["additional_services"] as! [[String : Any]]
            
            return assitional_services_arr.count
        }
        else if section == 1
        {
            return 1
        }
        else if section == 3
        {
            return 1
        }
        else if section == 4
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return  SCREEN_WIDTH * (96/375)
        }
        else if section == 3
        {
            return 40
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 2
        {
            return SCREEN_WIDTH * (80/375)
        }
        else if indexPath.section == 1
        {
            return UITableViewAutomaticDimension
        }
        else if indexPath.section == 3
        {
            return 50
        }
        else if indexPath.section == 4
        {
            return SCREEN_WIDTH * (160/375)
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 2
        {
            return SCREEN_WIDTH * (80/375)
        }
        else if indexPath.section == 1
        {
            return 133 * scaleFactorX
        }
        else if indexPath.section == 3
        {
            return 50
        }
        else if indexPath.section == 4
        {
            return SCREEN_WIDTH * (160/375)
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            //recast your view as a UITableViewHeaderFooterView
            let header = UIView(frame : CGRect(x: 20, y: 0, width: SCREEN_WIDTH, height: 80))
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell") as! RequestCell?
            
            if cell == nil
            {
                cell = RequestCell(style: UITableViewCellStyle.default, reuseIdentifier: "RequestCell")
            }
            
            cell?.contentView.backgroundColor = UIColor.white
            cell?.contentView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: (cell?.contentView.frame.size.height)!)
            
            cell?.btnCall?.addTarget(self, action: #selector(callUser), for: .touchUpInside)
            cell?.btnMail?.addTarget(self, action: #selector(mailUser), for: .touchUpInside)
            
            let dict = providerServiceData.user_id
            
            cell?.unReadCountView.isHidden = true
            
            cell?.unReadCountView.layer.cornerRadius = (cell?.unReadCountView.frame.size.height)!/2
            cell?.unReadCountView.layer.borderColor = UIColor.clear.cgColor
            cell?.unReadCountView.layer.borderWidth = 1.0
            cell?.unReadCountView.layer.masksToBounds = true
            
            if let fcm_user_id = dict["fcm_user_id"] as? String
            {
                Conversation.showProviderUnReadCount(from_fcm_user_id: fcm_user_id, completion: { (unreadcount) in
                    
                    if unreadcount == "0"
                    {
                        cell?.unReadCountLabel.text = ""
                        cell?.unReadCountView.isHidden = true
                    }
                    else
                    {
                        cell?.unReadCountView.isHidden = false
                        cell?.unReadCountLabel.text = unreadcount
                    }
                })
            }
            
            cell?.imgService?.layer.cornerRadius = (10.0/375) * SCREEN_WIDTH
            cell?.imgService?.layer.masksToBounds = true
            
            cell?.imgService?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            cell?.imgService?.layer.borderWidth = 1.0
            
            let imageStr = dict["avatar_url"] as! String
            var imageUrlStr = ""
            if imageStr.contains("http")
            {
                imageUrlStr = imageStr
            }
            else
            {
                imageUrlStr = String(format:"%@/user_images/%@", BASE_URL, imageStr)
            }
            
            let imageUrl = URL(string: imageUrlStr)
            
            cell?.imgService?.setShowActivityIndicator(true)
            cell?.imgService?.setIndicatorStyle(.gray)
            
            cell?.imgService?.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "group29"))
            
            let name = dict["name"] as! String
            cell?.lblName?.text = name
            
            let numberShow = providerServiceData.show_phone
            if numberShow == 1
            {
                cell?.btnCall?.isHidden = false
                cell?.lblPhone?.isHidden = false
                
                let contactTxt = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
                let number : NSString = String(format : "%@ %@", dict["country_code"] as! CVarArg, dict["mobile"] as! CVarArg) as NSString
                
                let contact : String = String(format : "\(contactTxt): \(number)")
                
                let attributedString = NSMutableAttributedString(string: contact)
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                        NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
                    ], range: NSRange(location: 0, length: contactTxt.length+1)
                )
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                        NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                    ], range: NSRange(location: attributedString.length-number.length, length: number.length)
                )
                cell?.lblPhone?.attributedText = attributedString
                
                cell?.distanceConstraint.constant = 18
            }
            else
            {
                cell?.btnCall?.isHidden = true
                cell?.lblPhone?.isHidden = true
                
                cell?.distanceConstraint.constant = 0
            }
            
            let distance = providerServiceData.distance
            let distanceStr : String = String (format : "%.2f", distance)
            let distanceStr1 = " \(distanceStr)km "
            
            cell?.lblKm?.text = distanceStr1
            
            let status : Int = providerServiceData.request_status
            
            if (status == 1)
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "pending")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "pending_ar")
                }
            }
            else if (status == 2)
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "accepted")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "accepted_ar")
                }
            }
            else if (status == 3)
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "rejected")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "rejected_ar")
                }
            }
            else if (status == 4)
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "progress")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "progress_ar")
                }
            }
            else if (status == 5)
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "completed")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "completed_ar")
                }
            }
            else if status == 6
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "reached")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "reached_ar")
                }
            }
            else if status == 7
            {
                if language == "en"
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "canceled")
                }
                else
                {
                    cell?.statusImage?.image = #imageLiteral(resourceName: "canceled_ar")
                }
            }
            
            header.addSubview((cell?.contentView)!)
            
            return header
        }
        else if section == 3
        {
            let header = UIView(frame : CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
            header.backgroundColor = UIColor.white
            
            var lblHeader : UILabel
            if (LanguageManager.currentLanguageIndex() == 0)
            {
                lblHeader = UILabel(frame : CGRect(x: 15, y: 20, width: SCREEN_WIDTH, height: 20))
            }
            else
            {
                lblHeader = UILabel(frame : CGRect(x: -15, y: 20, width: SCREEN_WIDTH, height: 20))
            }
            
            lblHeader.font = UIFont(name: FONT_NAVIGATION_BAR, size: 16.5)
            lblHeader.textColor = UIColor.uicolorFromRGB(78, 78, 78)
            
            lblHeader.text = NSLocalizedString("Delivery Date & Time", comment: "")
            
            header.addSubview(lblHeader)
            
            return header
        }
        else
        {
            return nil
        }
    }
    
    func getCommaSepratedStringFromArray(completeArray arr : [String]) -> String
    {
        var nameArr : [String] = []
        
        for i in 0 ..< arr.count
        {
            let nameStr = arr[i]
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PackageCellTableViewCell", for: indexPath) as! PackageCellTableViewCell
            
            cell.time_label1.text = NSLocalizedString("TAKEN_TIME", comment: "")
            cell.cost_label1.text = NSLocalizedString("CHARGE", comment: "")
            
            let dict = providerServiceData.service_data
            
            let price = dict["price"] as! NSNumber
            let duration = dict["duration"] as! NSNumber
            
            cell.time_label.text = "\(duration) min"
            cell.cost_label.text = "\(currency_code)\(price)"
            
            let service_name = dict["service_name"] as! [String : Any]
            
            if language == "en"
            {
                cell.package_name_label.text = service_name["en"] as? String
            }
            else
            {
                cell.package_name_label.text = service_name["ar"] as? String
            }
            
            let subservices_arr = dict["requirements"] as! [String]
            if subservices_arr.count > 0
            {
                cell.required_label.text = self.getCommaSepratedStringFromArray(completeArray: subservices_arr)
            }
            else
            {
                cell.required_label.text = ""
            }
            
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WashServiceCell", for: indexPath) as! WashServiceCell
            
            cell.lblTakenTime.text = NSLocalizedString("CHARGE", comment: "")
            cell.lblCharge.text = ""
            
            let dict = providerServiceData.service_data
            let subservices_arr = dict["additional_services"] as! [[String : Any]]
            
            let dict_sub_service = subservices_arr[indexPath.row]
            
            var name = ""
            if language == "en"
            {
                name = dict_sub_service["title_en"] as! String
            }
            else
            {
                name = dict_sub_service["title_ar"] as! String
            }
            
            let price = dict_sub_service["price"] as! String
            
            cell.nameLabel.text = name
            cell.priceLabel.text = ""
            cell.timeLabel.text = "\(currency_code)\(price)"
            
            if indexPath.row == 0
            {
                let maskPAth1 = UIBezierPath(roundedRect: (cell.upperView.bounds), byRoundingCorners: [.topLeft , .topRight], cornerRadii:CGSize.init(width: 10.0, height: 0.0))
                let maskLayer1 = CAShapeLayer()
                maskLayer1.frame = (cell.upperView.bounds)
                maskLayer1.path = maskPAth1.cgPath
                cell.upperView.layer.mask = maskLayer1
            }
            else if indexPath.row == subservices_arr.count - 1
            {
                let maskPAth1 = UIBezierPath(roundedRect: (cell.lowerView.bounds), byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii:CGSize.init(width: 10.0, height: 0.0))
                let maskLayer1 = CAShapeLayer()
                maskLayer1.frame = (cell.lowerView.bounds)
                maskLayer1.path = maskPAth1.cgPath
                cell.lowerView.layer.mask = maskLayer1
            }
            
            if indexPath.row == 0 && indexPath.row == subservices_arr.count - 1
            {
                let maskPAth1 = UIBezierPath(roundedRect: (cell.lowerView.bounds), byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii:CGSize.init(width: 10.0, height: 0.0))
                let maskLayer1 = CAShapeLayer()
                maskLayer1.frame = (cell.lowerView.bounds)
                maskLayer1.path = maskPAth1.cgPath
                cell.lowerView.layer.mask = maskLayer1
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
        }
        else if indexPath.section == 3
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
            }
            
            let formatter = DateFormatter()
            
            var endTimeStr = providerServiceData.endtime
            
            if (endTimeStr.contains(" "))
            {
                formatter.dateFormat = "HH:mm a"
            }
            else
            {
                formatter.dateFormat = "HH:mm"
            }
            
            //formatter.dateFormat = "HH:mm"
            var date = formatter.date(from: endTimeStr)
            formatter.dateFormat = "hh:mm a"
            
            if let date1 = date
            {
                endTimeStr = formatter.string(from: date1)
            }
            
            var startTimeStr = providerServiceData.starttime
            
            if (startTimeStr.contains(" "))
            {
                formatter.dateFormat = "HH:mm a"
            }
            else
            {
                formatter.dateFormat = "HH:mm"
            }
            
            //formatter.dateFormat = "HH:mm"
            date = formatter.date(from: startTimeStr)
            formatter.dateFormat = "hh:mm a"
            
            if let date1 = date
            {
                startTimeStr = formatter.string(from: date1)
            }
            
            var str1 = providerServiceData.date
            formatter.dateFormat = "yyyy-MM-dd"
            date = formatter.date(from: str1)
            formatter.dateFormat = "dd MMM yyyy"
            str1 = formatter.string(from: date!)
            
            cell?.textLabel?.text = "\(str1), \(startTimeStr) - \(endTimeStr)"
            
            cell?.textLabel?.textColor = UIColor.uicolorFromRGB(142, 142, 142)
            cell?.textLabel?.font = UIFont.init(name: FONT_NAVIGATION_BAR, size: 16.5)
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else if indexPath.section == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCostCell") as! TotalCostCell?
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell?.lblTotalCost.text = NSLocalizedString("TOTAL_COST", comment: "")
            
            cell?.addressLabel.text = providerServiceData.user_address
            
            if language == "en"
            {
                cell?.addressLabel.textAlignment = .left
            }
            else
            {
                cell?.addressLabel.textAlignment = .right
            }
            
            let priceStr = providerServiceData.price
            let str1 = String(format : "%.2f", priceStr)
            cell?.totalCostLael.text = "\(currency_code)\(str1)"
            
            cell?.addressTrailingConstraint.constant = 10
            
            cell?.navigationBtn.isHidden = true
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as UITableViewCell?
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
    }
    // MARK: - Other Methods
    
    func callUser(sender : UIButton)
    {
        let dict = providerServiceData.user_id
        
        let contact : String = String(format : "%@%@", dict["country_code"] as! CVarArg, dict["mobile"] as! CVarArg)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailUser(sender : UIButton)
    {
        let provider_name_dict = providerServiceData.user_id
        
        let provider_name = provider_name_dict["name"] as! String
        
        if let device_token = provider_name_dict["device_token"] as? String
        {
            if let fcm_user_id = provider_name_dict["fcm_user_id"] as? String
            {
                if fcm_user_id != ""
                {
                    let messages = MessagesVC()
                
                    messages.selected_user_name = provider_name
                    messages.selected_user_id = fcm_user_id
                    messages.device_token_string = device_token
                
                    self.navigationController?.pushViewController(messages, animated: true)
                }
            }
        }
    }
}
