//
//  AddServiceViewController.swift
//  CarWash
//
//  Created by Ratina on 6/11/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddServiceViewController: UIViewController
{
    @IBOutlet weak var addTableView: UITableView!
 
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var package_name_en_text: UITextField!
    @IBOutlet weak var package_name_ar_text: UITextField!
    
    @IBOutlet weak var short_description_text_en: KMPlaceholderTextView!
    @IBOutlet weak var short_description_text_ar: KMPlaceholderTextView!
    
    @IBOutlet weak var long_description_text_en: KMPlaceholderTextView!
    @IBOutlet weak var long_description_text_ar: KMPlaceholderTextView!
    
    @IBOutlet weak var car_type_button: UIButton!
    
    @IBOutlet weak var price_en_text: UITextField!
    @IBOutlet weak var price_ar_text: UITextField!
    
    @IBOutlet weak var duration_en_text: UITextField!
    @IBOutlet weak var duration_ar_text: UITextField!
    
    @IBOutlet var durationPicker: UIPickerView!
    
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var waterButton: UIButton!
    @IBOutlet weak var electricityButton: UIButton!
    @IBOutlet weak var placeButton: UIButton!
    @IBOutlet weak var cashButton: UIButton!
    @IBOutlet weak var cardButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var additionalButton: UIButton!
    
    @IBOutlet var txtCurrent : UITextField!
    
    var water_selected = 0
    var electricity_selected = 0
    var place_selected = 0
    var cash_selected = 0
    var card_selected = 0
    var other_selected = 0
    
    var minAry  : NSMutableArray = []
    var hourAry  : NSMutableArray = []
    
    var currency_code : String = ""
    
    var additional_service_data : [[String : Any]] = []
    
    var isEdit = false
    var editServiceData = ServicesModel()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        addTableView.register(UINib(nibName: "AddAdditionalTableViewCell", bundle: nil),       forCellReuseIdentifier: "AddAdditionalTableViewCell")
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        //print("dict = \(dict)")
        
        let countryCodeCA = dict.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        setUpTableView()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpTableView()
    {
        addTableView.delegate = self
        addTableView.dataSource = self
        
        durationPicker.delegate = self
        durationPicker.dataSource = self
        
        headerView.frame = CGRect(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: SCREEN_WIDTH, height: headerView.frame.size.height)
        addTableView.tableHeaderView = headerView
        
        footerView.frame = CGRect(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: SCREEN_WIDTH, height: footerView.frame.size.height)
        addTableView.tableFooterView = footerView
        
        CommonFunctions.setTextViewBorderColor(short_description_text_ar)
        CommonFunctions.setTextViewBorderColor(short_description_text_en)
        CommonFunctions.setTextViewBorderColor(long_description_text_ar)
        CommonFunctions.setTextViewBorderColor(long_description_text_en)
        
        CommonFunctions.setTextFieldBorderColor(package_name_ar_text)
        CommonFunctions.setTextFieldBorderColor(package_name_en_text)
        
        CommonFunctions.setUIButtonBorderColor(car_type_button)
        CommonFunctions.setUIButtonBorderColor(additionalButton)
        
        package_name_en_text.setLeftPaddingPoints(5.0)
        package_name_en_text.setRightPaddingPoints(5.0)
        
        package_name_ar_text.setLeftPaddingPoints(5.0)
        package_name_ar_text.setRightPaddingPoints(5.0)
        
        var min : String?
        for i in sequence(first: 00, next: { $0 + 5 <= 55 ? $0 + 5 : nil })
        {
            if i == 0 || i == 5
            {
                if i == 0
                {
                    min = "00"
                }
                else
                {
                    min = "05"
                }
            }
            else
            {
                min = String(format: "%d", i)
            }
            
            minAry.add(min ?? "")
        }
        
        for i in 0 ... 11
        {
            min = String(format: "%d", i)
            hourAry.add(min ?? "")
        }
        
        duration_ar_text.inputView = durationPicker
        duration_en_text.inputView = durationPicker
        
        saveButton.setTitle(NSLocalizedString("SAVE_TEXT", comment: ""), for: UIControlState.normal)
        
        if isEdit
        {
            additional_service_data = editServiceData.additional_services
            
            package_name_ar_text.text = editServiceData.service_name_ar
            package_name_en_text.text = editServiceData.service_name_en
            
            long_description_text_ar.text = editServiceData.description_ar
            long_description_text_en.text = editServiceData.description_en
            
            short_description_text_ar.text = editServiceData.sort_description_ar
            short_description_text_en.text = editServiceData.sort_description_en
            
            duration_ar_text.text = editServiceData.duration
            duration_en_text.text = editServiceData.duration
            
            price_ar_text.text = "\(currency_code)\(editServiceData.price)"
            price_en_text.text = "\(currency_code)\(editServiceData.price)"
            
            car_type_button.setTitle(editServiceData.car_type, for: .normal)
            
            for str in editServiceData.requirements
            {
                if str.contains("Water")
                {
                    water_selected = 1
                    waterButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                
                if str.contains("Electricity")
                {
                    electricity_selected = 1
                    electricityButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                
                if str.contains("Suitable Place")
                {
                    place_selected = 1
                    placeButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                
                if str.contains("Cash")
                {
                    cash_selected = 1
                    cashButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                
                if str.contains("Pay By Card")
                {
                    card_selected = 1
                    cardButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                
                if str.contains("Other")
                {
                    other_selected = 1
                    otherButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
            }
            
            self.addTableView.reloadData()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
    }
    
    func setUpNavigationBar()
    {
        if isEdit
        {
            self.title = NSLocalizedString("SERVICE_DETAIL", comment: "")
        }
        else
        {
            self.title = NSLocalizedString("ADD_SERVICE", comment: "")
        }
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectCarTypeButtonAction(_ sender: Any)
    {
        RESIGN_KEYBOARD
        
        let typeObj = TypeVC()
        
        typeObj.okPressed =
            { (str : NSString) -> Void in
                
                self.car_type_button.setTitle(str as String, for: .normal)
        }
        
        self.navigationController?.pushViewController(typeObj, animated: true)
    }
    
    @IBAction func waterButtonAction(_ sender: Any)
    {
        if water_selected == 0
        {
            water_selected = 1
            waterButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
        else
        {
            water_selected = 0
            waterButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func electricityButtonAction(_ sender: Any)
    {
        if electricity_selected == 0
        {
            electricity_selected = 1
            electricityButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
        else
        {
            electricity_selected = 0
            electricityButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func placeButtonAction(_ sender: Any)
    {
        if place_selected == 0
        {
            place_selected = 1
            placeButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
        else
        {
            place_selected = 0
            placeButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func cashButtonAction(_ sender: Any)
    {
        if cash_selected == 0
        {
            cash_selected = 1
            cashButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
        else
        {
            cash_selected = 0
            cashButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func cardButtonAction(_ sender: Any)
    {
        if card_selected == 0
        {
            card_selected = 1
            cardButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
        else
        {
            card_selected = 0
            cardButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func otherButtonAction(_ sender: Any)
    {
        if other_selected == 0
        {
            other_selected = 1
            otherButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
        }
        else
        {
            other_selected = 0
            otherButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func additionalServiceButtonAction(_ sender: Any)
    {
        callAdditionalServiceView(isEdit: false, index: 0)
    }
    
    func callAdditionalServiceView(isEdit : Bool, index : Int)
    {
        let add = AddAdditionalViewController()
        
        add.isEdit = isEdit
        if isEdit
        {
            add.additional_edit_data = additional_service_data[index]
        }
        
        add.savePressed =
            { (dict : [String : Any]) -> Void in
                
                if isEdit
                {
                    self.additional_service_data[index] = dict
                }
                else
                {
                    self.additional_service_data.append(dict)
                }
                
                self.addTableView.reloadData()
        }
        
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any)
    {
        if validateUserInput()
        {
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            addNewPackageAPI()
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if (package_name_en_text.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_EN", comment: ""), dismissBloack:
                {
                    self.package_name_en_text.becomeFirstResponder()
            })
        }
        else if (package_name_ar_text.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_AR", comment: ""), dismissBloack:
                {
                    self.package_name_ar_text.becomeFirstResponder()
            })
        }
        else if (short_description_text_en.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_EN", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if (short_description_text_ar.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_AR", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if (long_description_text_en.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_long_EN", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if (long_description_text_ar.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_LONG_AR", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if car_type_button.title(for: .normal) == "Car Type"
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_CARS", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if (duration_en_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DURATION", comment: ""), dismissBloack:
                {
                    self.duration_en_text?.becomeFirstResponder()
            })
        }
        else if(price_en_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PRICE", comment: ""), dismissBloack:
                {
                    self.price_en_text?.becomeFirstResponder()
            })
        }
        else if (duration_ar_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DURATION", comment: ""), dismissBloack:
                {
                    self.duration_ar_text?.becomeFirstResponder()
            })
        }
        else if(price_ar_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PRICE", comment: ""), dismissBloack:
                {
                    self.price_ar_text?.becomeFirstResponder()
            })
        }
        else if (duration_en_text?.text != duration_ar_text?.text)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("DIFFERENT_DURATION", comment: "") , dismissBloack:
                {
                    
            })
        }
        else if (price_en_text?.text != price_ar_text?.text)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("DIFFERENT_PRICE", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if water_selected == 0 && electricity_selected == 0 && place_selected == 0 && cash_selected == 0 && card_selected == 0 && other_selected == 0
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("Please select atleast one requirement", comment: ""), dismissBloack:
                {
                    
            })
        }
        
        return isError
    }
    
    func getCommaSepratedStringFromArray(completeArray arr : [String]) -> String
    {
        var nameArr : [String] = []
        
        for i in 0 ..< arr.count
        {
            let nameStr = arr[i]
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    func addNewPackageAPI()
    {
        var price = price_ar_text.text
        price = price?.replacingOccurrences(of: currency_code, with: "")
        
        var strType : String = car_type_button.title(for: .normal)!
        
        if(strType.isEqual(NSLocalizedString("SMALL_CARS", comment: "")))
        {
            strType = "small"
        }
        else if(strType.isEqual(NSLocalizedString("MEDIUM_CARS", comment: "")))
        {
            strType = "medium"
        }
        else if(strType.isEqual(NSLocalizedString("BIG_CARS", comment: "")))
        {
            strType = "big"
        }
        
        var requirements : [String] = []
        
        if water_selected == 1
        {
            requirements.append("Water")
        }
        
        if electricity_selected == 1
        {
            requirements.append("Electricity")
        }
        
        if place_selected == 1
        {
            requirements.append("Suitable Place")
        }
        
        if cash_selected == 1
        {
            requirements.append("Cash")
        }
        
        if card_selected == 1
        {
            requirements.append("Pay By Card")
        }
        
        if other_selected == 1
        {
            requirements.append("Other")
        }
        
        let duration_arr = duration_ar_text.text?.components(separatedBy: ":")
        let duration_hr_str = duration_arr![0]
        let duration_min_str = duration_arr![1]
        
        var duration_hr = Int(duration_hr_str)
        duration_hr = duration_hr! * 60
        
        let duration_min = Int(duration_min_str)
        
        duration_hr = duration_hr! + duration_min!
        
        var params : [String : Any]?
        var url = ""
        
        if isEdit
        {
            params = [
                
                "id" : editServiceData.id,
                
                "service_name_en" : package_name_en_text.text ?? "",
                "service_name_ar" : package_name_ar_text.text ?? "",
                
                "description_en" : long_description_text_en.text,
                "description_ar" : long_description_text_ar.text,
                
                "sort_description_en" : short_description_text_en.text,
                "sort_description_ar" : short_description_text_ar.text,
                
                "duration" : duration_hr ?? 0,
                "price" : price ?? "",
                
                "car_type" : strType,
                
                "requirements" : requirements,
                
                "additional_services" : additional_service_data
            ]
            
            url = BASE_URL + "service/editservice"
        }
        else
        {
            params = [
                
                "service_name_en" : package_name_en_text.text ?? "",
                "service_name_ar" : package_name_ar_text.text ?? "",
                
                "description_en" : long_description_text_en.text,
                "description_ar" : long_description_text_ar.text,
                
                "sort_description_en" : short_description_text_en.text,
                "sort_description_ar" : short_description_text_ar.text,
                
                "duration" : duration_hr ?? 0,
                "price" : price ?? "",
                
                "car_type" : strType,
                
                "requirements" : requirements,
                
                "additional_services" : additional_service_data
            ]
            
            url = BASE_URL + "service/addservice"
        }
        
        print("add or edit package params = ", params!)
    
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        
                })
                
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                //let replyData = responseDictionary["data"] as! [String : Any]
                
                if replyStatus == "success"
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            self?.navigationController?.popViewController(animated: true)
                    })
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            
                    })
                }
            }
        }
    }
    
    //MARK: KeyboardManager done action
    func doneAction()
    {
        if (txtCurrent == duration_en_text || txtCurrent == duration_ar_text)
        {
            if (txtCurrent?.text?.isEmpty)!
            {
                var strMin  : String = ""
                var strHour : String = ""
                
                strHour = (hourAry[0] as? String)!
                strMin  = (minAry[0] as? String)!
                
                let str : String = String(format: "%@:%@", strHour, strMin)
                txtCurrent?.text = str
                
                duration_en_text.text = txtCurrent?.text
                duration_ar_text.text = txtCurrent?.text
            }
            else
            {
                duration_ar_text.text = txtCurrent?.text
                duration_en_text.text = txtCurrent?.text
            }
        }
    }
}

extension AddServiceViewController : UITableViewDelegate, UITableViewDataSource
{
    func   tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)                                                                                                                            -> Int
    {
        return additional_service_data.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if additional_service_data.count > 0
        {
            let header = UIView(frame : CGRect(x: 20, y: 0, width: SCREEN_WIDTH, height: 40))
            header.backgroundColor = UIColor.uicolorFromRGB(247, 247, 247)
            
            let lblHeader = UILabel(frame : CGRect(x: 20, y: 0, width: SCREEN_WIDTH, height: 40))
            lblHeader.font = UIFont(name: FONT_SEMIBOLD, size: 16)
            lblHeader.textColor = UIColor.uicolorFromRGB(2, 166, 242)
            
            lblHeader.text = NSLocalizedString("ADDITIONAL_SERVICES_LIST", comment: "")
            
            header.addSubview(lblHeader)
            
            return header
        }
        else
        {
            return nil
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if additional_service_data.count > 0
        {
            return 50
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 329 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddAdditionalTableViewCell") as! AddAdditionalTableViewCell

        let dict = additional_service_data[indexPath.row]
        
        cell.delegate = self
        
        cell.deleteButton.tag = indexPath.row
        cell.editButton.tag = indexPath.row
        
        cell.service_name_en_label.text = dict["title_en"] as? String
        cell.service_name_ar_label.text = dict["title_ar"] as? String
        
        if dict["desc_en"] as? String == ""
        {
            cell.description_en_label.text = "-"
        }
        else
        {
            cell.description_en_label.text = dict["desc_en"] as? String
        }
        
        if dict["desc_ar"] as? String == ""
        {
            cell.description_ar_label.text = "-"
        }
        else
        {
            cell.description_ar_label.text = dict["desc_ar"] as? String
        }
        
        cell.price_ar_label.text = "\(currency_code)\(dict["price"] as? String ?? "")"
        cell.price_en_label.text = "\(currency_code)\(dict["price"] as? String ?? "")"

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
}

extension AddServiceViewController : AddAdditionalTableViewCellDelegate
{
    func deleteButtonDelegateMethod(_ cell: AddAdditionalTableViewCell)
    {
        let tag = cell.deleteButton.tag
        
        AlertViewController.showAlertWith(title: "", message: "Do you really want to delete this Additional Service?", buttonsArray: ["No", "Yes"]) { (buttonIndex) in
            
            if buttonIndex == 1
            {
                self.additional_service_data.remove(at: tag)
                self.addTableView.reloadData()
            }
        }
    }
    
    func editButtonDelegateMethod(_ cell: AddAdditionalTableViewCell)
    {
        callAdditionalServiceView(isEdit: true, index: cell.editButton.tag)
    }
}

extension AddServiceViewController : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        txtCurrent = textField
        
        if textField == price_ar_text
        {
            if #available(iOS 10.0, *)
            {
                textField.keyboardType = UIKeyboardType.asciiCapableNumberPad
            }
            else
            {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        else if textField == price_en_text
        {
            if #available(iOS 10.0, *)
            {
                textField.keyboardType = UIKeyboardType.asciiCapableNumberPad
            }
            else
            {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        
        if (textField == duration_en_text)
        {
            if (!(textField.text?.isEmpty)!)
            {
                let ary = duration_en_text.text?.components(separatedBy: ":")
                var str1 : String = ary![0]
                
                durationPicker?.selectRow(hourAry.index(of: str1), inComponent: 0, animated: false)
                
                str1 = ary![1]
                durationPicker?.selectRow(minAry.index(of: str1), inComponent: 1, animated: false)
            }
            else
            {
                durationPicker?.selectRow(0, inComponent: 0, animated: false)
                durationPicker?.selectRow(0, inComponent: 1, animated: false)
            }
        }
        else if(textField == duration_ar_text)
        {
            if (!(textField.text?.isEmpty)!)
            {
                let ary = duration_ar_text.text?.components(separatedBy: ":")
                var str1 : String = ary![0]
                
                durationPicker?.selectRow(hourAry.index(of: str1), inComponent: 0, animated: false)
                
                str1 = ary![1]
                durationPicker?.selectRow(minAry.index(of: str1), inComponent: 1, animated: false)
            }
            else
            {
                durationPicker?.selectRow(0, inComponent: 0, animated: false)
                durationPicker?.selectRow(0, inComponent: 1, animated: false)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField == price_en_text || textField == price_ar_text)
        {
            if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)
            {
                return false
            }
            
            if (textField.text?.hasPrefix(currency_code))!
            {
                var text = textField.text! as NSString
                text = text.replacingOccurrences(of: currency_code, with: "") as NSString
                
                if (string.count > 0) && (text.length + string.count) > 3
                {
                    return false
                }
            }
            else
            {
                if (string.count > 0) && ((textField.text?.count)! + string.count) > 3
                {
                    return false
                }
            }
            
            if !((textField.text?.hasPrefix(currency_code))!)
            {
                if string.count > 0
                {
                    let str1 = textField.text
                    if str1?.count == currency_code.count
                    {
                        let mutableString: String = "\(currency_code)\((textField.text)!)"
                        textField.text = mutableString
                    }
                    else
                    {
                        let mutableString: String = "\(currency_code)"
                        textField.text = mutableString
                    }
                }
            }
            else
            {
                if string.count == 0
                {
                    if textField.text?.count == currency_code.count {
                        textField.text = ""
                    }
                }
            }
        }
        
        if (textField == package_name_en_text || textField == package_name_ar_text)
        {
            if (string.count > 0) && ((textField.text?.count)! + string.count) > 40
            {
                return false
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField == price_en_text)
        {
            if textField.text == currency_code
            {
                price_en_text.text = ""
                price_ar_text.text = ""
            }
            else
            {
                price_en_text.text = textField.text
                price_ar_text.text = price_en_text.text
            }
        }
        else if (textField == price_ar_text)
        {
            if textField.text == currency_code
            {
                price_en_text.text = ""
                price_ar_text.text = ""
            }
            else
            {
                price_ar_text.text = textField.text
                price_en_text.text = price_ar_text.text
            }
        }
    }
}

extension AddServiceViewController : UITextViewDelegate
{
    // MARK: uitextview delegates
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (textView == short_description_text_ar || textView == short_description_text_en || textView == long_description_text_ar || textView == long_description_text_en)
        {
            if (textView.text.count > 150)
            {
                if text == ""
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        
        return true
    }
}

extension AddServiceViewController : UIPickerViewDelegate, UIPickerViewDataSource
{
    // MARK: -  PickerView Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return component == 0 ? hourAry.count : minAry.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return  component == 0  ? hourAry[row] as? String :  minAry[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        var strMin  : String = ""
        var strHour : String = ""
        
        strHour = (hourAry[pickerView.selectedRow(inComponent: 0)] as? String)!
        strMin  = (minAry[pickerView.selectedRow(inComponent: 1)] as? String)!
        
        let str : String = String(format: "%@:%@", strHour, strMin)
        txtCurrent?.text = str
    }
}
