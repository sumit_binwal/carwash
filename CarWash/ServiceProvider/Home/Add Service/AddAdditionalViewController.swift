//
//  AddAdditionalViewController.swift
//  CarWash
//
//  Created by Ratina on 6/11/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class AddAdditionalViewController: UIViewController
{
    @IBOutlet weak var service_name_en_text: UITextField!
    @IBOutlet weak var service_name_ar_text: UITextField!
    
    @IBOutlet weak var description_en_text: KMPlaceholderTextView!
    @IBOutlet weak var description_ar_text: KMPlaceholderTextView!
    
    @IBOutlet weak var price_en_text: UITextField!
    @IBOutlet weak var price_ar_text: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var additionalTableView: UITableView!
    
    var currency_code : String = ""
    
    var additional_edit_data : [String : Any] = [:]
    
    var savePressed : ((_ dict : [String : Any]) -> Void)? = nil
    var isEdit = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpNavigationBar()
        setUpTableView()
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        //print("dict = \(dict)")
        
        let countryCodeCA = dict.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        if isEdit
        {
            self.title = NSLocalizedString("EDIT_ADDITIONAL_SERVICES", comment: "")
        }
        else
        {
            self.title = NSLocalizedString("ADD_ADDITIONAL_SERVICES", comment: "")
        }
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpTableView()
    {
        additionalTableView.delegate = self
        additionalTableView.dataSource = self
        
        headerView.frame = CGRect(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        additionalTableView.tableHeaderView = headerView
    }
    
    func setUpView()
    {
        CommonFunctions.setTextViewBorderColor(description_ar_text)
        CommonFunctions.setTextViewBorderColor(description_en_text)
        
        CommonFunctions.setTextFieldBorderColor(service_name_en_text)
        CommonFunctions.setTextFieldBorderColor(service_name_ar_text)
        
        service_name_ar_text.setLeftPaddingPoints(5.0)
        service_name_ar_text.setRightPaddingPoints(5.0)
        
        service_name_en_text.setLeftPaddingPoints(5.0)
        service_name_ar_text.setRightPaddingPoints(5.0)
        
        CommonFunctions.setUIButtonBorderColor(saveButton)
        
        saveButton.setTitle(NSLocalizedString("SAVE_TEXT", comment: ""), for: UIControlState.normal)
        
        if isEdit
        {
            service_name_en_text.text = additional_edit_data["title_en"] as? String
            service_name_ar_text.text = additional_edit_data["title_ar"] as? String
        
            description_en_text.text = additional_edit_data["desc_en"] as? String
            description_ar_text.text = additional_edit_data["desc_ar"] as? String
            
            price_en_text.text = "\(currency_code)\(additional_edit_data["price"] as? String ?? "")"
            price_ar_text.text = "\(currency_code)\(additional_edit_data["price"] as? String ?? "")"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func saveButtonAction(_ sender: Any)
    {
        if (validateUserInput())
        {
            var additinal_data : [String : Any] = [:]
            
            additinal_data["title_en"] = service_name_en_text.text
            additinal_data["title_ar"] = service_name_ar_text.text
            
            additinal_data["desc_en"] = description_en_text.text
            additinal_data["desc_ar"] = description_ar_text.text
            
            let price_en = price_en_text.text?.replacingOccurrences(of: currency_code, with: "")
            additinal_data["price"] = price_en
            
            if (savePressed != nil)
            {
                savePressed!(additinal_data)
            }
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if (service_name_en_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_EN", comment: ""), dismissBloack:
                {
                    self.service_name_en_text?.becomeFirstResponder()
            })
        }
        //else if (description_en_text?.text?.isEmpty)!
        //{
        //    isError = false
        //    AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_EN", comment: ""), dismissBloack:
        //        {
        //            self.description_en_text?.becomeFirstResponder()
        //    })
        //}
        else if (price_en_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PRICE", comment: ""), dismissBloack:
                {
                    self.price_en_text?.becomeFirstResponder()
            })
        }
        else if (service_name_ar_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_AR", comment: ""), dismissBloack:
                {
                    self.service_name_ar_text?.becomeFirstResponder()
            })
        }
        //else if (description_ar_text?.text?.isEmpty)!
        //{
        //    isError = false
        //    AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_AR", comment: ""), dismissBloack:
        //        {
        //            self.description_ar_text?.becomeFirstResponder()
        //    })
        //}
        else if (price_ar_text?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PRICE", comment: ""), dismissBloack:
                {
                    self.price_ar_text?.becomeFirstResponder()
            })
        }
        else if (price_en_text?.text != price_ar_text?.text)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("DIFFERENT_PRICE", comment: ""), dismissBloack:
                {
                    
            })
        }
        
        return isError
    }
}

extension UITextField
{
    func setLeftPaddingPoints(_ amount:CGFloat)
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat)
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension AddAdditionalViewController : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == price_en_text
        {
            if #available(iOS 10.0, *)
            {
                textField.keyboardType = UIKeyboardType.asciiCapableNumberPad
            }
            else
            {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        else if textField == price_ar_text
        {
            if #available(iOS 10.0, *)
            {
                textField.keyboardType = UIKeyboardType.asciiCapableNumberPad
            }
            else
            {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField == price_en_text || textField == price_ar_text)
        {
            if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)
            {
                return false
            }
            
            if (textField.text?.hasPrefix(currency_code))!
            {
                var text = textField.text! as NSString
                text = text.replacingOccurrences(of: currency_code, with: "") as NSString
                
                if (string.count > 0) && (text.length + string.count) > 3
                {
                    return false
                }
            }
            else
            {
                if (string.count > 0) && ((textField.text?.count)! + string.count) > 3
                {
                    return false
                }
            }
            
            if !((textField.text?.hasPrefix(currency_code))!)
            {
                if string.count > 0
                {
                    let str1 = textField.text
                    if str1?.count == currency_code.count
                    {
                        let mutableString: String = "\(currency_code)\((textField.text)!)"
                        textField.text = mutableString
                    }
                    else
                    {
                        let mutableString: String = "\(currency_code)"
                        textField.text = mutableString
                    }
                }
            }
            else
            {
                if string.count == 0
                {
                    if textField.text?.count == currency_code.count {
                        textField.text = ""
                    }
                }
            }
        }
        
        if (textField == service_name_en_text || textField == service_name_ar_text)
        {
            if (string.count > 0) && ((textField.text?.count)! + string.count) > 40
            {
                return false
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField == price_en_text)
        {
            if textField.text == currency_code
            {
                price_en_text.text = ""
                price_ar_text.text = ""
            }
            else
            {
                price_en_text.text = textField.text
                price_ar_text.text = price_en_text.text
            }
        }
        else if (textField == price_ar_text)
        {
            if textField.text == currency_code
            {
                price_en_text.text = ""
                price_ar_text.text = ""
            }
            else
            {
                price_ar_text.text = textField.text
                price_en_text.text = price_ar_text.text
            }
        }
    }
}

extension AddAdditionalViewController : UITextViewDelegate
{
    // MARK: uitextview delegates
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (textView == description_en_text || textView == description_ar_text)
        {
            if (textView.text.count > 150)
            {
                if text == ""
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        
        return true
    }
}

extension AddAdditionalViewController : UITableViewDelegate, UITableViewDataSource
{
    func   tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)                                                                                                                            -> Int
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddAdditionalTableViewCell") as! AddAdditionalTableViewCell
        
        return cell
    }
}
