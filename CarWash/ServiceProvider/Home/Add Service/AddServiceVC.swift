//
//  AddServiceVC.swift
//  CarWash
//
//  Created by iOS on 23/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import RSKImageCropper
import QuartzCore
import MBProgressHUD
import AWSS3

class AddServiceVC: UIViewController, SubServiceListDelegate
{
    @IBOutlet var tblAdd : UITableView?
    @IBOutlet weak var btnSave: UIButton!
    
    var serviceAry  : NSMutableArray = []
    let imagePicker = UIImagePickerController()
    var imageCropVC : RSKImageCropViewController!
    
    var serviceDic : NSDictionary?
    
    var isImageSelected : Bool?
    var isCropped : Bool = false
    var currentTag : Int?
    var userImg :UIImage?
    var fileUrl : NSURL?
    var strDesc_en : String = "Service Description"
    
    var strDesc_ar : String =  "وصف الخدمة"
    
    var firstTime = 0
    
    var document_url = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        currentTag = 0
        isImageSelected = false
        
        setUpNavigationBar()
        
        tblAdd?.register(UINib(nibName: "ProfileDetailCell", bundle: nil),       forCellReuseIdentifier: "ProfileDetailCell")
        tblAdd?.register(UINib(nibName: "SubServiceList", bundle: nil), forCellReuseIdentifier: "SubServiceList")
        tblAdd?.register(UINib(nibName: "AddSubServicesCell", bundle: nil), forCellReuseIdentifier: "AddSubServicesCell")
        
        btnSave.setTitle(NSLocalizedString("SAVE_TEXT", comment: ""), for: UIControlState.normal)
        
        print("service arr = \(String(describing: serviceDic))")
        
        if serviceDic != nil
        {
            let dictHeader : NSMutableDictionary = [:]
            
            let dictServiceName = serviceDic?.object(forKey: "description") as? NSDictionary
            dictHeader.setValue(dictServiceName?.object(forKey: "ar"), forKey: "desc_ar")
            dictHeader.setValue(dictServiceName?.object(forKey: "en"), forKey: "desc_en")
            
            let dictServiceDescription = serviceDic?.object(forKey: "service_name") as? NSDictionary
            dictHeader.setValue(dictServiceDescription?.object(forKey: "ar"), forKey: "service_name_ar")
            dictHeader.setValue(dictServiceDescription?.object(forKey: "en"), forKey: "service_name_en")
            
            dictHeader.setValue(serviceDic?.object(forKey: "car_type"), forKey: "car_type")
            dictHeader.setValue(serviceDic?.object(forKey: "service_image"), forKey: "service_image")
            
            fileUrl = NSURL(string: (serviceDic?.object(forKey: "service_image") as? String)!)
            dictHeader.setObject((serviceDic?.object(forKey: "service_image") as? String)!, forKey: "image" as NSCopying)
            
            //do
            //{
            //    let imageData = try Data(contentsOf: fileUrl! as URL)
            //    dictHeader.setObject(imageData , forKey: "image" as NSCopying)
            //}
            //catch
            //{
            //
            //}
          
            serviceAry.add(dictHeader)
            
            if serviceDic?.object(forKey: "subservices") != nil
            {
                let arr1 : NSMutableArray = (serviceDic?.object(forKey: "subservices") as? NSArray)!.mutableCopy() as! NSMutableArray
                serviceAry.add(arr1)
            }
            
            if serviceDic?.object(forKey: "additional_services") != nil
            {
                let arr1 : NSMutableArray = (serviceDic?.object(forKey: "additional_services") as? NSArray)!.mutableCopy() as! NSMutableArray
                serviceAry.add(arr1)
            }
        }
        else
        {
            let dict : NSMutableDictionary = [:]
            serviceAry.add(dict)
        }
        
        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector (nextAction), name: NSNotification.Name(rawValue: "nextAction"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func setUpNavigationBar()
    {
        if serviceDic != nil
        {
            self.title = NSLocalizedString("SERVICE_DETAIL", comment: "")
        }
        else
        {
            self.title = NSLocalizedString("ADD_SERVICE", comment: "")
        }
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doneAction()
    {
        let indexpath : NSIndexPath = NSIndexPath(row: 0, section: 0) as NSIndexPath
        let cell : ProfileDetailCell = tblAdd?.cellForRow(at: indexpath as IndexPath) as! ProfileDetailCell
        
        if(currentTag == 1 || currentTag == 2)
        {
            let field : UITextField?
            
            if(currentTag == 1)
            {
                field = cell.txtNameEng
            }
            else
            {
                field =  cell.txtNameArabic
            }
            
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            
            if (!(field?.text?.isEmpty)!)
            {
                if (field?.tag == 1 )
                {
                    dict.setObject(field?.text ?? "", forKey: "service_name_en" as NSCopying)
                }
                else
                {
                    dict.setObject(field?.text ?? "", forKey: "service_name_ar" as NSCopying)
                }
            }
            
            self.serviceAry.replaceObject(at: 0, with: dict)
        }
        else if (currentTag == 3 || currentTag == 4)
        {
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            
            let field : UITextView?
            
            if(currentTag == 3)
            {
                field = cell.txVDescEng
            }
            else
            {
                field =  cell.txVDescArabic
            }
            
            if (!(field?.text?.isEmpty)!)
            {
                if (field?.tag == 3)
                {
                    dict.setObject(field?.text ?? "", forKey: "desc_en" as NSCopying)
                }
                else
                {
                    dict.setObject(field?.text ?? "", forKey: "desc_ar" as NSCopying)
                }
            }
            
            self.serviceAry.replaceObject(at: 0, with: dict)
        }
    }
    
    func nextAction()
    {
        let indexpath : NSIndexPath = NSIndexPath(row: 0, section: 0) as NSIndexPath
        let cell : ProfileDetailCell = tblAdd?.cellForRow(at: indexpath as IndexPath) as! ProfileDetailCell
        
        if(currentTag == 1 || currentTag == 2)
        {
            let field : UITextField?
            
            if(currentTag == 1)
            {
                field = cell.txtNameEng
            }
            else
            {
                field =  cell.txtNameArabic
            }
            
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            
            if (!(field?.text?.isEmpty)!)
            {
                if (field?.tag == 1 )
                {
                    dict.setObject(field?.text ?? "", forKey: "service_name_en" as NSCopying)
                }
                else
                {
                    dict.setObject(field?.text ?? "", forKey: "service_name_ar" as NSCopying)
                }
            }
            
            self.serviceAry.replaceObject(at: 0, with: dict)
        }
        else if (currentTag == 3 || currentTag == 4)
        {
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            
            let field : UITextView?
            
            if(currentTag == 3)
            {
                field = cell.txVDescEng
            }
            else
            {
                field =  cell.txVDescArabic
            }
            
            if (!(field?.text?.isEmpty)!)
            {
                if (field?.tag == 3)
                {
                    dict.setObject(field?.text ?? "", forKey: "desc_en" as NSCopying)
                }
                else
                {
                    dict.setObject(field?.text ?? "", forKey: "desc_ar" as NSCopying)
                }
            }
            
            self.serviceAry.replaceObject(at: 0, with: dict)
        }
    }
    
    func subServiceListDelegateClicked(cell: SubServiceList)
    {
        let ary : NSMutableArray = serviceAry.object(at: cell.cellSection) as! NSMutableArray
        let dict1 : NSDictionary = ary.object(at: cell.cellRow) as! NSDictionary
        
        //print("dict = \(dict1)")
        
        let add = AddSubServices()
        add.serviceDic = dict1
        
        if (cell.cellSection == 1) {
            add.serviceTypeTitle = "Sub"
        } else {
            add.serviceTypeTitle = "Additional"
        }
        
        add.savePressed =
            { (dict : NSMutableDictionary) -> Void in
                
                ary.remove(dict1)
                ary.add(dict)
                self.serviceAry.replaceObject(at: cell.cellSection, with: ary)
                self.tblAdd?.reloadData()
        }
        
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    // MARK: Other Methods
    
    func selectCarType()
    {
        RESIGN_KEYBOARD
        
        let typeObj = TypeVC()
        
        typeObj.okPressed =
            { (str : NSString) -> Void in
                
                let cell : ProfileDetailCell = self.tblAdd?.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfileDetailCell
                cell.btnType?.setTitle(str as String, for: .normal)
                
                //////// replace array
                
                let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
                dict.setObject(str, forKey: "car_type" as NSCopying)
                self.serviceAry.replaceObject(at: 0, with: dict)
        }
        
        self.navigationController?.pushViewController(typeObj, animated: true)
    }
    
    func addSubService()
    {
        let add = AddSubServices()
        add.serviceTypeTitle = "Sub"
        
        add.savePressed =
            { (dict : NSMutableDictionary) -> Void in
                
                if (self.serviceAry.count == 1)
                {
                    let ary : NSMutableArray = NSMutableArray()
                    ary.add(dict)

                    self.serviceAry.add(ary)
                    self.tblAdd?.reloadData()
                }
                else
                {
                    let ary : NSMutableArray = self.serviceAry.object(at: 1) as! NSMutableArray
                    ary.add(dict)
                    self.serviceAry.replaceObject(at: 1, with: ary)
                    self.tblAdd?.reloadData()
                }
            }
        
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    func addSubServices()
    {
        if(serviceAry.count == 1)
        {
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_ADD_SUB_SERVICE", comment: ""), dismissBloack:
                {
        
            })
        }
        else
        {
            let add = AddSubServices()
            add.serviceTypeTitle = "Additional"
            
            add.savePressed =
                { (dict : NSMutableDictionary) -> Void in
                    
                    if (self.serviceAry.count == 2)
                    {
                        let ary : NSMutableArray = NSMutableArray()
                        ary.add(dict)
                        
                        self.serviceAry.add(ary)
                        self.tblAdd?.reloadData()
                    }
                    else
                    {
                        let ary : NSMutableArray = self.serviceAry.object(at: 2) as! NSMutableArray
                        ary.add(dict)
                        self.serviceAry.replaceObject(at: 2, with: ary)
                        self.tblAdd?.reloadData()
                    }
            }
            
            self.navigationController?.pushViewController(add, animated: true)
        }
    }
    
    func changePic()
    {
        RESIGN_KEYBOARD
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func save()
    {
        if(validate())
        {
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            //addServiceApi()
            
            if serviceDic != nil {
                editServiceApi()
            } else {
                addServiceApi()
            }
        }
    }
   
    //MARK: Validate User Input
    
    func validate () -> Bool
    {
        print("serviceAry ", serviceAry.count)
        
        var isError : Bool = true
        
        let dict : NSMutableDictionary = serviceAry.object(at: 0) as! NSMutableDictionary
        
        if ((dict.object(forKey: "image")) == nil)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_LOGO", comment: ""), dismissBloack:
                {
                   
            })
        }
        else if !(isImageSelected!)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_LOGO", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if ((dict.object(forKey: "service_name_en")) == nil)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_EN", comment: ""), dismissBloack:
                {
                   
            })
        }
        else if ((dict.object(forKey: "service_name_ar")) == nil)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_AR", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if ((dict.object(forKey: "desc_en")) == nil)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_EN", comment: ""), dismissBloack:
                {
                
            })
        }
        else if ((dict.object(forKey: "desc_ar")) == nil)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_AR", comment: ""), dismissBloack:
                {
                   
            })
        }
        else if ((dict.object(forKey: "car_type")) == nil)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_CARS", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if (serviceAry.count == 1)
        {
             isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_ADD_SUB_SERVICE", comment: ""), dismissBloack:
                {
                    
            })
        }
        
        return isError
    }
    
    //MARK: API Call
    
    func addServiceApi()
    {
        let service1 = ServiceProvider()
        
        var string_name = ""
        
        if(isImageSelected)!
        {
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            string_name  = (dict.object(forKey: "image") as? String)!
        }
        
        var serviceJSON : NSString = ""
        var additionalServiceJSON : NSString = ""
        
        do
        {
            let ary : NSArray = serviceAry.object(at : 1) as! NSArray
            if(ary.count > 0)
            {
                let jsonData = try JSONSerialization.data(withJSONObject: ary , options: JSONSerialization.WritingOptions.prettyPrinted)
                serviceJSON = String(data: jsonData, encoding: String.Encoding.utf8)! as NSString
            }
        }
        catch
        {
            print(error)
        }
        
        do
        {
            if (serviceAry.count == 3)
            {
                let ary : NSArray = serviceAry.object(at : 2) as! NSArray
                if(ary.count > 0)
                {
                    let jsonData = try JSONSerialization.data(withJSONObject: ary , options: JSONSerialization.WritingOptions.prettyPrinted)
                    additionalServiceJSON = String(data: jsonData, encoding: String.Encoding.utf8)! as NSString
                }
            }
        }
        catch
        {
            print(error)
        }
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict1 = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let countryCodeCA = dict1.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        let currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        if serviceJSON.contains(currency_code)
        {
            serviceJSON = serviceJSON.replacingOccurrences(of: currency_code, with: "") as NSString
        }
        
        if additionalServiceJSON.contains(currency_code)
        {
            additionalServiceJSON = additionalServiceJSON.replacingOccurrences(of: currency_code, with: "") as NSString
        }
        
        let dict : NSDictionary = serviceAry.object(at : 0) as! NSDictionary
        
        var strType : String = dict.object(forKey: "car_type") as! String
        
        if(strType.isEqual(NSLocalizedString("SMALL_CARS", comment: "")))
        {
            strType = "small"
        }
        else if(strType.isEqual(NSLocalizedString("MEDIUM_CARS", comment: "")))
        {
            strType = "medium"
        }
        else if(strType.isEqual(NSLocalizedString("BIG_CARS", comment: "")))
        {
            strType = "big"
        }
        
        let dic : NSMutableDictionary = [
            
            "service_name_en"           : dict.object(forKey: "service_name_en") ?? "",
            "service_name_ar"           : dict.object(forKey: "service_name_ar") ?? "",
            "description_en"            : dict.object(forKey: "desc_en")  ?? "",
            "description_ar"            : dict.object(forKey: "desc_ar")  ?? "",
            "car_type"                  : strType ,
            "subservices"               : serviceJSON,
            "additional_services"       : additionalServiceJSON,
            "requirements"              : "",
            "notes"                     : "",
            "service_image"             : string_name
        ]
        
        print("dict = ", dic)
        
        service1.addServiceWithParameters(dic, nil, success: { (response, data)
            in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("addServiceApi = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            self.navigationController?.popViewController(animated: true)
                    })
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            
                    })
                }
            }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    // edit service api call
    
    func editServiceApi()
    {
        let service = ServiceProvider()
        
        var string_name = ""
        
        if(isImageSelected)!
        {
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            string_name  = (dict.object(forKey: "image") as? String)!
        }
        
        var serviceJSON : NSString = ""
        var additionalServiceJSON : NSString = ""
        
        do
        {
            let ary : NSArray = serviceAry.object(at : 1) as! NSArray
            
            if(ary.count > 0)
            {
                let jsonData = try JSONSerialization.data(withJSONObject: ary , options: JSONSerialization.WritingOptions.prettyPrinted)
                serviceJSON = String(data: jsonData, encoding: String.Encoding.utf8)! as NSString
            }
        }
        catch
        {
            print(error)
        }
        
        do
        {
            if (serviceAry.count == 3)
            {
                let ary : NSArray = serviceAry.object(at : 2) as! NSArray
                if(ary.count > 0)
                {
                    let jsonData = try JSONSerialization.data(withJSONObject: ary , options: JSONSerialization.WritingOptions.prettyPrinted)
                    additionalServiceJSON = String(data: jsonData, encoding: String.Encoding.utf8)! as NSString
                }
            }
        }
        catch
        {
            print(error)
        }
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict1 = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        //print("dict = \(dict)")
        
        let countryCodeCA = dict1.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        let currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        if serviceJSON.contains(currency_code)
        {
            serviceJSON = serviceJSON.replacingOccurrences(of: currency_code, with: "") as NSString
        }
        
        if additionalServiceJSON.contains(currency_code)
        {
            additionalServiceJSON = additionalServiceJSON.replacingOccurrences(of: currency_code, with: "") as NSString
        }
        
        let dict : NSDictionary = serviceAry.object(at : 0) as! NSDictionary

        var strType : String = dict.object(forKey: "car_type") as! String
        
        if(strType.isEqual(NSLocalizedString("SMALL_CARS", comment: "")))
        {
            strType = "small"
        }
        else if(strType.isEqual(NSLocalizedString("MEDIUM_CARS", comment: "")))
        {
            strType = "medium"
        }
        else if(strType.isEqual(NSLocalizedString("BIG_CARS", comment: "")))
        {
            strType = "big"
        }
        
        //print("service dic = \(serviceDic)")
        
        let dic : NSMutableDictionary = [
            
            "id"                        : serviceDic?.object(forKey: "id") ?? "",
            "service_name_en"           : dict.object(forKey: "service_name_en") ?? "",
            "service_name_ar"           : dict.object(forKey: "service_name_ar") ?? "",
            "description_en"            : dict.object(forKey: "desc_en")  ?? "",
            "description_ar"            : dict.object(forKey: "desc_ar")  ?? "",
            "car_type"                  : strType,
            "subservices"               : serviceJSON,
            "additional_services"       : additionalServiceJSON,
            "requirements"              : "",
            "notes"                     : "",
            "service_image"             : string_name
        ]
        
        print("dict = ", dic)
        
        service.editServiceWithParameters(dic, nil, success: { (response, data)
            in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("addServiceApi = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            self.navigationController?.popViewController(animated: true)
                    })
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            
                    })
                }
            }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

extension AddServiceVC : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        currentTag = textField.tag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        //////// replace array
        let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
        
        if (!(textField.text?.isEmpty)!)
        {
            if (textField.tag == 1 )
            {
                dict.setObject(textField.text ?? "", forKey: "service_name_en" as NSCopying)
            }
            else
            {
                dict.setObject(textField.text ?? "", forKey: "service_name_ar" as NSCopying)
            }
        }
        
        self.serviceAry.replaceObject(at: 0, with: dict)
    }
}

extension AddServiceVC : UITextViewDelegate
{
    // MARK: uitextview delegates
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        currentTag = textView.tag
        
        textView.textColor = UIColor.uicolorFromRGB(88, 88, 88)
        
        if (textView.tag == 3 && textView.text == strDesc_en)
        {
            textView.text = ""
        }
        else if (textView.text == strDesc_ar)
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.tag == 3)
        {
            if (textView.text == strDesc_en || textView.text.isEmpty)
            {
                textView.textColor = UIColor.uicolorFromRGB(199, 199, 205)
                textView.text = strDesc_en
            }
            else
            {
                let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
                dict.setObject(textView.text ?? "", forKey: "desc_en" as NSCopying)
                self.serviceAry.replaceObject(at: 0, with: dict)
            }
        }
        else if (textView.tag == 4)
        {
            if (textView.text == strDesc_ar || textView.text.isEmpty)
            {
                textView.textColor = UIColor.uicolorFromRGB(199, 199, 205)
                textView.text = strDesc_ar
            }
            else
            {
                let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
                dict.setObject(textView.text ?? "", forKey: "desc_ar" as NSCopying)
                self.serviceAry.replaceObject(at: 0, with: dict)
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (textView.text.count > 150) {
            //return false
            
            if text == "" {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
    
}

extension AddServiceVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // UIImagePicker Delegates
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                self.imageCropVC = nil
                
                self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.circle)
                
                self.imageCropVC.delegate = self
                
                self.present(self.imageCropVC, animated: true, completion: nil)
            })
        }
    }
}

extension AddServiceVC : RSKImageCropViewControllerDelegate
{
    // RSKImageCropViewController Delegates
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect)
    {
        self.dismiss(animated: true, completion: nil)
        
        _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation(croppedImage, 0.6)!)
        
        if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
            self.uploadServiceImage(usingImage: fileURL as NSURL)
        }
    }
    
    func uploadServiceImage(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "service" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameService + CommonFunctions.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.document_url = "https://s3-us-west-2.amazonaws.com/car-wash/service/" + CommonFunctions.getUserIdData()! + "/" + keyName
                    print(self.document_url)
                    
                    let indexpath : NSIndexPath = NSIndexPath(row: 0, section: 0) as NSIndexPath
                    let cell : ProfileDetailCell = self.tblAdd?.cellForRow(at: indexpath as IndexPath) as! ProfileDetailCell
                    
                    cell.imgService?.layer.cornerRadius = (0.2933 * SCREEN_WIDTH)/2  //97/375
                    cell.imgService?.layer.masksToBounds = true
                    
                    self.isImageSelected = true
                    self.isCropped = true
                    
                    let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
                    dict.setObject(self.document_url, forKey: "image" as NSCopying)
                    self.serviceAry.replaceObject(at: 0, with: dict)
                    
                    cell.imgService?.setShowActivityIndicator(true)
                    cell.imgService?.setIndicatorStyle(.gray)
                    
                    cell.imgService?.sd_setImage(with: URL.init(string: self.document_url), placeholderImage: #imageLiteral(resourceName: "group20"))
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                })
            }
            
            return nil
        }
    }
}

extension AddServiceVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return serviceAry.count == 1 ? 1 : serviceAry.count == 3 ? serviceAry.count+1     : serviceAry.count == 2 ? serviceAry.count+1 : 3;
    }
    
    func   tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)                                                                                                                            -> Int
    {
        if (section == 0)
        {
            return serviceAry.count == 1 ? 2 : 1
        }
        else if (section == 1  && self.serviceAry.count > 1)
        {
            let ary : NSMutableArray = self.serviceAry.object(at: 1) as! NSMutableArray
            
            return ary.count
        }
        else if (section == 2  && self.serviceAry.count > 2)
        {
            let ary : NSMutableArray = self.serviceAry.object(at: 2) as! NSMutableArray
            
            return ary.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if (section > 0 && section < serviceAry.count)
        {
            let header = UIView(frame : CGRect(x: 20, y: 0, width: SCREEN_WIDTH, height: 40))
            header.backgroundColor = UIColor.uicolorFromRGB(247, 247, 247)
            
            let lblHeader = UILabel(frame : CGRect(x: 20, y: 0, width: SCREEN_WIDTH, height: 40))
            lblHeader.font = UIFont(name: FONT_SEMIBOLD, size: 16)
            lblHeader.textColor = UIColor.uicolorFromRGB(2, 166, 242)
            
            //print("serviceAry = \(serviceAry.count)")
            
            if serviceAry.count > 2
            {
                let arr = serviceAry.object(at: 2) as! NSMutableArray
                //print("arr = \(arr)")
                
                if arr.count == 0
                {
                    lblHeader.text = section == 1 ?  NSLocalizedString("SUB_SERVICES_LIST", comment: "") : ""
                }
                else
                {
                    lblHeader.text = section == 1 ?  NSLocalizedString("SUB_SERVICES_LIST", comment: "") : NSLocalizedString("ADDITIONAL_SERVICES_LIST", comment: "")
                }
            }
            else
            {
                lblHeader.text = section == 1 ?  NSLocalizedString("SUB_SERVICES_LIST", comment: "") : NSLocalizedString("ADDITIONAL_SERVICES_LIST", comment: "")
            }
            
            header.addSubview(lblHeader)
            
            return header
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return section == 0 ? 0 : 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == serviceAry.count)
        {
            return 140 * scaleFactorX
        }
        
        return  indexPath.row == 0 && indexPath.section == 0 ? 565 * scaleFactorX : serviceAry.count == 1 && indexPath.row == serviceAry.count ? 140 * scaleFactorX : 440 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0 && indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailCell") as! ProfileDetailCell?
            
            cell?.txtNameEng?.delegate = self
            cell?.txtNameArabic?.delegate = self
            cell?.txVDescArabic?.delegate = self
            cell?.txVDescEng?.delegate = self
            
            cell?.btnType?.setTitle(NSLocalizedString("CAR_TYPE", comment: ""), for: UIControlState.normal)
            
            cell?.btnType?.addTarget(self, action: #selector(selectCarType), for: UIControlEvents.touchUpInside)
            
            cell?.btnCamera?.addTarget(self, action: #selector(changePic), for: UIControlEvents.touchUpInside)
            
            cell?.btnType?.layer.cornerRadius = 5.0
            cell?.btnType?.layer.masksToBounds = true
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            let dict : NSMutableDictionary = self.serviceAry.object(at: 0) as! NSMutableDictionary
            //print("dict = \(dict)")
            
            if dict.allKeys.count != 0
            {
                if dict.object(forKey: "desc_en") != nil {
                    cell?.txVDescEng?.text = dict.object(forKey: "desc_en") as! String
                }
                
                if dict.object(forKey: "desc_ar") != nil {
                    cell?.txVDescArabic?.text = dict.object(forKey: "desc_ar") as! String
                }
                
                if dict.object(forKey: "service_name_en") != nil {
                    cell?.txtNameEng?.text = dict.object(forKey: "service_name_en") as? String
                }
                
                if dict.object(forKey: "service_name_ar") != nil {
                    cell?.txtNameArabic?.text = dict.object(forKey: "service_name_ar") as? String
                }
                
                if dict.object(forKey: "car_type") != nil {
                    var strType : String = dict.object(forKey: "car_type") as! String
                    
                    if(strType.isEqual("small"))
                    {
                        strType = NSLocalizedString("SMALL_CARS", comment: "")
                    }
                    else if(strType.isEqual("medium"))
                    {
                        strType = NSLocalizedString("MEDIUM_CARS", comment: "")
                    }
                    else if(strType.isEqual("big"))
                    {
                        strType = NSLocalizedString("BIG_CARS", comment: "")
                    }
                    
                    cell?.btnType?.setTitle(strType, for: .normal)
                }
                
                if dict.object(forKey: "service_image") != nil
                {
                    let check_value = dict.object(forKey: "service_image") as! String
                    
                    let fileUrl = NSURL(string: (dict.object(forKey: "service_image") as? String)!)
                    
                    cell?.imgService?.sd_setImage(with: fileUrl! as URL, placeholderImage: nil)
                    
                    userImg  = cell?.imgService?.image
                    
                    if check_value.contains("undefined") {
                        isImageSelected = false
                    } else {
                        isImageSelected = true
                    }
                }
                
                cell?.imgService?.layer.cornerRadius = (0.2933 * SCREEN_WIDTH)/2  //97/375
                cell?.imgService?.layer.masksToBounds = true
            }
            
            return cell!;
        }
        else if(serviceAry.count == 1 && indexPath.row == serviceAry.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddSubServicesCell") as! AddSubServicesCell?
            
            cell?.btnAddService?.setTitle(NSLocalizedString("ADD_SUB_SERVICES", comment: ""), for: UIControlState.normal)
            cell?.btnAddAdditionalService?.setTitle(NSLocalizedString("ADD_ADDITIONAL_SERVICES", comment: ""), for: UIControlState.normal)
            
            cell?.btnAddService?.layer.cornerRadius = 5.0
            cell?.btnAddService?.layer.masksToBounds = true
            cell?.btnAddService?.addTarget(self, action: #selector(addSubService), for: UIControlEvents.touchUpInside)
            
            cell?.btnAddAdditionalService?.layer.cornerRadius = 5.0
            cell?.btnAddAdditionalService?.layer.masksToBounds = true
            cell?.btnAddAdditionalService?.addTarget(self, action: #selector(addSubServices), for: UIControlEvents.touchUpInside)
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!;
        }
        else if (indexPath.section == serviceAry.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddSubServicesCell") as! AddSubServicesCell?
            
            cell?.btnAddService?.setTitle(NSLocalizedString("ADD_SUB_SERVICES", comment: ""), for: UIControlState.normal)
            cell?.btnAddAdditionalService?.setTitle(NSLocalizedString("ADD_ADDITIONAL_SERVICES", comment: ""), for: UIControlState.normal)
            
            cell?.btnAddService?.layer.cornerRadius = 5.0
            cell?.btnAddService?.layer.masksToBounds = true
            cell?.btnAddService?.addTarget(self, action: #selector(addSubService), for: UIControlEvents.touchUpInside)
            
            cell?.btnAddAdditionalService?.layer.cornerRadius = 5.0
            cell?.btnAddAdditionalService?.layer.masksToBounds = true
            cell?.btnAddAdditionalService?.addTarget(self, action: #selector(addSubServices), for: UIControlEvents.touchUpInside)
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!;
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceList") as! SubServiceList?
            
            cell?.delegate = self;
            
            let ary : NSMutableArray = serviceAry.object(at: indexPath.section) as! NSMutableArray
            let dict : NSDictionary = ary.object(at: indexPath.row) as! NSDictionary
            
            cell?.cellSection = indexPath.section
            cell?.cellRow = indexPath.row
            
            cell?.txtTitle_en?.text         = dict.object(forKey: "title_en") as? String
            cell?.txtTitle_ar?.text         = dict.object(forKey: "title_ar") as? String
            cell?.txtPrice_ar?.text         = dict.object(forKey: "price_ar") as? String
            cell?.txtPrice_en?.text         = dict.object(forKey: "price_en") as? String
            cell?.txtDuration_ar?.text      = dict.object(forKey: "duration_ar") as? String
            cell?.txtDuration_en?.text      = dict.object(forKey: "duration_en") as? String
            cell?.txvDesc_ar?.text          = dict.object(forKey: "desc_ar") as? String
            cell?.txvDesc_en?.text          = dict.object(forKey: "desc_en") as? String
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!;
        }
    }
}
