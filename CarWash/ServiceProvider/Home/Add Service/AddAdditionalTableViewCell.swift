//
//  AddAdditionalTableViewCell.swift
//  CarWash
//
//  Created by Ratina on 6/11/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

//MARK: -
protocol AddAdditionalTableViewCellDelegate: class
{
    func deleteButtonDelegateMethod(_ cell: AddAdditionalTableViewCell)
    func editButtonDelegateMethod(_ cell: AddAdditionalTableViewCell)
}

class AddAdditionalTableViewCell: UITableViewCell {

    @IBOutlet weak var service_name_en_label: UILabel!
    @IBOutlet weak var service_name_ar_label: UILabel!
    
    @IBOutlet weak var description_en_label: UILabel!
    @IBOutlet weak var description_ar_label: UILabel!
    
    @IBOutlet weak var price_en_label: UITextField!
    @IBOutlet weak var price_ar_label: UITextField!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    weak var delegate: AddAdditionalTableViewCellDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonACtion(_ sender: Any)
    {
        guard let deleteDelegate = delegate else
        {
            return
        }
        
        deleteDelegate.deleteButtonDelegateMethod(self)
    }
    
    @IBAction func editButtonACtion(_ sender: Any)
    {
        guard let editDelegate = delegate else
        {
            return
        }
        
        editDelegate.editButtonDelegateMethod(self)
    }
}
