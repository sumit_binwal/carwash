//
//  ProviderHomeVC.swift
//  CarWash
//
//  Created by iOS on 04/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProviderHomeVC : UIViewController
{
    @IBOutlet var searchBar         : UISearchBar?
    @IBOutlet var searchView        : UIView?
    @IBOutlet var notificationView  : UIView?
    @IBOutlet weak var notificationBgImage: UIImageView!
    @IBOutlet weak var labelNoNotification: UILabel!
    @IBOutlet var tblNotification   : UITableView?
    @IBOutlet var tblHome           : UITableView?
    @IBOutlet var lblTitle          : UILabel?
    @IBOutlet var lblSubTitle       : UILabel?
    @IBOutlet var btnAddService     : UIButton?
    
    @IBOutlet var filterView: UIView!
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet var filterFooterView: UIView!
    
    var filterArr : [[String:Any]] = []
    
    var bell : UIBarButtonItem?
    var filter : UIBarButtonItem?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var providerServicesArr = [ProviderServicesModel]()
    
    var notificatioDataAry : NSMutableArray = []
    
    let refreshControl = UIRefreshControl()
    
    var pageNumber = 1
    
    var isPageRefresh = false
    
    var selectedFilterArr : [String] = []
    
    let formatter = DateFormatter()
    
    var date_string = ""
    
    var datePicker = UIDatePicker()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUpLabels()
        
        tblHome?.register(UINib(nibName: "ProviderHomeCell", bundle: nil), forCellReuseIdentifier: "ProviderHomeCell")
        
        tblNotification?.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        filterTableView?.register(UINib(nibName: "filterProviderTableViewCell", bundle: nil), forCellReuseIdentifier: "filterProviderTableViewCell")
        
        filterTableView?.register(UINib(nibName: "SelectDateTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectDateTableViewCell")
        
        searchView?.layer.shadowColor = UIColor.lightGray.cgColor
        searchView?.layer.shadowOffset = CGSize(width: 3, height: 3)
        searchView?.layer.shadowOpacity = 0.2
        searchView?.layer.shadowRadius = 3.0
        searchView?.layer.masksToBounds = false
        
        self.searchView?.isHidden = false
        self.lblTitle?.isHidden = true
        self.lblSubTitle?.isHidden = true
        self.tblHome?.isHidden = true
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *)
        {
            tblHome?.refreshControl = refreshControl
        }
        else
        {
            tblHome?.addSubview(refreshControl)
        }

        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
        
        if language == "en"
        {
            notificationBgImage.image = UIImage.init(named: "popoverBg")
        }
        else
        {
            notificationBgImage.image = UIImage.init(named: "popoverBg_ar")
        }
        
        var dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Pending"),
            "name" : "1"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Accepted"),
            "name" : "2"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Progress"),
            "name" : "4"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Reached"),
            "name" : "6"
        ]
        
        filterArr.append(dict)
        
        datePicker.datePickerMode = .date
        formatter.dateFormat = "dd MMM yyyy"
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker)
    {
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        
        formatter.dateFormat = "dd MMM yyyy"
        
        cellDate.dateTextField.text = formatter.string(from: (datePicker.date))
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        date_string = formatter.string(from: (datePicker.date))
    }
    
    func refreshHomeData(_ sender: Any) {
        
        self.providerServicesArr.removeAll()
        pageNumber = 1
        isPageRefresh = true
        
        if searchBar?.text == ""
        {
            serviceApiSearch(searchString: "")
        }
        else
        {
            serviceApiSearch(searchString: (searchBar?.text)!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        let textFieldInsideUISearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        
        searchBar?.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        searchBar?.setValue(NSLocalizedString("CANCEL_TEXT", comment: ""), forKey:"_cancelButtonText")
        
        submitButton.setTitle(NSLocalizedString("DONE", comment: ""), for: .normal)
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.left
        }
        else
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.right
        }
        
        self.setUpNavigationBar()

        labelNoNotification?.text = NSLocalizedString("NO_NOTIFICATION", comment: "")
        
        self.searchBar?.text = ""
        searchView?.isHidden = true
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
        
        tblHome?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        self.providerServicesArr.removeAll()
        isPageRefresh = true
        pageNumber = 1
        
        self.serviceApiSearch(searchString: "")
    }
    
    func setUpNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
        
        self.navigationItem.rightBarButtonItem = nil
        
        self.title = NSLocalizedString("HOME_TITLE", comment: "")
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        self.navigationController?.navigationBar.shadowImage = nil
        
        if (providerServicesArr.count > 0)
        {
            self.navigationController?.navigationBar.isTranslucent = false
        }
        else
        {
            self.navigationController?.navigationBar.isTranslucent = false
        }
        
        self.navigationItem.hidesBackButton = true
    
        bell = UIBarButtonItem(image:  #imageLiteral(resourceName: "bell"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(bellButtonPressed))
        
        filter = UIBarButtonItem(image:  #imageLiteral(resourceName: "menu"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(filterButtonPressed))

        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
   
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.rightBarButtonItems = [bell!, filter!]
    }
    
    func setUpLabels()
    {
        let str1 = NSLocalizedString("ADD_SERVICE_LABEL_1", comment: "") as NSString
        let str2 = NSLocalizedString("ADD_SERVICE_LABEL_2", comment: "") as NSString
        
        let attributedString = NSMutableAttributedString(string: String(format: "%@ %@", str1, str2))
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(88, 88, 88)
            ], range: NSRange(location: 0, length: str1.length)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: attributedString.length-str2.length, length: str2.length)
        )
        
        lblTitle?.attributedText = attributedString
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bellButtonPressed()
    {
        self.searchBar?.endEditing(true)
        searchBar?.setShowsCancelButton(false, animated: true)
        
        showPopOver()
    }
    
    func filterButtonPressed()
    {
        self.searchBar?.endEditing(true)
        searchBar?.setShowsCancelButton(false, animated: true)
        
        showFilterPopUp()
    }
    
    func showFilterPopUp()
    {
        filterView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        
        bell?.isEnabled = false
        filter?.isEnabled = false
        
        self.filterTableView?.tableFooterView = self.filterFooterView
        
        self.filterTableView.reloadData()
        
        APPDELEGATE.window?.addSubview(filterView!)
    }
    
    func showPopOver()
    {
        notificationView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        bell?.isEnabled = false
        filter?.isEnabled = false
        
        self.labelNoNotification.isHidden = true
        self.tblNotification?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        notificationApi()
        
        APPDELEGATE.window?.addSubview(notificationView!)
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func closeNotificationView()
    {
        self.tblHome?.reloadData()
        bell?.isEnabled = true
        filter?.isEnabled = true
        notificationView?.removeFromSuperview()
    }
    
    @IBAction func closeFilterView()
    {
        self.selectedFilterArr.removeAll()
        
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        cellDate.dateTextField.text = ""
        
        date_string = ""
        
        bell?.isEnabled = true
        filter?.isEnabled = true
        filterView?.removeFromSuperview()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        self.providerServicesArr.removeAll()
        pageNumber = 1
        isPageRefresh = true
        
        self.serviceApiSearch(searchString: "")
    }
    
    @IBAction func submitButtonAction()
    {
        bell?.isEnabled = true
        filter?.isEnabled = true
        filterView?.removeFromSuperview()
        
        if selectedFilterArr.count == 0 && date_string == ""
        {
            self.tblHome?.reloadData()
            
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("Please select atleast one option.", comment: ""), dismissBloack: {
                
            })
        }
        else
        {
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
          
            self.providerServicesArr.removeAll()
            pageNumber = 1
            isPageRefresh = true
            
            self.serviceApiSearch(searchString: "")
        }
    }
    
    @IBAction func addService()
    {
        let services = AddServiceViewController()
        self.navigationController?.pushViewController(services, animated: true)
    }
    
    //MARK: API Call
    
    func notificationApi()
    {
        let service = CustomerService()
        
        service.notificationApiRequestWithParameters(nil, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    self.notificatioDataAry = (data?.object(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray
                    
                    DispatchQueue.main.async
                        {
                            if(self.notificatioDataAry.count == 0)
                            {
                                self.tblNotification?.isHidden = true
                                self.labelNoNotification.isHidden = false
                            }
                            else
                            {
                                self.labelNoNotification.isHidden = true
                                self.tblNotification?.isHidden = false
                                self.tblNotification?.reloadData()
                            }
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
  
    func serviceApiSearch(searchString : String)
    {
        var searchStr = searchString as NSString
        searchStr = searchStr.replacingOccurrences(of: " ", with: "%20") as NSString
        
        var sort_string = ""
        if selectedFilterArr.count > 0
        {
            sort_string = CommonFunctions.getCommaSepratedStringFromArray (completeArray: self.selectedFilterArr)
        }
        else
        {
            sort_string = ""
        }
        
        let url = BASE_URL + "booking/bookinglist?search=\(searchStr)&page=\(pageNumber)&request_status=\(sort_string)&date=\(date_string)"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .get, parameters: nil) { [weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            self?.refreshControl.endRefreshing()
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        self?.lblTitle?.isHidden = false
                        self?.lblSubTitle?.isHidden = false
                        self?.tblHome?.isHidden = true
                        self?.isPageRefresh = true
                })
                
                return
            }
            
            if dictionary == nil
            {
                self?.lblTitle?.isHidden = false
                self?.lblSubTitle?.isHidden = false
                self?.tblHome?.isHidden = true
                self?.isPageRefresh = true
                
                return
            }
            
            if statusCode == 200
            {
                let str = dictionary!["status"] as? String
                
                if str == "success"
                {
                    let servicesDtaArr1 = dictionary!["data"] as? [[String : Any]]
                    
                    guard servicesDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self?.updateServicesModelArray(usingArray: servicesDtaArr1!)
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            self?.lblTitle?.isHidden = false
                            self?.lblSubTitle?.isHidden = false
                            self?.tblHome?.isHidden = true
                            self?.isPageRefresh = true
                    })
                }
            }
        }
    }
    
    //MARK:- Updating review Model Array
    func updateServicesModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0
        {
            self.isPageRefresh = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ProviderServicesModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            providerServicesArr.append(model) // Adding model to array
        }
        
        self.tblHome?.isHidden = false
        
        if searchBar?.text == ""
        {
            if self.providerServicesArr.count > 0
            {
                self.searchView?.isHidden = false
                self.tblHome?.tableHeaderView = self.searchView
            }
            else
            {
                self.searchView?.isHidden = true
                self.tblHome?.tableHeaderView = nil
            }
            
        }
        else
        {
            self.searchView?.isHidden = false
            self.tblHome?.tableHeaderView = self.searchView
        }
        
        self.tblHome?.reloadData()
        
        if self.providerServicesArr.count > 0
        {
            self.lblTitle?.isHidden = true
            self.lblSubTitle?.isHidden = true
        }
        else
        {
            self.lblTitle?.isHidden = false
            self.lblSubTitle?.isHidden = false
            
            self.lblTitle?.bringSubview(toFront: self.tblHome!)
            self.lblSubTitle?.bringSubview(toFront: self.tblHome!)
        }
        
        if self.providerServicesArr.count == 0
        {
            if self.selectedFilterArr.count > 0 || self.date_string != ""
            {
                self.filter?.isEnabled = true
            }
            else
            {
                self.filter?.isEnabled = false
            }
        }
        else
        {
            self.filter?.isEnabled = true
        }
    }
    
    func doneAction()
    {
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        
        formatter.dateFormat = "dd MMM yyyy"
        
        cellDate.dateTextField.text = formatter.string(from: (datePicker.date))
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        date_string = formatter.string(from: (datePicker.date))
    }
}

extension ProviderHomeVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblHome
        {
            return providerServicesArr.count
        }
        else if tableView == filterTableView
        {
            return filterArr.count + 2
        }
        else
        {
            return notificatioDataAry.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblHome
        {
            return 100 * scaleFactorX
        }
        else if tableView == filterTableView
        {
            if indexPath.row == filterArr.count + 1
            {
                return SCREEN_WIDTH * (77/375)
            }
            else
            {
                return SCREEN_WIDTH * (44/375)
            }
        }
        else
        {
            return SCREEN_WIDTH * (88/375)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView == tblHome)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderHomeCell") as! ProviderHomeCell?
            
            if self.providerServicesArr.count > 0
            {
                cell?.btnCall?.addTarget(self, action: #selector(callUser), for: .touchUpInside)
                
                cell?.btnMail?.addTarget(self, action: #selector(mailUser), for: .touchUpInside)
                
                cell?.unreadCountView.isHidden = true
                
                cell?.unreadCountView.layer.cornerRadius = (cell?.unreadCountView.frame.size.height)!/2
                cell?.unreadCountView.layer.borderColor = UIColor.clear.cgColor
                cell?.unreadCountView.layer.borderWidth = 1.0
                cell?.unreadCountView.layer.masksToBounds = true
                
                cell?.imgUser?.layer.cornerRadius = /*(cell?.imgUser?.frame.size.height)!*/ 58 / 2 * scaleFactorX
                cell?.imgUser?.layer.masksToBounds = true
                
                cell?.imgUser?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
                cell?.imgUser?.layer.borderWidth = 1.0
                
                let numberShow = providerServicesArr[indexPath.row].show_phone
                if numberShow == 1
                {
                    cell?.btnCall?.isHidden = false
                }
                else
                {
                    cell?.btnCall?.isHidden = true
                }
                
                let userDict = providerServicesArr[indexPath.row].user_id
                
                let imageUrlStr = userDict["avatar_url"] as! String
                let imageUrl = URL(string: imageUrlStr)
                
                cell?.imgUser?.setShowActivityIndicator(true)
                cell?.imgUser?.setIndicatorStyle(.gray)
                
                cell?.imgUser?.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "group20"))
                
                let nameStr = userDict["name"] as! String
                cell?.lblName?.text = nameStr
                
                if let fcm_user_id = userDict["fcm_user_id"] as? String
                {
                    Conversation.showProviderUnReadCount(from_fcm_user_id: fcm_user_id, completion: { (unreadcount) in
                        
                        if unreadcount == "0"
                        {
                            cell?.unreadCountLabel.text = ""
                            cell?.unreadCountView.isHidden = true
                        }
                        else
                        {
                            cell?.unreadCountView.isHidden = false
                            cell?.unreadCountLabel.text = unreadcount
                        }
                    })
                }
                
                let formatter = DateFormatter()
                
                var endTimeStr = providerServicesArr[indexPath.row].endtime
                
                if (endTimeStr.contains(" "))
                {
                    formatter.dateFormat = "HH:mm a"
                }
                else
                {
                    formatter.dateFormat = "HH:mm"
                }
                
                var date = formatter.date(from: endTimeStr)
                formatter.dateFormat = "hh:mm a"
                
                if let date1 = date
                {
                    endTimeStr = formatter.string(from: date1)
                }
                
                var startTimeStr = providerServicesArr[indexPath.row].starttime
                
                if (startTimeStr.contains(" "))
                {
                    formatter.dateFormat = "HH:mm a"
                }
                
                date = formatter.date(from: startTimeStr)
                formatter.dateFormat = "hh:mm a"
                
                if let date1 = date
                {
                    startTimeStr = formatter.string(from: date1)
                }
                
                var str1 = providerServicesArr[indexPath.row].date
                formatter.dateFormat = "yyyy-MM-dd"
                date = formatter.date(from: str1)
                formatter.dateFormat = "dd MMM yyyy"
                str1 = formatter.string(from: date!)
                
                cell?.lblDate?.text = "\(str1),    \(startTimeStr) - \(endTimeStr)"
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                if (isPageRefresh && self.providerServicesArr.count - 1 == indexPath.row)
                {
                    pageNumber += 1
                    
                    if searchBar?.text == ""
                    {
                        serviceApiSearch(searchString: "")
                    }
                    else
                    {
                        serviceApiSearch(searchString: (searchBar?.text)!)
                    }
                }
                
                let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
                let dictUser1 = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
                
                let countryCodeCA = dictUser1.object(forKey: "iso_code")
                let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
                let localeCA = NSLocale(localeIdentifier: localeIdCA)
                let currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
                
                cell?.orderNumber.text = NSLocalizedString("Request Number", comment: "") + " : \(providerServicesArr[indexPath.row].inc_id)"
                
                let priceStr = providerServicesArr[indexPath.row].price
                let price123 = String(format : "%.2f", priceStr)
                cell?.costLabel.text = NSLocalizedString("Cost", comment: "") + " : \(currency_code)\(price123)"
                
                let status : Int = providerServicesArr[indexPath.row].request_status
                
                if (status == 1)
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "pending")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "pending_ar")
                    }
                }
                else if (status == 2)
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "accepted")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "accepted_ar")
                    }
                }
                else if (status == 3)
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "canceled")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "canceled_ar")
                    }
                }
                else if (status == 4)
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "progress")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "progress_ar")
                    }
                }
                else if (status == 5)
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "completed")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "completed_ar")
                    }
                }
                else if status == 6
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "reached")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "reached_ar")
                    }
                }
                else if status == 7
                {
                    if language == "en"
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "canceled")
                    }
                    else
                    {
                        cell?.statusImage?.image = #imageLiteral(resourceName: "canceled_ar")
                    }
                }
            }
            
            return cell!;
        }
        else if tableView == filterTableView
        {
            if indexPath.row == filterArr.count + 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDateTableViewCell") as! SelectDateTableViewCell?
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell?.selectDateLabel.text = CommonFunctions.getLocalizedString(localizedName: "SELECT_DATE")
                
                cell?.dateTextField.inputView = datePicker
                cell?.dateTextField.delegate = self
                
                if(LanguageManager.currentLanguageIndex() == 0)
                {
                    cell?.dateTextField.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
                    cell?.dateTextField.textAlignment = NSTextAlignment.left
                }
                else
                {
                    cell?.dateTextField.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
                    cell?.dateTextField.textAlignment = NSTextAlignment.right
                }
                
                return cell!
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "filterProviderTableViewCell") as! filterProviderTableViewCell?
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell?.checkButton.isUserInteractionEnabled = false
                
                if indexPath.row == 0
                {
                    if selectedFilterArr.count > 0 || date_string != ""
                    {
                        if selectedFilterArr.count == filterArr.count
                        {
                            cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                        }
                        else
                        {
                            cell?.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                        }
                    }
                    else
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                        
                        for dict in filterArr
                        {
                            selectedFilterArr.append(dict["name"] as! String)
                        }
                    }
                    
                    cell?.sortLabel.text = CommonFunctions.getLocalizedString(localizedName: "Select All")
                }
                else
                {
                    let dict = filterArr[indexPath.row - 1]
                    
                    if selectedFilterArr.contains(dict["name"] as! String)
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    }
                    else
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                    }
                    
                    cell?.sortLabel.text = dict["image"] as? String
                }
                
                return cell!
            }
        }
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell?
            
            if cell == nil
            {
                cell = NotificationCell(style: UITableViewCellStyle.default, reuseIdentifier: "NotificationCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            let dic:NSDictionary = notificatioDataAry.object(at: indexPath.row) as! NSDictionary
            
            cell?.lblTitle?.text = dic.value(forKey: "message") as? String
            
            let datestring = dic.value(forKey: "createdAt") as? String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: datestring!)
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            let dateString = dateFormatter.string(from: date!)
            
            cell?.lblDate?.text = dateString
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblHome
        {
            let completedRequest = CompletedRequestVC()
            completedRequest.providerServiceData = providerServicesArr[indexPath.row]
            self.navigationController?.pushViewController(completedRequest, animated: true)
        }
        else if tableView == filterTableView
        {
            if indexPath.row == 0
            {
                if selectedFilterArr.count == 0
                {
                    for (i, dict)  in filterArr.enumerated()
                    {
                        let index = IndexPath(row: i + 1, section: 0)
                        let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                        selectedFilterArr.append(dict["name"] as! String)
                        cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                else
                {
                    for (i, dict)  in filterArr.enumerated()
                    {
                        for (j, str) in selectedFilterArr.enumerated()
                        {
                            if str == dict["name"] as! String
                            {
                                selectedFilterArr.remove(at: j)
                                
                                let index = IndexPath(row: i + 1, section: 0)
                                let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                                cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                            }
                        }
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                }
            }
            else if indexPath.row == filterArr.count + 1
            {
                let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
                
                cellDate.dateTextField.becomeFirstResponder()
            }
            else
            {
                let cell = tableView.cellForRow(at: indexPath) as! filterProviderTableViewCell
                
                let dict = filterArr[indexPath.row - 1]
                
                if selectedFilterArr.contains(dict["name"] as! String)
                {
                    cell.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                    for (i, str) in selectedFilterArr.enumerated()
                    {
                        if str == dict["name"] as! String
                        {
                            selectedFilterArr.remove(at: i)
                        }
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                }
                else
                {
                    cell.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    selectedFilterArr.append(dict["name"] as! String)
                }
                
                if selectedFilterArr.count == filterArr.count
                {
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
            }
        }
    }
    
    // MARK: - Other Methods
    
    func callUser(sender : UIButton)
    {
        let point = tblHome?.convert(CGPoint.zero, from: sender)
        let indexPath = tblHome?.indexPathForRow(at: point!)
        
        let dict = providerServicesArr[(indexPath?.row)!].user_id
        let contact : String = String(format : "%@%@", dict["country_code"] as! String, dict["mobile"] as! String)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailUser(sender : UIButton)
    {
        let point = tblHome?.convert(CGPoint.zero, from: sender)
        let indexPath = tblHome?.indexPathForRow(at: point!)
        
        let provider_name_dict = providerServicesArr[(indexPath?.row)!].user_id
        
        let provider_name = provider_name_dict["name"] as! String
        
        if let device_token = provider_name_dict["device_token"] as? String
        {
            if let fcm_user_id = provider_name_dict["fcm_user_id"] as? String
            {
                if fcm_user_id != ""
                {
                    let messages = MessagesVC()
                
                    messages.selected_user_name = provider_name
                    messages.selected_user_id = fcm_user_id
                    messages.device_token_string = device_token
                
                    self.navigationController?.pushViewController(messages, animated: true)
                }
            }
        }
    }
}

extension ProviderHomeVC : UISearchBarDelegate
{
    //MARK: - UISearchController delegates
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
       
        self.providerServicesArr.removeAll()
        pageNumber = 1
        searchBar.text = ""
        isPageRefresh = true
        
        serviceApiSearch(searchString: "")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        self.providerServicesArr.removeAll()
        pageNumber = 1
        isPageRefresh = true
        
        serviceApiSearch(searchString: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
    }
}

extension ProviderHomeVC : UITextFieldDelegate
{
    
}
