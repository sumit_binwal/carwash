//
//  ProviderServicesModel.swift
//  CarWash
//
//  Created by Ratina on 6/12/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import Foundation

class ProviderServicesModel
{
    var id = ""
    var inc_id = ""
    var provider_id = ""
    
    var request_status = 0
    
    var date = ""
    var createdAt = ""
    
    var distance = 0.0
    var duration = 0
    var price = 0.0
    
    var endtime = ""
    
    var service_data : [String : Any] = [:]
    //"service_data" =
    //{
    //    "additional_services" =
    //    (
    //        {
    //            "desc_ar" = "Gdgk;ldfh";
    //            "desc_en" = "Lsjflk skflsdg";
    //            price = 5;
    //            "title_ar" = "additional ar";
    //            "title_en" = additional;
    //        }
    //    );
    //
    //    "car_type" = medium;
    //
    //    createdAt = "2018-06-11T12:51:44.551Z";
    //
    //    description =
    //    {
    //        ar = Hfhgf;
    //        en = Fgjfj;
    //    };
    //
    //    duration = 15;
    //
    //    id = 5b1e70605dbeb2be56dbec63;
    //
    //    "inc_id" = 1003;
    //
    //    isDeleted = 0;
    //
    //    isactive = 1;
    //
    //    notes =
    //    (
    //        ""
    //    );
    //
    //    price = 15;
    //
    //    requirements =
    //    (
    //        Water,
    //        Electricity
    //    );
    //
    //    "service_name" =
    //    {
    //        ar = hkjhsf;
    //        en = "package name eng";
    //    };
    //
    //    "sort_description" =
    //    {
    //        ar = "Hksjhfkjds gdshgksj\thfkdhgs";
    //        en = Fdgdfh;
    //    };
    //
    //    updatedAt = "2018-06-12T05:09:10.312Z";
    //
    //    "user_id" =
    //    {
    //        "device_token" = "eF9hzjJWz04:APA91bHHNH-Dq24iYnOO7SNPdeZ0tz6ZmIFt1hJyT8ggt0zoXV099wVuw5STegYehnnjLZuA_XYY--WE8gwFJi1bD-LwHRYbiovR5T_RJ2Lt2Jp3kzEUbwwjp70fS-6nGYjRr2vU_zCy";
    //        id = 5ad9d255757011592a5f4788;
    //        name = "Car User 5";
    //        "notification_setting" = 1;
    //        os = ios;
    //        "provider_name" =
    //        {
    //            ar = "Nx,n ,mbm";
    //            en = "Car Provider 5";
    //        };
    //    };
    //}
    
    var service_id = ""
    var starttime = ""
    var user_address = ""
    
    var show_phone = 0
    
    var user_id : [String : Any] = [:]
    //"user_id" =
    //{
    //    "_id" = 5adefc027c8d6cb371f5a0db;
    //    "avatar_url" = "https://s3-us-west-2.amazonaws.com/car-wash/avatar/5adefc027c8d6cb371f5a0db/avatar1525095578.jpg";
    //    "country_code" = "+966";
    //    "device_token" = "";
    //    email = "caruser8@yopmail.com";
    //    "fcm_user_id" = UGCvLyvyBYNf0SgenKrqwwOEYJ33;
    //    "inc_id" = 105;
    //    mobile = "123-456-987";
    //    name = "Car User 8";
    //};
    
    var user_location : [Double] = []
    //"user_location" =
    //(
    //    "75.78726959228516",
    //    "26.91243362426758"
    //);
    
    init()
    {
        id = ""
        inc_id = ""
        provider_id = ""
        
        request_status = 0
        
        date = ""
        createdAt = ""
        
        distance = 0.0
        duration = 0
        price = 0.0
        
        endtime = ""
        
        service_data = [:]
        
        service_id = ""
        starttime = ""
        user_address = ""
        
        show_phone = 0
        
        user_id = [:]
        user_location = []
    }
    
    deinit
    {
        print("ProviderServices Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let cateID = dictionary["_id"] as? String
        {
            id = cateID
        }
        
        if let inc_id1 = dictionary["inc_id"] as? Int
        {
            inc_id = String(inc_id1)
        }
        
        if let provider_id1 = dictionary["provider_id"] as? String
        {
            provider_id = provider_id1
        }
        
        if let request_status1 = dictionary["request_status"] as? Int
        {
            request_status = request_status1
        }
        
        if let date1 = dictionary["date"] as? String
        {
            date = date1
        }
        
        if let createdAt1 = dictionary["createdAt"] as? String
        {
            createdAt = createdAt1
        }
        
        if let distance1 = dictionary["distance"] as? Double
        {
            distance = distance1
        }
        
        if let duration1 = dictionary["duration"] as? Int
        {
            duration = duration1
        }
        
        if let endtime1 = dictionary["endtime"] as? NSArray
        {
            endtime = endtime1[0] as! String
        }
        
        if let starttime1 = dictionary["starttime"] as? NSArray
        {
            starttime = starttime1[0] as! String
        }
        
        if let price1 = dictionary["price"] as? Double
        {
            price = price1
        }
        
        if let service_data1 = dictionary["service_data"] as? [String : Any]
        {
            service_data = service_data1
        }
        
        if let service_id1 = dictionary["service_id"] as? String
        {
            service_id = service_id1
        }
        
        if let show_phone1 = dictionary["show_phone"] as? Int
        {
            show_phone = show_phone1
        }
        
        if let user_address1 = dictionary["user_address"] as? String
        {
            user_address = user_address1
        }
        
        if let user_id1 = dictionary["user_id"] as? [String : Any]
        {
            user_id = user_id1
        }
        
        if let user_location1 = dictionary["user_location"] as? [Double]
        {
            user_location = user_location1
        }
    }
}

//{
//    "_id" = 5b1faf1e58cdb260709187fe;
//    "cancel_reason" = "";
//    createdAt = "2018-06-12T11:31:42.966Z";
//    date = "2018-06-12";
//    day = Tuesday;
//    distance = "0.197778002435678";
//    duration = 15;
//    endtime =             (
//    "18:30",
//    1110
//    );
//    "inc_id" = 1008;
//    price = 20;
//    "provider_id" = 5ad9d255757011592a5f4788;
//    "request_status" = 1;
//    "service_data" =             {
//        "additional_services" =                 (
//            {
//                "desc_ar" = "Gdgk;ldfh";
//                "desc_en" = "Lsjflk skflsdg";
//                price = 5;
//                "title_ar" = "additional ar";
//                "title_en" = additional;
//            }
//        );
//        "car_type" = medium;
//        createdAt = "2018-06-11T12:51:44.551Z";
//        description =                 {
//            ar = Hfhgf;
//            en = Fgjfj;
//        };
//        duration = 15;
//        id = 5b1e70605dbeb2be56dbec63;
//        "inc_id" = 1003;
//        isDeleted = 0;
//        isactive = 1;
//        notes =                 (
//        ""
//        );
//        price = 15;
//        requirements =                 (
//        Water,
//        Electricity
//        );
//        "service_name" =                 {
//            ar = hkjhsf;
//            en = "package name eng";
//        };
//        "sort_description" =                 {
//            ar = "Hksjhfkjds gdshgksj\thfkdhgs";
//            en = Fdgdfh;
//        };
//        updatedAt = "2018-06-12T05:09:10.312Z";
//        "user_id" =                 {
//            "device_token" = "eF9hzjJWz04:APA91bHHNH-Dq24iYnOO7SNPdeZ0tz6ZmIFt1hJyT8ggt0zoXV099wVuw5STegYehnnjLZuA_XYY--WE8gwFJi1bD-LwHRYbiovR5T_RJ2Lt2Jp3kzEUbwwjp70fS-6nGYjRr2vU_zCy";
//            id = 5ad9d255757011592a5f4788;
//            name = "Car User 5";
//            "notification_setting" = 1;
//            os = ios;
//            "provider_name" =                     {
//                ar = "Nx,n ,mbm";
//                en = "Car Provider 5";
//            };
//        };
//    };
//    "service_id" = 5b1e70605dbeb2be56dbec63;
//    "show_phone" = 1;
//    starttime =             (
//    "18:00",
//    1080
//    );
//    updatedAt = "2018-06-12T11:31:42.966Z";
//    "user_address" = "Jacob Road, Ganpati Nagar, Jaipur, Rajasthan";
//    "user_id" =             {
//        "_id" = 5adefc027c8d6cb371f5a0db;
//        "avatar_url" = "https://s3-us-west-2.amazonaws.com/car-wash/avatar/5adefc027c8d6cb371f5a0db/avatar1525095578.jpg";
//        "country_code" = "+966";
//        "device_token" = "";
//        email = "caruser8@yopmail.com";
//        "fcm_user_id" = UGCvLyvyBYNf0SgenKrqwwOEYJ33;
//        "inc_id" = 105;
//        mobile = "123-456-987";
//        name = "Car User 8";
//    };
//    "user_location" =             (
//        "75.78726959228516",
//        "26.91243362426758"
//    );
//},
